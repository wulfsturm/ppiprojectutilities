﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RefactorPPISource
{
    public class RemoveComments
    {
        public string Line { get; set; }
        public int CommentLevel { get; set; }
        public RemoveComments(string statement, int commentLevelIn)
        {
            CommentLevel = commentLevelIn;
            if (!String.IsNullOrEmpty(statement))
            {
                string line = statement;
                if (CommentLevel > 0)
                {
                    if (line.Contains("*/"))
                    {
                        int end = line.IndexOf("*/");
                        Line = line.Remove(0, end + 2).Trim();
                        CommentLevel--;
                    }
                    else
                        Line = string.Empty;
                }
                else if (line.Contains("//"))
                {
                    int index = line.IndexOf("//");
                    Line = line.Substring(0, index).Trim();
                }
                else if (line.Contains("/*"))
                {
                    int start = line.IndexOf("/*");
                    if (line.Contains("*/"))
                    {
                        int end = line.IndexOf("*/");
                        Line = line.Remove(start, end - start + 2).Trim();
                    }
                    else
                    {
                        Line = line.Substring(0, start).Trim();
                        CommentLevel++;
                    }
                }
                else
                    Line = line;
            }
            else
                Line = string.Empty;
        }
    }
}
