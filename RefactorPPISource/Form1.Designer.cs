﻿namespace RefactorPPISource
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SourceCodeTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ProjectDirectoryTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.RefactorButton = new System.Windows.Forms.Button();
            this.ExitButton = new System.Windows.Forms.Button();
            this.LogTextBox = new System.Windows.Forms.TextBox();
            this.BrowseSourceCodeButton = new System.Windows.Forms.Button();
            this.BrowseProjectDirectoryButton = new System.Windows.Forms.Button();
            this.stdlibCheckBox = new System.Windows.Forms.CheckBox();
            this.stringCheckBox = new System.Windows.Forms.CheckBox();
            this.ScreenCheckBox = new System.Windows.Forms.CheckBox();
            this.GUIFuncsCheckBox = new System.Windows.Forms.CheckBox();
            this.DataObjectCheckBox = new System.Windows.Forms.CheckBox();
            this.TableObjectCheckBox = new System.Windows.Forms.CheckBox();
            this.UnmanagedCodeWrapperCheckBox = new System.Windows.Forms.CheckBox();
            this.PrettifyCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // SourceCodeTextBox
            // 
            this.SourceCodeTextBox.AllowDrop = true;
            this.SourceCodeTextBox.Location = new System.Drawing.Point(163, 61);
            this.SourceCodeTextBox.Name = "SourceCodeTextBox";
            this.SourceCodeTextBox.Size = new System.Drawing.Size(691, 26);
            this.SourceCodeTextBox.TabIndex = 19;
            this.SourceCodeTextBox.TextChanged += new System.EventHandler(this.SourceCodeTextBox_TextChanged);
            this.SourceCodeTextBox.DragDrop += new System.Windows.Forms.DragEventHandler(this.SourceCodeTextBox_DragDrop);
            this.SourceCodeTextBox.DragEnter += new System.Windows.Forms.DragEventHandler(this.SourceCodeTextBox_DragEnter);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(50, 64);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 20);
            this.label3.TabIndex = 18;
            this.label3.Text = "Source Code";
            // 
            // ProjectDirectoryTextBox
            // 
            this.ProjectDirectoryTextBox.Location = new System.Drawing.Point(163, 12);
            this.ProjectDirectoryTextBox.Name = "ProjectDirectoryTextBox";
            this.ProjectDirectoryTextBox.Size = new System.Drawing.Size(691, 26);
            this.ProjectDirectoryTextBox.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 20);
            this.label1.TabIndex = 15;
            this.label1.Text = "Project Directory";
            // 
            // RefactorButton
            // 
            this.RefactorButton.Location = new System.Drawing.Point(190, 144);
            this.RefactorButton.Name = "RefactorButton";
            this.RefactorButton.Size = new System.Drawing.Size(104, 36);
            this.RefactorButton.TabIndex = 21;
            this.RefactorButton.Text = "Refactor";
            this.RefactorButton.UseVisualStyleBackColor = true;
            this.RefactorButton.Click += new System.EventHandler(this.RefactorButton_Click);
            // 
            // ExitButton
            // 
            this.ExitButton.Location = new System.Drawing.Point(413, 144);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(104, 36);
            this.ExitButton.TabIndex = 22;
            this.ExitButton.Text = "Exit";
            this.ExitButton.UseVisualStyleBackColor = true;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // LogTextBox
            // 
            this.LogTextBox.Location = new System.Drawing.Point(163, 195);
            this.LogTextBox.Multiline = true;
            this.LogTextBox.Name = "LogTextBox";
            this.LogTextBox.ReadOnly = true;
            this.LogTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.LogTextBox.Size = new System.Drawing.Size(691, 355);
            this.LogTextBox.TabIndex = 23;
            // 
            // BrowseSourceCodeButton
            // 
            this.BrowseSourceCodeButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BrowseSourceCodeButton.BackgroundImage = global::RefactorPPISource.Properties.Resources.browse;
            this.BrowseSourceCodeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BrowseSourceCodeButton.FlatAppearance.BorderSize = 0;
            this.BrowseSourceCodeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BrowseSourceCodeButton.Location = new System.Drawing.Point(864, 53);
            this.BrowseSourceCodeButton.Name = "BrowseSourceCodeButton";
            this.BrowseSourceCodeButton.Size = new System.Drawing.Size(47, 42);
            this.BrowseSourceCodeButton.TabIndex = 20;
            this.BrowseSourceCodeButton.Tag = "Input";
            this.BrowseSourceCodeButton.UseVisualStyleBackColor = true;
            this.BrowseSourceCodeButton.Click += new System.EventHandler(this.BrowseFileButton_Click);
            // 
            // BrowseProjectDirectoryButton
            // 
            this.BrowseProjectDirectoryButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BrowseProjectDirectoryButton.BackgroundImage = global::RefactorPPISource.Properties.Resources.browse;
            this.BrowseProjectDirectoryButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BrowseProjectDirectoryButton.FlatAppearance.BorderSize = 0;
            this.BrowseProjectDirectoryButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BrowseProjectDirectoryButton.Location = new System.Drawing.Point(864, 4);
            this.BrowseProjectDirectoryButton.Name = "BrowseProjectDirectoryButton";
            this.BrowseProjectDirectoryButton.Size = new System.Drawing.Size(47, 42);
            this.BrowseProjectDirectoryButton.TabIndex = 17;
            this.BrowseProjectDirectoryButton.Tag = "ProjectDirectory";
            this.BrowseProjectDirectoryButton.UseVisualStyleBackColor = true;
            this.BrowseProjectDirectoryButton.Click += new System.EventHandler(this.BrowseFolderButton_Click);
            // 
            // stdlibCheckBox
            // 
            this.stdlibCheckBox.AutoSize = true;
            this.stdlibCheckBox.Checked = true;
            this.stdlibCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.stdlibCheckBox.Location = new System.Drawing.Point(163, 93);
            this.stdlibCheckBox.Name = "stdlibCheckBox";
            this.stdlibCheckBox.Size = new System.Drawing.Size(118, 24);
            this.stdlibCheckBox.TabIndex = 24;
            this.stdlibCheckBox.Text = "Add stdlib.h";
            this.stdlibCheckBox.UseVisualStyleBackColor = true;
            // 
            // stringCheckBox
            // 
            this.stringCheckBox.AutoSize = true;
            this.stringCheckBox.Checked = true;
            this.stringCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.stringCheckBox.Location = new System.Drawing.Point(163, 114);
            this.stringCheckBox.Name = "stringCheckBox";
            this.stringCheckBox.Size = new System.Drawing.Size(120, 24);
            this.stringCheckBox.TabIndex = 25;
            this.stringCheckBox.Text = "Add string.h";
            this.stringCheckBox.UseVisualStyleBackColor = true;
            // 
            // ScreenCheckBox
            // 
            this.ScreenCheckBox.AutoSize = true;
            this.ScreenCheckBox.Checked = true;
            this.ScreenCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ScreenCheckBox.Location = new System.Drawing.Point(304, 93);
            this.ScreenCheckBox.Name = "ScreenCheckBox";
            this.ScreenCheckBox.Size = new System.Drawing.Size(131, 24);
            this.ScreenCheckBox.TabIndex = 26;
            this.ScreenCheckBox.Text = "Add Screen.h";
            this.ScreenCheckBox.UseVisualStyleBackColor = true;
            // 
            // GUIFuncsCheckBox
            // 
            this.GUIFuncsCheckBox.AutoSize = true;
            this.GUIFuncsCheckBox.Checked = true;
            this.GUIFuncsCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.GUIFuncsCheckBox.Location = new System.Drawing.Point(304, 114);
            this.GUIFuncsCheckBox.Name = "GUIFuncsCheckBox";
            this.GUIFuncsCheckBox.Size = new System.Drawing.Size(148, 24);
            this.GUIFuncsCheckBox.TabIndex = 27;
            this.GUIFuncsCheckBox.Text = "Add GUIfuncs.h";
            this.GUIFuncsCheckBox.UseVisualStyleBackColor = true;
            // 
            // DataObjectCheckBox
            // 
            this.DataObjectCheckBox.AutoSize = true;
            this.DataObjectCheckBox.Checked = true;
            this.DataObjectCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.DataObjectCheckBox.Location = new System.Drawing.Point(458, 93);
            this.DataObjectCheckBox.Name = "DataObjectCheckBox";
            this.DataObjectCheckBox.Size = new System.Drawing.Size(163, 24);
            this.DataObjectCheckBox.TabIndex = 28;
            this.DataObjectCheckBox.Text = "Add DataObject.h";
            this.DataObjectCheckBox.UseVisualStyleBackColor = true;
            // 
            // TableObjectCheckBox
            // 
            this.TableObjectCheckBox.AutoSize = true;
            this.TableObjectCheckBox.Checked = true;
            this.TableObjectCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.TableObjectCheckBox.Location = new System.Drawing.Point(459, 114);
            this.TableObjectCheckBox.Name = "TableObjectCheckBox";
            this.TableObjectCheckBox.Size = new System.Drawing.Size(168, 24);
            this.TableObjectCheckBox.TabIndex = 29;
            this.TableObjectCheckBox.Text = "Add TableObject.h";
            this.TableObjectCheckBox.UseVisualStyleBackColor = true;
            // 
            // UnmanagedCodeWrapperCheckBox
            // 
            this.UnmanagedCodeWrapperCheckBox.AutoSize = true;
            this.UnmanagedCodeWrapperCheckBox.Checked = true;
            this.UnmanagedCodeWrapperCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.UnmanagedCodeWrapperCheckBox.Location = new System.Drawing.Point(644, 93);
            this.UnmanagedCodeWrapperCheckBox.Name = "UnmanagedCodeWrapperCheckBox";
            this.UnmanagedCodeWrapperCheckBox.Size = new System.Drawing.Size(270, 24);
            this.UnmanagedCodeWrapperCheckBox.TabIndex = 30;
            this.UnmanagedCodeWrapperCheckBox.Text = "Add UnmanagedCodeWrapper.h";
            this.UnmanagedCodeWrapperCheckBox.UseVisualStyleBackColor = true;
            // 
            // PrettifyCheckBox
            // 
            this.PrettifyCheckBox.AutoSize = true;
            this.PrettifyCheckBox.Checked = true;
            this.PrettifyCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.PrettifyCheckBox.Location = new System.Drawing.Point(644, 114);
            this.PrettifyCheckBox.Name = "PrettifyCheckBox";
            this.PrettifyCheckBox.Size = new System.Drawing.Size(186, 24);
            this.PrettifyCheckBox.TabIndex = 31;
            this.PrettifyCheckBox.Text = "Prettify Source Code";
            this.PrettifyCheckBox.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 562);
            this.Controls.Add(this.PrettifyCheckBox);
            this.Controls.Add(this.UnmanagedCodeWrapperCheckBox);
            this.Controls.Add(this.TableObjectCheckBox);
            this.Controls.Add(this.DataObjectCheckBox);
            this.Controls.Add(this.GUIFuncsCheckBox);
            this.Controls.Add(this.ScreenCheckBox);
            this.Controls.Add(this.stringCheckBox);
            this.Controls.Add(this.stdlibCheckBox);
            this.Controls.Add(this.LogTextBox);
            this.Controls.Add(this.ExitButton);
            this.Controls.Add(this.RefactorButton);
            this.Controls.Add(this.BrowseSourceCodeButton);
            this.Controls.Add(this.SourceCodeTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.BrowseProjectDirectoryButton);
            this.Controls.Add(this.ProjectDirectoryTextBox);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Refector PPI C Source Code";
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BrowseSourceCodeButton;
        private System.Windows.Forms.TextBox SourceCodeTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button BrowseProjectDirectoryButton;
        private System.Windows.Forms.TextBox ProjectDirectoryTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button RefactorButton;
        private System.Windows.Forms.Button ExitButton;
        private System.Windows.Forms.TextBox LogTextBox;
        private System.Windows.Forms.CheckBox stdlibCheckBox;
        private System.Windows.Forms.CheckBox stringCheckBox;
        private System.Windows.Forms.CheckBox ScreenCheckBox;
        private System.Windows.Forms.CheckBox GUIFuncsCheckBox;
        private System.Windows.Forms.CheckBox DataObjectCheckBox;
        private System.Windows.Forms.CheckBox TableObjectCheckBox;
        private System.Windows.Forms.CheckBox UnmanagedCodeWrapperCheckBox;
        private System.Windows.Forms.CheckBox PrettifyCheckBox;
    }
}

