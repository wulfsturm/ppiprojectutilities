﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RefactorPPISource
{
    public class CParser
    {
        private string[] reservedWords =
        {
                "auto",    "double", "int",    "struct",   "break", "else",  "long",   "switch",   "case",     "enum", "register", "typedef",
                "char",    "extern", "return", "union",    "const", "float", "short",  "unsigned", "continue", "for",  "signed",   "void",
                "default", "goto",   "sizeof", "volatile", "do",    "if",    "static", "while"
        };
        public String[] ReservedWords { get { return reservedWords; } }
        public bool InAssignStatement { get; set; }
        public int IfLevel { get; set; }
        public int ForLevel { get; set; }
        public int SwitchLevel { get; set; }
    }
}
