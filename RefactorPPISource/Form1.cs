﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RefactorPPISource
{
    public partial class Form1 : Form
    {
        private char[] TRIMCHARS = new char[] { ' ', '\t' };
        private enum ADDFLAG
        {
            Beginning,
            AfterSystemIncludes,
            BeforeFirstBreak,
            AfterFirstBreak,
            AfterAllIncludes
        }
        private bool performSourceTextChanged;
        public Form1()
        {
            InitializeComponent();
            RefactorButton.Enabled = false;
            performSourceTextChanged = true;
            ProjectDirectoryTextBox.Text = Properties.Settings.Default.ProjectDirectory;
            this.Icon = Properties.Resources.refactor1;
        }
        protected override void OnPaint(PaintEventArgs e)
        {
//            SourceCodeTextBox.Focus();
            base.OnPaint(e);
        }
        string GetExceptionMessage(string message, System.Exception exception)
        {
            string text = string.Format("{0}\n{1}", message, exception.Message);
            System.Exception inner = exception.InnerException;
            while (inner != null)
            {
                message = string.Format("{0}\n{1}", message, inner.Message);
                inner = inner.InnerException;
            }
            return text;
        }
        List<string> ReadFile(string fileName)
        {
            List<string> contents = new List<string>();
            try
            {
                StreamReader streamReader = new StreamReader(fileName);
                while (!streamReader.EndOfStream)
                {
                    string line = streamReader.ReadLine();
                    contents.Add(line);
                }
                streamReader.Close();
            }
            catch (System.Exception ex)
            {
                string message = GetExceptionMessage(string.Format("Error opening or reading \"{0}\"", fileName), ex);
                MessageBox.Show(message);
            }
            return contents;
        }
        private string WriteFile(List<string> contents, string fileName)
        {
            string retval = string.Empty;
            try
            {
                StreamWriter streamWriter = new StreamWriter(fileName, false);
                foreach (string line in contents)
                    streamWriter.WriteLine(line);
                streamWriter.Close();
            }
            catch (System.Exception ex)
            {
                string message = GetExceptionMessage(string.Format("Error opening or writing to {0}.", fileName), ex);
                MessageBox.Show(message);
                retval = string.Format("Failed to write {0}", fileName);
            }
            return retval;
        }
        private void AddLinesToTextBox(List<string> lines, TextBox textBox)
        {
            string[] activityText = textBox.Lines;
            List<string> activityList = new List<string>(activityText);
            activityList.AddRange(lines);
            textBox.Lines = activityList.ToArray<string>();
        }
        private void AddLineToTextBox(string line, TextBox textBox)
        {
            string[] activityText = textBox.Lines;
            List<string> activityList = new List<string>(activityText);
            activityList.Add(line);
            textBox.Lines = activityList.ToArray<string>();
            textBox.SelectionStart = textBox.TextLength;
            textBox.ScrollToCaret();
        }
        public string GetStartingWhiteSpace(string line)
        {
            string retval = string.Empty;
            int i = 0;
            while ((i < line.Length) && (string.IsNullOrWhiteSpace(line.Substring(i, 1)))) i++;
            if (i > 0)
                retval = line.Substring(0, i);
            return retval;
        }
        public int FindFirstInclude(List<string> fileContents, int start)
        {
            int end = start;
            while ((end < fileContents.Count()) && (!fileContents[end].StartsWith("#include"))) end++;
            if (end >= fileContents.Count()) end = 0;
            return end;
        }
        public int FindLastInclude(List<string> fileContents, int start)
        {
            int lastInclude = start;
            for (int i = start; i < fileContents.Count(); i++)
                if (fileContents[i].StartsWith("#include ")) lastInclude = i;
            return lastInclude;
        }
        public int SkipCommentsAndWhiteSpace(List<string> fileContents, int start)
        {
            int end = start;
            bool loop = true;
            while (loop)
            {
                if ((!fileContents[end].Trim().StartsWith("/")) && !(fileContents[end].Trim() == ""))
                    loop = false;
                else
                {
                    end = SkipCommentBlock(fileContents, end);
                    if ((!fileContents[end].Trim().StartsWith("/")) && !(fileContents[end].Trim() == ""))
                        loop = false;
                    else
                        end++;
                }
            }
            return end;
        }
        public int SkipCommentBlock(List<string> fileContents, int start)
        {
            int end = start;
            if (fileContents[end].Trim().StartsWith("/*"))
            {
                if (!fileContents[end].Trim().Contains("*/"))
                {
                    end++;
                    while ((end < fileContents.Count) && (!fileContents[end].Trim().Contains("*/"))) end++;
                }
                else
                    end++;
            }
            else if (fileContents[end].Trim().StartsWith("//"))
                end++;
            return end;
        }
        public string IndentLine(string line, int indent)
        {
            line = line.Trim();
            for (int i = 0; i < indent; i++)
                line = string.Format("    {0}", line);
            return line;
        }
        private bool FindStatement(string statement, List<string> fileContents)
        {
            bool retval = false;
            foreach (string line in fileContents)
            {
                if (line.Trim() == statement.Trim())
                {
                    retval = true;
                    break;
                }
            }
            return retval;
        }
        private bool AddStatement(List<string> fileContents, string statement, string afterStatement)
        {
            bool retval = false;
            if (!FindStatement(statement, fileContents))
            {
                if (String.IsNullOrEmpty(afterStatement))
                {
                    int index = 0;
                    while ((fileContents[index].StartsWith("/")) || (fileContents[index].StartsWith(" "))) index++;
                    if (index < fileContents.Count())
                    {
                        fileContents.Insert(index, statement);
                        AddLineToTextBox(string.Format("\"{0}\" added", statement), LogTextBox);
                        retval = true;
                    }
                    else
                    {
                        MessageBox.Show(String.Format("No place found to add statement \"{0}\".", statement));
                        AddLineToTextBox(String.Format("No place found to add statement \"{0}\".", statement), LogTextBox);
                    }
                }
                else
                {
                    int index = 0;
                    while ((index < fileContents.Count()) && (!fileContents[index].Contains(afterStatement))) index++;
                    if (index < fileContents.Count())
                    {
                        string whiteSpaces = GetStartingWhiteSpace(fileContents[index]);
                        string line = string.Format("{0}{1}", whiteSpaces, statement);
                        fileContents.Insert(index + 1, line);
                        AddLineToTextBox(string.Format("\"{0}\" added after \"{1}\"", line, fileContents[index]), LogTextBox);
                        retval = true;
                    }
                    else
                    {
                        MessageBox.Show(String.Format("No statement \"{0}\" found in file.\n\"{1}\" not added.", afterStatement, statement));
                        AddLineToTextBox(String.Format("No statement \"{0}\" found in file.\n\"{1}\" not added.", afterStatement, statement), LogTextBox);
                    }
                }
            }
            return retval;
        }
        private bool AddStatement(List<string> fileContents, string statement, ADDFLAG addFlag)
        {
            bool retval = false;
            if (!FindStatement(statement, fileContents))
            {
                int index = 0;
                switch (addFlag)
                {
                    case ADDFLAG.AfterAllIncludes:
                        index = FindLastInclude(fileContents, 0);
                        if (index < fileContents.Count())
                        {
                                fileContents.Insert(index + 1, statement);
                                AddLineToTextBox(string.Format("\"{0}\" added", statement), LogTextBox);
                                retval = true;
                        }
                        else
                        {
                            MessageBox.Show(String.Format("No place found to add statement \"{0}\".", statement));
                            AddLineToTextBox(String.Format("No place found to add statement \"{0}\".", statement), LogTextBox);
                        }
                        break;

                    case ADDFLAG.AfterSystemIncludes:
                        index = FindFirstInclude(fileContents, 0);
                        if (index < fileContents.Count())
                        {
                            while (fileContents[index].StartsWith("#include <")) index++;
                            if (index < fileContents.Count())
                            {
                                fileContents.Insert(index, statement);
                                AddLineToTextBox(string.Format("\"{0}\" added", statement), LogTextBox);
                                retval = true;
                            }
                            else
                            {
                                MessageBox.Show(String.Format("No place found to add statement \"{0}\".", statement));
                                AddLineToTextBox(String.Format("No place found to add statement \"{0}\".", statement), LogTextBox);
                            }
                        }
                        else
                        {
                            MessageBox.Show(String.Format("No place found to add statement \"{0}\".", statement));
                            AddLineToTextBox(String.Format("No place found to add statement \"{0}\".", statement), LogTextBox);
                        }
                        break;

                    case ADDFLAG.BeforeFirstBreak:
                        {
                            index = FindFirstInclude(fileContents, 0);
                            bool firstBreak = false;
                            while (!firstBreak)
                            {
                                if (fileContents[index].StartsWith("#include"))
                                {
                                    index++;
                                    if (index >= fileContents.Count)
                                        break;
                                }
                                else if (fileContents[index].Trim().StartsWith("/"))
                                {
                                    index = SkipCommentBlock(fileContents, index);
                                    if (index >= fileContents.Count)
                                        break;
                                }
                                else
                                {
                                    firstBreak = true;
                                }
                            }
                            if (index < fileContents.Count())
                            {
                                fileContents.Insert(index, statement);
                                AddLineToTextBox(string.Format("\"{0}\" added", statement), LogTextBox);
                                retval = true;
                            }
                            else
                            {
                                MessageBox.Show(String.Format("No place found to add statement \"{0}\".", statement));
                                AddLineToTextBox(String.Format("No place found to add statement \"{0}\".", statement), LogTextBox);
                            }
                        }
                        break;

                    case ADDFLAG.AfterFirstBreak:
                        {
                            index = FindFirstInclude(fileContents, 0);
                            bool firstBreak = false;
                            while (!firstBreak)
                            {
                                if (fileContents[index].StartsWith("#include"))
                                    index++;
                                else if (fileContents[index].Trim().StartsWith("/"))
                                {
                                    index = SkipCommentBlock(fileContents, index);
                                }
                                else
                                {
                                    index++;
                                    firstBreak = true;
                                }
                            }
                            if (index < fileContents.Count())
                            {
                                fileContents.Insert(index + 1, statement);
                                AddLineToTextBox(string.Format("\"{0}\" added", statement), LogTextBox);
                                retval = true;
                            }
                            else
                            {
                                MessageBox.Show(String.Format("No place found to add statement \"{0}\".", statement));
                                AddLineToTextBox(String.Format("No place found to add statement \"{0}\".", statement), LogTextBox);
                            }
                        }
                        break;

                    case ADDFLAG.Beginning:
                        index = FindFirstInclude(fileContents, 0);
                        if (index < fileContents.Count())
                        {
                            fileContents.Insert(index, statement);
                            AddLineToTextBox(string.Format("\"{0}\" added", statement), LogTextBox);
                            retval = true;
                        }
                        else
                        {
                            MessageBox.Show(String.Format("No place found to add statement \"{0}\".", statement));
                            AddLineToTextBox(String.Format("No place found to add statement \"{0}\".", statement), LogTextBox);
                        }
                        break;
                }
            }
            return retval;
        }
        public bool ReplaceStatement(List<string> fileContents, string lineToReplace, string newLine)
        {
            bool retval = false;
            for (int i = 0; (i < fileContents.Count) && !retval; i++)
            {
                if (fileContents[i].Trim(TRIMCHARS) == lineToReplace)
                {
                    fileContents[i] = fileContents[i].Replace(lineToReplace, newLine);
                    AddLineToTextBox(String.Format("Statement \"{0}\" replaced with \"{1}\".", lineToReplace, newLine), LogTextBox);
                    retval = true;
                }
            }
            return retval;
        }
        public bool RemoveStatement(List<string> fileContents, string lineToRemove)
        {
            bool retval = false;
            for (int i = 0; (i < fileContents.Count) && !retval; i++)
            {
                if (fileContents[i].Trim(TRIMCHARS) == lineToRemove)
                {
                    fileContents.RemoveAt(i);
                    AddLineToTextBox(String.Format("Statement \"{0}\" removed.", lineToRemove), LogTextBox);
                    retval = true;
                }
            }
            return retval;
        }
        public bool ReplaceText(List<string> fileContents, string textToReplace, string newText)
        {
            bool retval = false;
            for (int i = 0; i < fileContents.Count; i++)
            {
                if (fileContents[i].Contains(textToReplace))
                {
                    fileContents[i] = fileContents[i].Replace(textToReplace, newText);
                    retval = true;
                }
            }
            return retval;
        }
        private void BrowseFolderButton_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.ShowNewFolderButton = false;
            //			fd.RootFolder = Environment.SpecialFolder.MyComputer;
            if ((string)button.Tag == "ProjectDirectory")
            {
                fd.Description = "Specify the path where the Visual Studio project is";
                fd.SelectedPath = ProjectDirectoryTextBox.Text;
            }
            DialogResult dialogResult = fd.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                if ((string)button.Tag == "ProjectDirectory")
                    ProjectDirectoryTextBox.Text = fd.SelectedPath;
            }
            fd.Dispose();
        }
        private string RegexFixItInnerWorkings(string line, string matchString, string o)
        {
            string a = line;
            Match am = Regex.Match(a, matchString);
            while (am.Success)
            {
                string s = a.Substring(am.Index, am.Length).Replace(o, string.Format(" {0} ", o));
                a = a.Remove(am.Index, am.Length);
                a = a.Insert(am.Index, s);
                am = am = Regex.Match(a, matchString);
            }
            return a;
        }
        private string RegexFixIt(string line, string o, bool escape)
        {
            string matchString;
            if (escape)
                matchString = string.Format("[0-9,a-z,A-Z,)]\\{0}[0-9,a-z,A-Z,(]", o);
            else
                matchString = string.Format("[0-9,a-z,A-Z,)]{0}[0-9,a-z,A-Z,(]", o);
            string a = RegexFixItInnerWorkings(line, matchString, o);
            if (escape)
                matchString = string.Format("[0-9,a-z,A-Z,)]\\{0}[ ]", o);
            else
                matchString = string.Format("[0-9,a-z,A-Z,)]{0}[ ]", o);
            a = RegexFixItInnerWorkings(a, matchString, o);
            if (escape)
                matchString = string.Format("[ ]\\{0}[0-9,a-z,A-Z,(]", o);
            else
                matchString = string.Format("[ ]{0}[0-9,a-z,A-Z,(]", o);
            a = RegexFixItInnerWorkings(a, matchString, o);
            return a;
        }
        private void PrettifyCode(List<string> fileContents)
        {
            int indent = 0;
            int index = FindLastInclude(fileContents, 0) + 1;
            // First pass
            if (index < fileContents.Count())
            {
                int commentLevel = 0;
                List<int> linesToRemove = new List<int>();
                for (int i = index; i < fileContents.Count(); i++)
                {
                    while (fileContents[i].Contains(" ;")) fileContents[i] = fileContents[i].Replace(" ;", ";");
                    fileContents[i] = fileContents[i].Replace("if(", "if (");
                    fileContents[i] = fileContents[i].Replace("for(", "for (");
                    fileContents[i] = RegexFixIt(fileContents[i], "<", false);
                    fileContents[i] = RegexFixIt(fileContents[i], ">", false);
                    fileContents[i] = RegexFixIt(fileContents[i], "==", false);
                    fileContents[i] = RegexFixIt(fileContents[i], "-", false);
                    fileContents[i] = RegexFixIt(fileContents[i], "+", true);
                    string line = fileContents[i].Trim();
                    RemoveComments removeComments = new RemoveComments(line, commentLevel);
                    line = removeComments.Line;
                    commentLevel = removeComments.CommentLevel;
                    if ((String.IsNullOrWhiteSpace(line.Trim())) || (commentLevel > 0))
                    {

                    }
                    else
                    {
                        if (line.Trim() == ",")
                        {
                            if (!fileContents[i + 1].Trim().StartsWith("}"))
                                fileContents[i - 1] = String.Format("{0},", fileContents[i - 1]);
                            linesToRemove.Add(i);
                        }
                        else if ((line.Contains("{")) && (line.Trim() != "{"))
                        {
                            if (line.TrimEnd().EndsWith("{"))
                            {
                                if (!line.Trim().StartsWith("{"))
                                {
                                    fileContents[i] = fileContents[i].Replace("{", "").TrimEnd();
                                    fileContents.Insert(i + 1, string.Format("{0}", "{"));
                                }
                            }
                            else if ((!line.TrimEnd().EndsWith("}")) && (!line.TrimEnd().EndsWith("};")))
                            {
                                if (!line.Trim().StartsWith("{"))
                                {
                                    int slot = fileContents.IndexOf("{");
                                    string beginningOfLine = fileContents[i].Substring(slot);
                                    string restOfLine = fileContents[i].Substring(slot + 1, fileContents[i].Length - slot - 1);
                                    fileContents[i] = beginningOfLine;
                                    fileContents.Insert(i + 1, string.Format("{0}", "{"));
                                    fileContents.Insert(i + 2, string.Format("{0}", restOfLine));
                                }
                            }
                        }
                        else if ((line.Contains("}")) && ((line.Trim() != "}") || (line.Trim() != "];")))
                        {
                            if (line.TrimEnd().EndsWith("}"))
                            {
                                line = fileContents[i].Replace("}", "").TrimEnd();
                                if (!String.IsNullOrEmpty(line.Trim()))
                                {
                                    fileContents[i] = string.Format("{0}", line);
                                    fileContents.Insert(i + 1, string.Format("{0}", "}"));
                                }
                            }
                            else if (line.TrimEnd().EndsWith("};"))
                            {
                                line = fileContents[i].Replace("};", "").TrimEnd();
                                if (!String.IsNullOrEmpty(line.Trim()))
                                {
                                    fileContents[i] = string.Format("{0}", line);
                                    fileContents.Insert(i + 1, string.Format("{0}", "};"));
                                }
                            }
                            else
                            {
                                string restOfLine = fileContents[i].Replace("}", "").Trim();
                                fileContents[i] = string.Format("{0}", "}");
                                fileContents.Insert(i + 1, string.Format("{0}", restOfLine));
                            }
                        }
                    }
                }
                for (int i = linesToRemove.Count - 1; i >= 0; i--)
                    fileContents.RemoveAt(linesToRemove[i]);
                // Second pass
                    int inCase = 0;
                int inIf = 0;
                bool inIfNoBraces = false;
                bool inContinue = false;
                for (int i = index; i < fileContents.Count(); i++)
                {
                    string line = fileContents[i].Trim();
                    RemoveComments removeComments = new RemoveComments(line, commentLevel);
                    line = removeComments.Line;
                    commentLevel = removeComments.CommentLevel;
                    if ((String.IsNullOrWhiteSpace(line.Trim())) || (commentLevel > 0))
                    {

                    }
                    else if (line.StartsWith("#"))
                    {

                    }
                    else if (line.Trim().Contains("{"))
                    {
                        fileContents[i] = string.Format("{0}", IndentLine(fileContents[i], indent));
                        inIfNoBraces = false;
                        if (!line.Trim().Contains("}")) indent++;
                    }
                    else if ((line.Trim().EndsWith("}")) || (line.Trim().EndsWith("},")) || (line.Trim().EndsWith("};")))
                    {
                        indent--;
                        fileContents[i] = string.Format("{0}", IndentLine(fileContents[i], indent));
                        inIfNoBraces = false;
                        if (inIf > 0) inIf--;
                    }
                    else if ((line.Trim().StartsWith("if ")) || (line.Trim().StartsWith("if(")))
                    {
                        fileContents[i] = string.Format("{0}", IndentLine(fileContents[i], indent));
                        if ((line.Trim().EndsWith(",")) && !inContinue)
                        {
                            indent++;
                            inContinue = true;
                        }
                        inIf++;
                        inIfNoBraces = true;
                    }
                    else if (line.Trim().StartsWith("else")) 
                    {
                        fileContents[i] = string.Format("{0}", IndentLine(fileContents[i], indent));
                        if ((line.Trim().EndsWith(",")) && !inContinue)
                        {
                            indent++;
                            inContinue = true;
                        }
                        inIf++;
                        inIfNoBraces = true;
                    }
                    else if ((line.Trim().StartsWith("case ")) || (line.Trim().StartsWith("default:")))
                    {
                        inCase++;
                        fileContents[i] = string.Format("{0}", IndentLine(fileContents[i], indent));
                        indent++;
                    }
                    //                    else if ((fileContents[i].Trim().StartsWith("break;") && inCase))
                    else if (line.Trim().StartsWith("break;"))
                    {
                        //                        inCase = false;
                        fileContents[i] = string.Format("{0}", IndentLine(fileContents[i], indent));
                        if (inCase != 0)
                        {
                            inCase--;
                            indent--;
                        }
                    }
                    else if ((line.Trim().EndsWith(",")) && !inContinue)
                    {
                        fileContents[i] = string.Format("{0}", IndentLine(fileContents[i], indent));
                        indent++;
                        inContinue = true;
                    }
                    else if ((line.Trim().EndsWith(")")) && inContinue)
                    {
                        fileContents[i] = string.Format("{0}", IndentLine(fileContents[i], indent));
                        indent--;
                        inContinue = false;
                    }
                    else if ((line.Trim().EndsWith(";")) && inContinue)
                    {
                        fileContents[i] = string.Format("{0}", IndentLine(fileContents[i], indent));
                        indent--;
                        inContinue = false;
                    }
                    else
                    {
                        if ((inIf > 0) && !inContinue)
                        {
                            fileContents[i] = string.Format("{0}", IndentLine(fileContents[i], indent + 1));
                            inIf--;
                        }
                        else
                            fileContents[i] = string.Format("{0}", IndentLine(fileContents[i], indent));
                    }
                }
            }
        }
        private void BrowseFileButton_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.InitialDirectory = ProjectDirectoryTextBox.Text;
            ofd.Filter = ".c files (*.c)|*.c|All files (*.*)|*.*";
            ofd.FilterIndex = 1;
            if (!String.IsNullOrEmpty(SourceCodeTextBox.Text))
            {
                if (!SourceCodeTextBox.Text.TrimStart().StartsWith("\""))
                {
                    string sourceFileName = SourceCodeTextBox.Text;
                    while (sourceFileName.StartsWith("\\")) sourceFileName = sourceFileName.Substring(1);
                    string[] parts = sourceFileName.Split(new char[] { '\\' });
                    for (int i = 0; i < (parts.Count() - 1); i++)
                    {
                        ofd.InitialDirectory = Path.Combine(ofd.InitialDirectory, parts[i]);
                    }
                }
                else
                    SourceCodeTextBox.Clear();
            }
            ofd.DefaultExt = ".c";
            ofd.CheckFileExists = true;
            ofd.CheckPathExists = true;
            ofd.Multiselect = true;
            ofd.ReadOnlyChecked = true;
            if (!string.IsNullOrEmpty(SourceCodeTextBox.Text))
                ofd.FileName = (new FileInfo(SourceCodeTextBox.Text)).Name;
            ofd.Title = "Choose Source File to Analyze...";
            DialogResult dialogResult = ofd.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                RefactorButton.Enabled = true;
                foreach (string fileName in ofd.FileNames)
                {
                    string sourceFileName = fileName;
                    if (sourceFileName.ToUpper().StartsWith(ProjectDirectoryTextBox.Text.ToUpper()))
                    {
                        sourceFileName = sourceFileName.Substring(ProjectDirectoryTextBox.Text.Length);
                        while (sourceFileName.StartsWith("\\")) sourceFileName = sourceFileName.Substring(1);
                        if (ofd.FileNames.Length == 1) SourceCodeTextBox.Text = sourceFileName;
                        else SourceCodeTextBox.Text = string.Format("{0} \"{1}\"", SourceCodeTextBox.Text, sourceFileName);
                    }
                    else
                    {
                        MessageBox.Show("The source file has to be in the directory path specified by Project Directory.");
                        BrowseFileButton_Click((object)null, (EventArgs)null);
                        RefactorButton.Enabled = false;
                    }
                }
            }
            ofd.Dispose();
        }
        private void SourceCodeTextBox_DragDrop(object sender, DragEventArgs e)
        {
            int iNum = 0;
            string sFileName = "";

            foreach (string fileName in (string[])e.Data.GetData(DataFormats.FileDrop))
            {
                if (iNum == 0) sFileName = fileName;
                iNum++;
            }
            if (iNum == 1)
            {
                FileInfo fi = new FileInfo(sFileName);
                if (fi.DirectoryName.StartsWith(ProjectDirectoryTextBox.Text, StringComparison.CurrentCultureIgnoreCase))
                {
                    this.SourceCodeTextBox.Text = fi.FullName.Substring(ProjectDirectoryTextBox.Text.Length);
                    if (this.SourceCodeTextBox.Text.StartsWith("\\")) this.SourceCodeTextBox.Text = this.SourceCodeTextBox.Text.Substring(1);
                    RefactorButton.Enabled = true;
                }
                else
                {
                    MessageBox.Show("You can only add files from the project specified in the Project Directory field.");
                    RefactorButton.Enabled = false;
                }
            }
            else
            {
                performSourceTextChanged = false;
                bool finalCheck = true;
                foreach (string fileName in (string[])e.Data.GetData(DataFormats.FileDrop))
                {
                    FileInfo fi = new FileInfo(fileName);
                    if (fi.DirectoryName.StartsWith(ProjectDirectoryTextBox.Text, StringComparison.CurrentCultureIgnoreCase))
                    {
                        string subString = fi.FullName.Substring(ProjectDirectoryTextBox.Text.Length);
                        if (subString.StartsWith("\\")) subString = subString.Substring(1);
                        this.SourceCodeTextBox.Text = String.Format("{0} \"{1}\"", this.SourceCodeTextBox.Text, subString);
                        if (finalCheck) RefactorButton.Enabled = true;
                    }
                    else
                    {
                        MessageBox.Show("You can only add files from the project specified in the Project Directory field.");
                        RefactorButton.Enabled = false;
                        finalCheck = false;
                    }
                }
                performSourceTextChanged = true;
                if (finalCheck) SourceCodeTextBox_TextChanged(SourceCodeTextBox, (EventArgs)null);
            }
        }
        private void SourceCodeTextBox_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop, false))
            {
                e.Effect = DragDropEffects.All;
            }
        }
        private void SourceCodeTextBox_TextChanged(object sender, EventArgs e)
        {
            if (performSourceTextChanged)
            {
                RefactorButton.Enabled = false;
                TextBox textBox = (TextBox)sender;
                if (!String.IsNullOrEmpty(textBox.Text))
                {
                    string[] files = textBox.Text.Split(new char[] { '\"' });
                    foreach (string SourceFileName in files)
                    {
                        if (!String.IsNullOrEmpty(SourceFileName.Trim()))
                        {
                            string sourceFileName = Path.Combine(ProjectDirectoryTextBox.Text, SourceFileName);
                            if (File.Exists(sourceFileName))
                            {
                                RefactorButton.Enabled = true;
                            }
                            else
                                RefactorButton.Enabled = false;
                        }
                    }
                }
                else
                    RefactorButton.Enabled = false;
            }
        }
        private void ExitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void Form1_Resize(object sender, EventArgs e)
        {
            Size size = this.Size;
            LogTextBox.Size = new Size(size.Width - 327, size.Height - 254);
        }

        private void RefactorButton_Click(object sender, EventArgs e)
        {
            string[] files = SourceCodeTextBox.Text.Split(new char[] { '\"' });
            LogTextBox.Clear();
            foreach (string SourceFileName in files)
            {
                if (!String.IsNullOrEmpty(SourceFileName.Trim()))
                {
                    string sourceFileName = Path.Combine(ProjectDirectoryTextBox.Text, SourceFileName);
                    if (File.Exists(sourceFileName))
                    {
                        AddLineToTextBox(string.Format("Working on \"{0}\" ...", sourceFileName), LogTextBox);
                        List<string> fileContents = ReadFile(sourceFileName);
                        if (fileContents.Count() > 0)
                        {
                            AddStatement(fileContents, "#include <stdio.h>", ADDFLAG.Beginning);
                            if (stdlibCheckBox.Checked)
                            {
                                AddStatement(fileContents, "#include <stdlib.h>", "#include <stdio.h>");
                                if (stringCheckBox.Checked)
                                    AddStatement(fileContents, "#include <string.h>", "#include <stdlib.h>");
                            }
                            else if (stringCheckBox.Checked)
                                AddStatement(fileContents, "#include <string.h>", "#include <stdio.h>");
                            if (ScreenCheckBox.Checked)
                            {
                                if (!ReplaceStatement(fileContents, "#include \"scratch.h\"", "#include \"Screen.h\""))
                                {
                                    if (!ReplaceStatement(fileContents, "#include \"Scratch.h\"", "#include \"Screen.h\""))
                                    {
                                        AddStatement(fileContents, "#include \"Screen.h\"", ADDFLAG.BeforeFirstBreak);
                                    }
                                }
                            }
                            RemoveStatement(fileContents, "#include \"scratch.h\"");
                            RemoveStatement(fileContents, "#include \"Scratch.h\"");
                            if (GUIFuncsCheckBox.Checked)
                                AddStatement(fileContents, "#include \"GUIFuncs.h\"", ADDFLAG.BeforeFirstBreak);
                            AddStatement(fileContents, "#include \"object.h\"", ADDFLAG.BeforeFirstBreak);
                            if (DataObjectCheckBox.Checked)
                            {
                                AddStatement(fileContents, "#include \"DataObject.h\"", "#include \"object.h\"");
                                if (TableObjectCheckBox.Checked)
                                    AddStatement(fileContents, "#include \"TableObject.h\"", "#include \"DataObject.h\"");
                            }
                            else if (TableObjectCheckBox.Checked)
                                AddStatement(fileContents, "#include \"TableObject.h\"", "#include \"object.h\"");
                            RemoveStatement(fileContents, "#include <strings.h>");
                            RemoveStatement(fileContents, "#include \"ISQO.h\"");
                            if (UnmanagedCodeWrapperCheckBox.Checked)
                                AddStatement(fileContents, "#include \"UnmanagedCodeWrapper.h\"", ADDFLAG.AfterAllIncludes);
                            if (PrettifyCheckBox.Checked)
                            {
                                ReplaceText(fileContents, "( ", "(");
                                ReplaceText(fileContents, " )", ")");
                                PrettifyCode(fileContents);
                            }
                            WriteFile(fileContents, sourceFileName);
                            AddLineToTextBox("C source code prettified...", LogTextBox);
                            RefactorButton.Enabled = false;
                        }
                    }
                    AddLineToTextBox(" ", LogTextBox);
                }
            }
        }
    }
}
