﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace WrapStringCharArrays
{
	public partial class WorkHorse : Form
	{
		private int percentageDone;
		private int filePercentageDone;
		private BackgroundWorker backGroundWorker;
		private Cursor currentCursor;
		private Timer MyTimer;

		private string rootDirectory;
		private TextBox logTextBox;

		private ArrayList args;
		private Task doWhat;

		public List<string> FileList { get; private set; }		
		public enum Task
		{
			Nothing = 0,
			Wrap = 1
		}
		public WorkHorse()
		{
			doWhat = Task.Nothing;
			args = (ArrayList)null;
			InitializeComponent();
			InitializeBackgroundWorker();
			SetUpWorkProgressBar(0, 100, 0);
			SetUpFileProgressBar(string.Empty, 0, 100, 0);
		}
		public WorkHorse(Task taskIn, List<string> fileList, string rootDirectoryIn, TextBox logTextBoxIn)
		{
			if (taskIn == Task.Wrap)
			{
				rootDirectory = rootDirectoryIn;
				logTextBox = logTextBoxIn;
				doWhat = taskIn;
				args = new ArrayList();
				args.Add(fileList);
				args.Add(rootDirectoryIn);
				InitializeComponent();
				InitializeBackgroundWorker();
				SetUpWorkProgressBar(0, fileList.Count, 0);
				SetUpFileProgressBar(string.Empty, 0, 100, 0);
			}
		}
		protected override void OnShown(EventArgs e)
		{
			if (doWhat != Task.Nothing)
			{
				MyTimer = new Timer();
				MyTimer.Tick += new EventHandler(MyTimer_Tick);
				MyTimer.Start();
				backGroundWorker.RunWorkerAsync(new object[] { doWhat, args });
			}
			base.OnShown(e);
			CustomizeProgressBarWindow(doWhat);
		}
		private void InitializeBackgroundWorker()
		{
			backGroundWorker = new BackgroundWorker();
			backGroundWorker.WorkerSupportsCancellation = true;
			backGroundWorker.WorkerReportsProgress = true;
			backGroundWorker.DoWork += new DoWorkEventHandler(backGroundWorker_DoWork);
			backGroundWorker.ProgressChanged += new ProgressChangedEventHandler(backGroundWorker_ProgressChanged);
			backGroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backGroundWorker_RunWorkerCompleted);
		}
		public void CustomizeProgressBarWindow(Task task)
		{
			if (task == Task.Wrap)
			{
				this.Size = new Size(1018, 269);
				FileProgressBar.Visible = true;
				FileProgressBarLabel.Visible = true;
				FileProgressBarLabel.Text = string.Empty;
				OperationCancelButton.Location = new Point(464, 170);
			}
			else
			{
				this.Size = new Size(1018, 192);
				FileProgressBar.Visible = false;
				FileProgressBarLabel.Visible = false;
				OperationCancelButton.Location = new Point(464, 80);
			}
		}
		public void SetUpWorkProgressBar(int min, int max, int current)
		{
			// Set up the progress bar
			WorkProgressBar.Minimum = min;
			WorkProgressBar.Maximum = current < max ? max : current;
			WorkProgressBar.Value = current;
		}
		public void SetUpFileProgressBar(string fileName, int min, int max, int current)
		{
			// Set up the progress bar
			FileProgressBarLabel.Text = fileName;
			FileProgressBar.Minimum = min;
			FileProgressBar.Maximum = current < max ? max : current;
			FileProgressBar.Value = current;
			filePercentageDone = 0;
		}
		private void backGroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			if (e.UserState == null)
			{
				filePercentageDone = e.ProgressPercentage;
			}
			else if (e.UserState.GetType() == typeof(List<string>))
			{
				percentageDone = e.ProgressPercentage;
				AddLinesToTextBox((List<string>)e.UserState, logTextBox);
			}
			else
			{
				ThreadUserState userState = (ThreadUserState)e.UserState;
				if (userState.Purpose == ThreadUserState.State.Setup)
				{
					percentageDone = 0;
					SetUpWorkProgressBar(userState.Minimum, userState.Maximum, 0);
					this.Text = userState.TitleText;
					filePercentageDone = 0;
					FileProgressBarLabel.Text = userState.FileLabel;
					FileProgressBar.Minimum = userState.FileMinimum;
					FileProgressBar.Maximum = userState.FileMaximum;
					FileProgressBar.Value = userState.FileCurrent;
				}
				else if (userState.Purpose == ThreadUserState.State.UpdateLimits)
				{
					SetUpWorkProgressBar(userState.Minimum, userState.Maximum, userState.Current);
					SetUpFileProgressBar(userState.FileLabel, userState.FileMinimum, userState.FileMaximum, userState.FileCurrent);
				}
				else if (userState.Purpose == ThreadUserState.State.UpdateTitle)
				{
					this.Text = userState.TitleText;
				}
			}
		}
		private void backGroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			// First, stop the timer
			MyTimer.Stop();
			// Next, handle the case where an exception was thrown.
			if (e.Error != null)
			{
				this.DialogResult = DialogResult.Abort;
				System.Windows.Forms.MessageBox.Show(e.Error.Message + "\n" + e.Error.StackTrace);
				this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			}
			else if (e.Cancelled) // Now see if the background worker was cancelled
			{
				this.DialogResult = DialogResult.Cancel;
				this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			}
			else      // if we got here, things seem to be fine.
			{
				WorkProgressBar.Value = WorkProgressBar.Maximum;
				this.Update();
				this.DialogResult = DialogResult.OK;
			}
			this.Close();
		}
		private void MyTimer_Tick(Object myObject, EventArgs myEventArgs)
		{
			if (percentageDone <= WorkProgressBar.Maximum)
				WorkProgressBar.Value = percentageDone;
			if ((FileProgressBar.Visible) && (filePercentageDone <= FileProgressBar.Maximum))
			{
				FileProgressBar.Value = filePercentageDone;
			}
			this.Update();
		}
		private void OperationCancelButton_Enter(object sender, EventArgs e)
		{
			currentCursor = this.Cursor;
			this.Cursor = Cursors.Arrow;
		}
		private void OperationCancelButton_Leave(object sender, EventArgs e)
		{
			this.Cursor = currentCursor;
		}
		private void OperationCancelButton_Click(object sender, EventArgs e)
		{
			backGroundWorker.CancelAsync();
			OperationCancelButton.Enabled = false;
		}
		private void backGroundWorker_DoWork(object sender, DoWorkEventArgs e)
		{
			// Get the BackgroundWorker that raised this event.
			BackgroundWorker worker = sender as BackgroundWorker;
			Object[] args = (Object[])e.Argument;
			e = DoIt((Task)args[0], (ArrayList)args[1], e);
			if (e.Result != null) throw (SystemException)e.Result;
		}
		private DoWorkEventArgs DoIt(Task doWhat, ArrayList args, DoWorkEventArgs retVal)
		{
			string sDelimiter = ",";
			char[] cDelimiter = sDelimiter.ToCharArray();
			string line = string.Empty;

			//            try
			{
				switch (doWhat)
				{
					case Task.Wrap:
						line = "Load";
						WrapText((List<string>)args[0], (string)args[1]);
						break;
				}
			}
			return retVal;
		}
		private List<string> WrapText(List<string> fileList, string rootDirectory)
		{
			OperationCancelButton.Enabled = true;
			ThreadUserState userState = new ThreadUserState();
			userState.Purpose = ThreadUserState.State.Setup;
			userState.TitleText = "Wrap Strings in Text...";
			userState.Minimum = 0;
			userState.Maximum = fileList.Count;
			backGroundWorker.ReportProgress(0, (object)userState);
			int counter = 0;
			for (int i = 0; i < fileList.Count; i++)
			{
				try
				{
					string sourceFileName = Path.Combine(rootDirectory, fileList[i]);
					if (File.Exists(sourceFileName))
					{
						List<string> fileContents = ReadFile(sourceFileName);
						List<string> textChanges = new List<string>();
						textChanges.Add(string.Format("Working on file: {0}", fileList[i]));
						textChanges.Add(string.Empty);
						userState.FileLabel = fileList[i];
						userState.Minimum = 0;
						userState.Maximum = fileContents.Count;
						backGroundWorker.ReportProgress(0, (object)userState);
						ThreadUserState fileState = new ThreadUserState();
						fileState.Purpose = ThreadUserState.State.FileReport;
						if (fileContents.Count() > 0)
						{
							int numberChanges = 0;
							for (int j = 0; j < fileContents.Count; j++)
							{
								string changes = string.Empty;
								string line = RegexFixIt(fileContents[j]);
								if (String.Compare(fileContents[j], line) != 0)
								{
									textChanges.Add(String.Format("Line:\n\"{0}\" changed to\n\"{1}\"", fileContents[i].TrimStart(), line.TrimStart()));
									fileContents[j] = line;
									numberChanges++;
								}
								counter = j;
								backGroundWorker.ReportProgress(j);

							}
							if (numberChanges > 0)
							{
								textChanges.Add(AddStatement(fileContents, fileList[i], "#include \"Stubs.h\"", "#include \"UnmanagedCodeWrapper.h\""));
								textChanges.Add(string.Format("A total of {0} changes were made to the file.\n\n\n", numberChanges + 1));
								WriteFile(fileContents, sourceFileName);
							}
						}
						backGroundWorker.ReportProgress(i+1, (object)textChanges);
					}
				}
				catch (System.Exception ex)
				{
					MessageBox.Show(ex.Message);
				}
			}
			return fileList;
		}
		private void AddLineToTextBox(string line, TextBox textBox)
		{
			string[] activityText = textBox.Lines;
			List<string> activityList = new List<string>(activityText);
			activityList.Add(line);
			textBox.Lines = activityList.ToArray<string>();
			textBox.SelectionStart = textBox.TextLength;
			textBox.ScrollToCaret();
		}
		private void AddLinesToTextBox(List<string> lines, TextBox textBox)
		{
			string[] activityText = textBox.Lines;
			List<string> activityList = new List<string>(activityText);
			activityList.AddRange(lines);
			textBox.Lines = activityList.ToArray<string>();
		}
		string GetExceptionMessage(string message, System.Exception exception)
		{
			string text = string.Format("{0}\n{1}", message, exception.Message);
			System.Exception inner = exception.InnerException;
			while (inner != null)
			{
				message = string.Format("{0}\n{1}", message, inner.Message);
				inner = inner.InnerException;
			}
			return text;
		}
		private bool SkipItAnyway(string text)
		{
			bool retval = false;
			int index = 0;
			while ((index < text.Length) && (text.Substring(index, 1) == " ")) index++;
			if ((index < text.Length) && (text.Substring(index, 1) == "=")) retval = true;
			return retval;
		}
		private string RegexFixIt(string line)
		{
			string skipMatchString = string.Format("strcpy[ ]*\\([ ]*p[A-Z][A-Z]*->[A-Za-z0-9._]*\\[[0,ijk]\\][ ,)]");
			string matchString1 = string.Format("p[A-Z][A-Z]*->[A-Za-z0-9._]*\\[[0a-z]\\][ ,;)]");
			string matchString2 = string.Format("p[A-Z][A-Z]*->[A-Za-z0-9._]*\\[0\\]\\[0\\][ ]*[!=]=[ ]*'\\0'");
			string newLine = line;
			Match skipMatch = Regex.Match(newLine, skipMatchString);
			if (!skipMatch.Success)
			{
				Match match2 = Regex.Match(newLine, matchString2);
				if (match2.Success)
				{
				}
				else
				{
					Match match1 = Regex.Match(newLine, matchString1);
					while (match1.Success)
					{
						if (!SkipItAnyway(newLine.Substring(match1.Index + match1.Length)))
						{
							Match bMatch = Regex.Match(match1.Value, "\\[[0a-z]\\]");
							if (bMatch.Success)
							{
								string insert = string.Format("StringArrayNullCheck({0}, {1}, \"\")", match1.Value.Substring(0, match1.Length - 4), bMatch.Value.Substring(1, 1));
								string part1 = newLine.Substring(0, match1.Index);
								string part2 = newLine.Substring(match1.Index + match1.Length - 1);
								newLine = string.Format("{0}{1}{2}", part1, insert, part2);
								match1 = Regex.Match(newLine, matchString1);
							}
							else break;
						}
						else
							break;
					}
				}
			}
			return newLine;
		}
		private string RegexFixCase2(string line)
		{
			string matchString = string.Format("p[A-Z][A-Z]*->[A-Za-z0-9._]*\\[[0]\\]\\[[0]\\][ ]*[!=]= '\0'");
			string newLine = line;
			Match match = Regex.Match(newLine, matchString);
			while (match.Success)
			{
				if (!SkipItAnyway(newLine.Substring(match.Index + match.Length)))
				{
					Match bMatch = Regex.Match(match.Value, "\\[[0ijk]\\]");
					if (bMatch.Success)
					{
						string insert = string.Format("StringArrayNullCheck({0}, {1}, \"\")", match.Value.Substring(0, match.Length - 4), bMatch.Value.Substring(1, 1));
						string part1 = newLine.Substring(0, match.Index);
						string part2 = newLine.Substring(match.Index + match.Length - 1);
						newLine = string.Format("{0}{1}{2}", part1, insert, part2);
						match = Regex.Match(newLine, matchString);
					}
					else break;
				}
			}
			return newLine;
		}
		List<string> ReadFile(string fileName)
		{
			List<string> contents = new List<string>();
			try
			{
				StreamReader streamReader = new StreamReader(fileName);
				while (!streamReader.EndOfStream)
				{
					string line = streamReader.ReadLine();
					contents.Add(line);
				}
				streamReader.Close();
			}
			catch (System.Exception ex)
			{
				string message = GetExceptionMessage(string.Format("Error opening or reading \"{0}\"", fileName), ex);
				MessageBox.Show(message);
			}
			return contents;
		}
		private string WriteFile(List<string> contents, string fileName)
		{
			string retval = string.Empty;
			try
			{
				StreamWriter streamWriter = new StreamWriter(fileName, false);
				foreach (string line in contents)
					streamWriter.WriteLine(line);
				streamWriter.Close();
			}
			catch (System.Exception ex)
			{
				string message = GetExceptionMessage(string.Format("Error opening or writing to {0}.", fileName), ex);
				MessageBox.Show(message);
				retval = string.Format("Failed to write {0}", fileName);
			}
			return retval;
		}
		public string GetStartingWhiteSpace(string line)
		{
			string retval = string.Empty;
			int i = 0;
			while ((i < line.Length) && (string.IsNullOrWhiteSpace(line.Substring(i, 1)))) i++;
			if (i > 0)
				retval = line.Substring(0, i);
			return retval;
		}
		private bool FindStatement(string statement, List<string> fileContents)
		{
			bool retval = false;
			foreach (string line in fileContents)
			{
				if (line.Trim() == statement.Trim())
				{
					retval = true;
					break;
				}
			}
			return retval;
		}
		private string AddStatement(List<string> fileContents, string fileName, string statement, string beforeStatement)
		{
			string retval = string.Empty;
			if (!FindStatement(statement, fileContents))
			{
				int index = 0;
				while ((index < fileContents.Count()) && (!fileContents[index].Contains(beforeStatement))) index++;
				if (index < fileContents.Count())
				{
					string whiteSpaces = GetStartingWhiteSpace(fileContents[index]);
					string line = string.Format("{0}{1}", whiteSpaces, statement);
					fileContents.Insert(index, line);
					retval = string.Format("\"{0}\" added before \"{1}\"", line, fileContents[index + 1]);
				}
				else
				{
					MessageBox.Show(String.Format("No statement \"{0}\" found in file \"{1}\".\n\"{2}\" not added.", beforeStatement, fileName, statement));
					retval = String.Format("No statement \"{0}\" found in file\"{1}\".\n\"{2}\" not added.", beforeStatement, fileName, statement);
				}
			}
			return retval;
		}
	}

	public class BackgroundWorkerCancelledException : System.ApplicationException
	{
		public BackgroundWorkerCancelledException() { }
		public BackgroundWorkerCancelledException(string message) { }
		public BackgroundWorkerCancelledException(string message, System.Exception inner) { }

		// Constructor needed for serialization 
		// when exception propagates from a remoting server to the client.
		protected BackgroundWorkerCancelledException(System.Runtime.Serialization.SerializationInfo info,
			System.Runtime.Serialization.StreamingContext context)
		{ }
	}

}
