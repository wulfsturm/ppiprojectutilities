﻿namespace WrapStringCharArrays
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BrowseSourceCodeButton = new System.Windows.Forms.Button();
            this.SourceCodeTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.BrowseProjectDirectoryButton = new System.Windows.Forms.Button();
            this.ProjectDirectoryTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.LogTextBox = new System.Windows.Forms.TextBox();
            this.ExitButton = new System.Windows.Forms.Button();
            this.WrapCodeButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BrowseSourceCodeButton
            // 
            this.BrowseSourceCodeButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BrowseSourceCodeButton.BackgroundImage = global::WrapStringCharArrays.Properties.Resources.browse;
            this.BrowseSourceCodeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BrowseSourceCodeButton.FlatAppearance.BorderSize = 0;
            this.BrowseSourceCodeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BrowseSourceCodeButton.Location = new System.Drawing.Point(849, 65);
            this.BrowseSourceCodeButton.Name = "BrowseSourceCodeButton";
            this.BrowseSourceCodeButton.Size = new System.Drawing.Size(47, 42);
            this.BrowseSourceCodeButton.TabIndex = 26;
            this.BrowseSourceCodeButton.Tag = "Input";
            this.BrowseSourceCodeButton.UseVisualStyleBackColor = true;
            this.BrowseSourceCodeButton.Click += new System.EventHandler(this.BrowseFolderButton_Click);
            // 
            // SourceCodeTextBox
            // 
            this.SourceCodeTextBox.AllowDrop = true;
            this.SourceCodeTextBox.Location = new System.Drawing.Point(148, 73);
            this.SourceCodeTextBox.Name = "SourceCodeTextBox";
            this.SourceCodeTextBox.Size = new System.Drawing.Size(691, 26);
            this.SourceCodeTextBox.TabIndex = 25;
            this.SourceCodeTextBox.TextChanged += new System.EventHandler(this.SourceCodeTextBox_TextChanged);
            this.SourceCodeTextBox.DragDrop += new System.Windows.Forms.DragEventHandler(this.SourceCodeTextBox_DragDrop);
            this.SourceCodeTextBox.DragEnter += new System.Windows.Forms.DragEventHandler(this.SourceCodeTextBox_DragEnter);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(35, 76);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 20);
            this.label3.TabIndex = 24;
            this.label3.Text = "Source Code";
            // 
            // BrowseProjectDirectoryButton
            // 
            this.BrowseProjectDirectoryButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BrowseProjectDirectoryButton.BackgroundImage = global::WrapStringCharArrays.Properties.Resources.browse;
            this.BrowseProjectDirectoryButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BrowseProjectDirectoryButton.FlatAppearance.BorderSize = 0;
            this.BrowseProjectDirectoryButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BrowseProjectDirectoryButton.Location = new System.Drawing.Point(849, 16);
            this.BrowseProjectDirectoryButton.Name = "BrowseProjectDirectoryButton";
            this.BrowseProjectDirectoryButton.Size = new System.Drawing.Size(47, 42);
            this.BrowseProjectDirectoryButton.TabIndex = 23;
            this.BrowseProjectDirectoryButton.Tag = "ProjectDirectory";
            this.BrowseProjectDirectoryButton.UseVisualStyleBackColor = true;
            this.BrowseProjectDirectoryButton.Click += new System.EventHandler(this.BrowseFolderButton_Click);
            // 
            // ProjectDirectoryTextBox
            // 
            this.ProjectDirectoryTextBox.Location = new System.Drawing.Point(148, 24);
            this.ProjectDirectoryTextBox.Name = "ProjectDirectoryTextBox";
            this.ProjectDirectoryTextBox.Size = new System.Drawing.Size(691, 26);
            this.ProjectDirectoryTextBox.TabIndex = 22;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 27);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 20);
            this.label1.TabIndex = 21;
            this.label1.Text = "Project Directory";
            // 
            // LogTextBox
            // 
            this.LogTextBox.Location = new System.Drawing.Point(148, 166);
            this.LogTextBox.Multiline = true;
            this.LogTextBox.Name = "LogTextBox";
            this.LogTextBox.ReadOnly = true;
            this.LogTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.LogTextBox.Size = new System.Drawing.Size(691, 355);
            this.LogTextBox.TabIndex = 29;
            // 
            // ExitButton
            // 
            this.ExitButton.Location = new System.Drawing.Point(403, 115);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(104, 36);
            this.ExitButton.TabIndex = 28;
            this.ExitButton.Text = "Exit";
            this.ExitButton.UseVisualStyleBackColor = true;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // WrapCodeButton
            // 
            this.WrapCodeButton.Location = new System.Drawing.Point(175, 115);
            this.WrapCodeButton.Name = "WrapCodeButton";
            this.WrapCodeButton.Size = new System.Drawing.Size(104, 36);
            this.WrapCodeButton.TabIndex = 27;
            this.WrapCodeButton.Text = "Wrap Code";
            this.WrapCodeButton.UseVisualStyleBackColor = true;
            this.WrapCodeButton.Click += new System.EventHandler(this.WrapCodeButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 562);
            this.Controls.Add(this.LogTextBox);
            this.Controls.Add(this.ExitButton);
            this.Controls.Add(this.WrapCodeButton);
            this.Controls.Add(this.BrowseSourceCodeButton);
            this.Controls.Add(this.SourceCodeTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.BrowseProjectDirectoryButton);
            this.Controls.Add(this.ProjectDirectoryTextBox);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "Put Wrapper Around String Arrays";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BrowseSourceCodeButton;
        private System.Windows.Forms.TextBox SourceCodeTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button BrowseProjectDirectoryButton;
        private System.Windows.Forms.TextBox ProjectDirectoryTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox LogTextBox;
        private System.Windows.Forms.Button ExitButton;
        private System.Windows.Forms.Button WrapCodeButton;
    }
}

