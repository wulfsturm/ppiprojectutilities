﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace WrapStringCharArrays
{
    public partial class Form1 : Form
    {
        private bool performSourceTextChanged;
        public Form1()
        {
            InitializeComponent();
            WrapCodeButton.Enabled = false;
            performSourceTextChanged = true;
            ProjectDirectoryTextBox.Text = Properties.Settings.Default.ProjectDirectory;
            this.Icon = Properties.Resources.icon;
        }
        string GetExceptionMessage(string message, System.Exception exception)
        {
            string text = string.Format("{0}\n{1}", message, exception.Message);
            System.Exception inner = exception.InnerException;
            while (inner != null)
            {
                message = string.Format("{0}\n{1}", message, inner.Message);
                inner = inner.InnerException;
            }
            return text;
        }
        private bool SkipItAnyway(string text)
        {
            bool retval = false;
            int index = 0;
            while ((index < text.Length) && (text.Substring(index, 1) == " ")) index++;
            if ((index < text.Length) && (text.Substring(index, 1) == "=")) retval = true;
            return retval;
        }
        private string RegexFixIt(string line)
        {
            string skipMatchString = string.Format("strcpy[ ]*\\([ ]*p[A-Z][A-Z]*->[A-Za-z0-9._]*\\[[0,ijk]\\][ ,)]");
            string matchString1 = string.Format("p[A-Z][A-Z]*->[A-Za-z0-9._]*\\[[0a-z]\\][ ,;)]");
            string matchString2 = string.Format("p[A-Z][A-Z]*->[A-Za-z0-9._]*\\[0\\]\\[0\\][ ]*[!=]=[ ]*'\\0'");
            string newLine = line;
            Match skipMatch = Regex.Match(newLine, skipMatchString);
            if (!skipMatch.Success)
            {
                Match match2 = Regex.Match(newLine, matchString2);
                if (match2.Success)
                {
                }
                else
                {
                    Match match1 = Regex.Match(newLine, matchString1);
                    while (match1.Success)
                    {
                        if (!SkipItAnyway(newLine.Substring(match1.Index + match1.Length)))
                        {
                            Match bMatch = Regex.Match(match1.Value, "\\[[0a-z]\\]");
                            if (bMatch.Success)
                            {
                                string insert = string.Format("StringArrayNullCheck({0}, {1}, \"\")", match1.Value.Substring(0, match1.Length - 4), bMatch.Value.Substring(1, 1));
                                string part1 = newLine.Substring(0, match1.Index);
                                string part2 = newLine.Substring(match1.Index + match1.Length - 1);
                                newLine = string.Format("{0}{1}{2}", part1, insert, part2);
                                match1 = Regex.Match(newLine, matchString1);
                            }
                            else break;
                        }
                        else
                            break;
                    }
                }
            }
            return newLine;
        }
        private string RegexFixCase2(string line)
        {
            string matchString = string.Format("p[A-Z][A-Z]*->[A-Za-z0-9._]*\\[[0]\\]\\[[0]\\][ ]*[!=]= '\0'");
            string newLine = line;
            Match match = Regex.Match(newLine, matchString);
            while (match.Success)
            {
                if (!SkipItAnyway(newLine.Substring(match.Index + match.Length)))
                {
                    Match bMatch = Regex.Match(match.Value, "\\[[0ijk]\\]");
                    if (bMatch.Success)
                    {
                        string insert = string.Format("StringArrayNullCheck({0}, {1}, \"\")", match.Value.Substring(0, match.Length - 4), bMatch.Value.Substring(1, 1));
                        string part1 = newLine.Substring(0, match.Index);
                        string part2 = newLine.Substring(match.Index + match.Length - 1);
                        newLine = string.Format("{0}{1}{2}", part1, insert, part2);
                        match = Regex.Match(newLine, matchString);
                    }
                    else break;
                }
            }
            return newLine;
        }
        List<string> ReadFile(string fileName)
        {
            List<string> contents = new List<string>();
            try
            {
                StreamReader streamReader = new StreamReader(fileName);
                while (!streamReader.EndOfStream)
                {
                    string line = streamReader.ReadLine();
                    contents.Add(line);
                }
                streamReader.Close();
            }
            catch (System.Exception ex)
            {
                string message = GetExceptionMessage(string.Format("Error opening or reading \"{0}\"", fileName), ex);
                MessageBox.Show(message);
            }
            return contents;
        }
        private string WriteFile(List<string> contents, string fileName)
        {
            string retval = string.Empty;
            try
            {
                StreamWriter streamWriter = new StreamWriter(fileName, false);
                foreach (string line in contents)
                    streamWriter.WriteLine(line);
                streamWriter.Close();
            }
            catch (System.Exception ex)
            {
                string message = GetExceptionMessage(string.Format("Error opening or writing to {0}.", fileName), ex);
                MessageBox.Show(message);
                retval = string.Format("Failed to write {0}", fileName);
            }
            return retval;
        }
        private void AddLinesToTextBox(List<string> lines, TextBox textBox)
        {
            string[] activityText = textBox.Lines;
            List<string> activityList = new List<string>(activityText);
            activityList.AddRange(lines);
            textBox.Lines = activityList.ToArray<string>();
        }
        private void AddLineToTextBox(string line, TextBox textBox)
        {
            string[] activityText = textBox.Lines;
            List<string> activityList = new List<string>(activityText);
            activityList.Add(line);
            textBox.Lines = activityList.ToArray<string>();
            textBox.SelectionStart = textBox.TextLength;
            textBox.ScrollToCaret();
        }
        public string GetStartingWhiteSpace(string line)
        {
            string retval = string.Empty;
            int i = 0;
            while ((i < line.Length) && (string.IsNullOrWhiteSpace(line.Substring(i, 1)))) i++;
            if (i > 0)
                retval = line.Substring(0, i);
            return retval;
        }
        private bool FindStatement(string statement, List<string> fileContents)
        {
            bool retval = false;
            foreach (string line in fileContents)
            {
                if (line.Trim() == statement.Trim())
                {
                    retval = true;
                    break;
                }
            }
            return retval;
        }
        private bool AddStatement(List<string> fileContents, string fileName, string statement, string beforeStatement)
        {
            bool retval = false;
            if (!FindStatement(statement, fileContents))
            {
                    int index = 0;
                    while ((index < fileContents.Count()) && (!fileContents[index].Contains(beforeStatement))) index++;
                    if (index < fileContents.Count())
                    {
                        string whiteSpaces = GetStartingWhiteSpace(fileContents[index]);
                        string line = string.Format("{0}{1}", whiteSpaces, statement);
                        fileContents.Insert(index, line);
                        AddLineToTextBox(string.Format("\"{0}\" added before \"{1}\"", line, fileContents[index + 1]), LogTextBox);
                        retval = true;
                    }
                    else
                    {
                        MessageBox.Show(String.Format("No statement \"{0}\" found in file \"{1}\".\n\"{2}\" not added.", beforeStatement, fileName, statement));
                        AddLineToTextBox(String.Format("No statement \"{0}\" found in file\"{1}\".\n\"{2}\" not added.", beforeStatement, fileName, statement), LogTextBox);
                    }
            }
            return retval;
        }
        private void BrowseFolderButton_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.ShowNewFolderButton = false;
            //			fd.RootFolder = Environment.SpecialFolder.MyComputer;
            if ((string)button.Tag == "ProjectDirectory")
            {
                fd.Description = "Specify the path where the Visual Studio project is";
                fd.SelectedPath = ProjectDirectoryTextBox.Text;
            }
            DialogResult dialogResult = fd.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                if ((string)button.Tag == "ProjectDirectory")
                    ProjectDirectoryTextBox.Text = fd.SelectedPath;
            }
            fd.Dispose();
        }
        private void SourceCodeTextBox_DragDrop(object sender, DragEventArgs e)
        {
            int iNum = 0;
            string sFileName = "";

            foreach (string fileName in (string[])e.Data.GetData(DataFormats.FileDrop))
            {
                if (iNum == 0) sFileName = fileName;
                iNum++;
            }
            if (iNum == 1)
            {
                FileInfo fi = new FileInfo(sFileName);
                if (fi.DirectoryName.StartsWith(ProjectDirectoryTextBox.Text, StringComparison.CurrentCultureIgnoreCase))
                {
                    this.SourceCodeTextBox.Text = fi.FullName.Substring(ProjectDirectoryTextBox.Text.Length);
                    if (this.SourceCodeTextBox.Text.StartsWith("\\")) this.SourceCodeTextBox.Text = this.SourceCodeTextBox.Text.Substring(1);
                    WrapCodeButton.Enabled = true;
                }
                else
                {
                    MessageBox.Show("You can only add files from the project specified in the Project Directory field.");
                    WrapCodeButton.Enabled = false;
                }
            }
            else
            {
                performSourceTextChanged = false;
                bool finalCheck = true;
                foreach (string fileName in (string[])e.Data.GetData(DataFormats.FileDrop))
                {
                    FileInfo fi = new FileInfo(fileName);
                    if (fi.DirectoryName.StartsWith(ProjectDirectoryTextBox.Text, StringComparison.CurrentCultureIgnoreCase))
                    {
                        string subString = fi.FullName.Substring(ProjectDirectoryTextBox.Text.Length);
                        if (subString.StartsWith("\\")) subString = subString.Substring(1);
                        this.SourceCodeTextBox.Text = String.Format("{0} \"{1}\"", this.SourceCodeTextBox.Text, subString);
                        if (finalCheck) WrapCodeButton.Enabled = true;
                    }
                    else
                    {
                        MessageBox.Show("You can only add files from the project specified in the Project Directory field.");
                        WrapCodeButton.Enabled = false;
                        finalCheck = false;
                    }
                }
                performSourceTextChanged = true;
                if (finalCheck) SourceCodeTextBox_TextChanged(SourceCodeTextBox, (EventArgs)null);
            }
        }
        private void SourceCodeTextBox_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop, false))
            {
                e.Effect = DragDropEffects.All;
            }
        }
        private void SourceCodeTextBox_TextChanged(object sender, EventArgs e)
        {
            if (performSourceTextChanged)
            {
                WrapCodeButton.Enabled = false;
                TextBox textBox = (TextBox)sender;
                if (!String.IsNullOrEmpty(textBox.Text))
                {
                    string[] files = textBox.Text.Split(new char[] { '\"' });
                    foreach (string SourceFileName in files)
                    {
                        if (!String.IsNullOrEmpty(SourceFileName.Trim()))
                        {
                            string sourceFileName = Path.Combine(ProjectDirectoryTextBox.Text, SourceFileName);
                            if (File.Exists(sourceFileName))
                            {
                                WrapCodeButton.Enabled = true;
                            }
                            else
                                WrapCodeButton.Enabled = false;
                        }
                    }
                }
                else
                    WrapCodeButton.Enabled = false;
            }
        }
        private void ExitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void Form1_Resize(object sender, EventArgs e)
        {
            Size size = this.Size;
            LogTextBox.Size = new Size(size.Width - 327, size.Height - 254);
        }
        private void WrapCodeButton_Click(object sender, EventArgs e)
        {
            string[] files = SourceCodeTextBox.Text.Split(new char[] { '\"' });
            List<string> fileList = new List<string>();
            LogTextBox.Clear();
            foreach (string SourceFileName in files)
            {
                if (!String.IsNullOrEmpty(SourceFileName.Trim()))
                {
                    fileList.Add(SourceFileName);
                }
            }
            WorkHorse workHorse = new WorkHorse(WorkHorse.Task.Wrap, fileList, ProjectDirectoryTextBox.Text, LogTextBox);
            DialogResult dialogResult = workHorse.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {

            }
        }
    }
}