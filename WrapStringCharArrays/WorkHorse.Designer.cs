﻿namespace WrapStringCharArrays
{
	partial class WorkHorse
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WorkHorse));
            this.WorkProgressBar = new System.Windows.Forms.ProgressBar();
            this.OperationCancelButton = new System.Windows.Forms.Button();
            this.FileProgressBar = new System.Windows.Forms.ProgressBar();
            this.FileProgressBarLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // WorkProgressBar
            // 
            this.WorkProgressBar.Location = new System.Drawing.Point(16, 16);
            this.WorkProgressBar.Margin = new System.Windows.Forms.Padding(4);
            this.WorkProgressBar.Name = "WorkProgressBar";
            this.WorkProgressBar.Size = new System.Drawing.Size(969, 22);
            this.WorkProgressBar.TabIndex = 0;
            // 
            // OperationCancelButton
            // 
            this.OperationCancelButton.Location = new System.Drawing.Point(464, 170);
            this.OperationCancelButton.Name = "OperationCancelButton";
            this.OperationCancelButton.Size = new System.Drawing.Size(75, 40);
            this.OperationCancelButton.TabIndex = 1;
            this.OperationCancelButton.Text = "Cancel";
            this.OperationCancelButton.UseVisualStyleBackColor = true;
            this.OperationCancelButton.Click += new System.EventHandler(this.OperationCancelButton_Click);
            this.OperationCancelButton.Enter += new System.EventHandler(this.OperationCancelButton_Enter);
            this.OperationCancelButton.Leave += new System.EventHandler(this.OperationCancelButton_Leave);
            // 
            // FileProgressBar
            // 
            this.FileProgressBar.Location = new System.Drawing.Point(16, 98);
            this.FileProgressBar.Margin = new System.Windows.Forms.Padding(4);
            this.FileProgressBar.Name = "FileProgressBar";
            this.FileProgressBar.Size = new System.Drawing.Size(969, 22);
            this.FileProgressBar.TabIndex = 2;
            // 
            // FileProgressBarLabel
            // 
            this.FileProgressBarLabel.AutoSize = true;
            this.FileProgressBarLabel.Location = new System.Drawing.Point(16, 74);
            this.FileProgressBarLabel.Name = "FileProgressBarLabel";
            this.FileProgressBarLabel.Size = new System.Drawing.Size(53, 20);
            this.FileProgressBarLabel.TabIndex = 3;
            this.FileProgressBarLabel.Text = "label1";
            // 
            // WorkHorse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 222);
            this.Controls.Add(this.FileProgressBarLabel);
            this.Controls.Add(this.FileProgressBar);
            this.Controls.Add(this.OperationCancelButton);
            this.Controls.Add(this.WorkProgressBar);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "WorkHorse";
            this.Text = "WorkHorse";
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ProgressBar WorkProgressBar;
		private System.Windows.Forms.Button OperationCancelButton;
		private System.Windows.Forms.ProgressBar FileProgressBar;
		private System.Windows.Forms.Label FileProgressBarLabel;
	}
}
