﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WrapStringCharArrays
{
	public class ThreadUserState
	{
		public State Purpose { get; set; }
		public String TitleText { get; set; }
		public Int32 Minimum { get; set; }
		public Int32 Maximum { get; set; }
		public Int32 Current { get; set; }
		public string FileLabel { get; set; }
		public Int32 FileMinimum { get; set; }
		public Int32 FileMaximum { get; set; }
		public Int32 FileCurrent { get; set; }
		public enum State
		{
			Setup,
			Report,
			FileReport,
			UpdateLimits,
			UpdateTitle
		};
		public ThreadUserState()
		{
			Purpose = State.Report;
			TitleText = string.Empty;
			FileLabel = string.Empty;
			Minimum = 0;
			Maximum = 0;
			Current = 0;
			FileMinimum = 0;
			FileMaximum = 0;
			FileCurrent = 0;
		}
		public ThreadUserState(State purposeIn, string titleTextIn, int minimumIn, int maximumIn, int currentIn, int fileMinimumIn, int fileMaximumIn, int fileCurrentIn)
		{
			Purpose = purposeIn;
			TitleText = titleTextIn;
			FileLabel = string.Empty;
			Minimum = minimumIn;
			Maximum = maximumIn;
			Current = currentIn;
			FileMinimum = fileMinimumIn;
			FileMaximum = fileMaximumIn;
			FileCurrent = fileCurrentIn;
		}
	}
}
