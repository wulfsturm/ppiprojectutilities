//************************************************************************************
// WinSearchFile application
// Copyright (C) 2005-2006, Massimo Beatini
//
// This software is provided "as-is", without any express or implied warranty. In 
// no event will the authors be held liable for any damages arising from the use 
// of this software.
//
// Permission is granted to anyone to use this software for any purpose, including 
// commercial applications, and to alter it and redistribute it freely, subject to 
// the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim 
//    that you wrote the original software. If you use this software in a product, 
//    an acknowledgment in the product documentation would be appreciated but is 
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be 
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
//************************************************************************************

using System;
using System.Collections.Specialized;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Threading;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace WinSearchFile
{
	/// <summary>
	/// Summary description for FileSearch.
	/// </summary>
	public class FileSearch
	{
		private String Dir;
		String SearchPattern, SearchForText;
		bool CaseSensitive;
        bool UseRegularExpression;
        bool UseIFilter;

		MainForm mainForm;
		string thrdName;
        private static int maxVisitDepth = 0;

        private static bool filterByDate = false;
        private static FileTimeType fileTimeType;
        private static DateTime startDateRange;
        private static DateTime endDateRange;


        //private static int countVisitDepth = 0;
        
        /// <summary>
        /// initialize the FileSearch class
        /// </summary>
        /// <param name="Dir"></param>
        /// <param name="SearchPattern"></param>
        /// <param name="SearchForText"></param>
        /// <param name="CaseSensitive"></param>
        /// <param name="UseRegularExpression"></param>
        /// <param name="MaxVisitDepth"></param>
        /// <param name="_filterByDate"></param>
        /// <param name="_fileTimeType"></param>
        /// <param name="_startDateRange"></param>
        /// <param name="_endDateRange"></param>
        /// <param name="mainForm"></param>
        /// <param name="thrdName"></param>
        public FileSearch(String Dir, String SearchPattern, String SearchForText, bool CaseSensitive, bool UseRegularExpression, bool UseIFilter, int MaxVisitDepth, bool _filterByDate, FileTimeType _fileTimeType, DateTime _startDateRange, DateTime _endDateRange, MainForm mainForm, string thrdName)
		{
			this.Dir = Dir;
			this.SearchPattern = SearchPattern;
			this.SearchForText = SearchForText;
			this.CaseSensitive = CaseSensitive;
            this.UseRegularExpression = UseRegularExpression;
            this.UseIFilter = UseIFilter;

            filterByDate = _filterByDate;
            fileTimeType = _fileTimeType;
            startDateRange = _startDateRange;
            endDateRange = _endDateRange;
            
            maxVisitDepth = MaxVisitDepth;
			this.mainForm = mainForm;
			this.thrdName = thrdName;
		}

		/// <summary>
		/// Start searching
		/// </summary>
		public void SearchDir()
		{
			// get the UpdateThreadStatus delegate
			MainForm.UpdateThreadStatusDelegate UTSDelegate = new MainForm.UpdateThreadStatusDelegate(this.mainForm.UpdateThreadStatus);
			
			// and Invoke it
			mainForm.Invoke(UTSDelegate, new object[] { this.thrdName, SearchThreadState.running });


			try
			{
				Regex FileExtensionDelim = new Regex(",");
				String []FileExtensions = FileExtensionDelim.Split(SearchPattern);

//				if (FileExtensions.Length == 0)
//				{
//					FileExtensions = new String[1];
//					FileExtensions[0] = "";
//				}

				for (int i = 0; i < FileExtensions.Length; i++)
				{
					FileExtensions[i] = FileExtensions[i].Trim();
				}


                GetFiles(Dir, Dir, FileExtensions, SearchForText, CaseSensitive, UseRegularExpression, UseIFilter, mainForm, 0);

				// update thread state
				mainForm.Invoke(UTSDelegate, new object[] { this.thrdName, SearchThreadState.ready });

			}
			catch (Exception)
			{
				// update thread state
				mainForm.Invoke(UTSDelegate, new object[] { this.thrdName, SearchThreadState.cancelled });
			}
		}

        /// <summary>
        /// Get files
        /// </summary>
        /// <param name="RootDir"></param>
        /// <param name="Dir"></param>
        /// <param name="FileExtensions"></param>
        /// <param name="SearchForText"></param>
        /// <param name="CaseSensitive"></param>
        /// <param name="UseRegularExpression"></param>
        /// <param name="mainForm"></param>
        /// <param name="countVisitDepth"></param>
        private static void GetFiles(string RootDir, string Dir, String[] FileExtensions, String SearchForText, bool CaseSensitive, bool UseRegularExpression, bool UseIFilter, MainForm mainForm, int countVisitDepth)
		{
            
            // get the AddListBoxItem delegate
			MainForm.AddListBoxItemDelegate ALBIDelegate = new MainForm.AddListBoxItemDelegate(mainForm.AddListBoxItem);

			try
			{
				foreach (string FileExtension in FileExtensions)
				{
					foreach (string File in Directory.GetFiles(Dir, FileExtension))
					{
                        // check if filter by date/time
                        if (filterByDate)
                        {
                            DateTime fdt = new DateTime();
                            FileInfo finfo = new FileInfo(File);
                            switch(fileTimeType)
                            {
                                case FileTimeType.CreationTime:
                                    fdt = finfo.CreationTime;
                                    break;
                                case FileTimeType.LastAccessTime:
                                    fdt = finfo.LastAccessTime;
                                    break;
                                case FileTimeType.LastWriteTime:
                                    fdt = finfo.LastWriteTime;
                                    break;
                            }
                            if (!((fdt.CompareTo(startDateRange.AddDays(-1)) > 0) && (fdt.CompareTo(endDateRange.AddDays(1)) < 0)))
                                continue;
                        }


                        if (FileContainsText(File, SearchForText, CaseSensitive, UseRegularExpression, UseIFilter))
						{
                            mainForm.Invoke(ALBIDelegate, new Object[] { File });
						}
					}
				}
			}
			catch (Exception)
			{
			}

            // check the visit depth
            if (maxVisitDepth > 0)
            {
                countVisitDepth++;
                if (countVisitDepth == maxVisitDepth)
                    return;
            }

			// Recursively add all the files in the
			// current directory's subdirectories.
			try
			{
				foreach (string D in Directory.GetDirectories(Dir))
				{
                    GetFiles(RootDir, D, FileExtensions, SearchForText, CaseSensitive, UseRegularExpression, UseIFilter, mainForm, countVisitDepth);
                }
			}
			catch (Exception)
			{
			}
		}


		/// <summary>
		/// get the file content
		/// </summary>
		/// <param name="FileName"></param>
		/// <param name="Error"></param>
		/// <returns></returns>
		public static String GetFileContent(String FileName, out bool Error)
		{
			String TextContent = "";

			FileInfo fInfo = new FileInfo(FileName);

			FileStream   fStream = null;
			StreamReader sReader = null;

			Error = false;

			try
			{
				fStream = fInfo.OpenRead();
				sReader = new StreamReader(fStream);

				TextContent = sReader.ReadToEnd();

			
			}
			catch (System.IO.IOException)
			{
				Error = true;
			}
			finally
			{
				if (fStream != null)
				{
					fStream.Close();
				}

				if (sReader != null)
				{
					sReader.Close();
				}

			}

			return TextContent;
		}

        		

        /// <summary>
        /// search in a PDF file
        /// </summary>
        /// <param name="FileName"></param>
        /// <param name="SearchForText"></param>
        /// <param name="CaseSensitive"></param>
        /// <param name="?"></param>
        /// <param name="UseRegularExpression"></param>
        /// <returns></returns>
        public static bool SearchInPdf(String FileName, String SearchForText, bool CaseSensitive, bool UseRegularExpression)
		{
			bool Result = false;
			PdfReader reader = new PdfReader(FileName);
			

			for (int page = 1; page <= reader.NumberOfPages; page++)
			{
				string pageContent = "";
				// get pdf page content
				byte[] pdf = reader.GetPageContent(page); 

				bool inp = false;
			
				// get only texts
				// discard all the rest
				for (int i = 0; i < pdf.Length; i++)
				{
					char s = Convert.ToChar(pdf[i]);
				
					if (s == '(')
					{
						inp = true;
					}
					else if (inp)
					{
						if (s == ')')
						{
							inp = false;
						}
						else
						{
							if (Char.IsLetterOrDigit(s) || (Char.IsWhiteSpace(s)) || (Char.IsPunctuation(s)) )
								pageContent += s;
						}
					}				
				}

                Result = containsPattern(SearchForText, CaseSensitive, UseRegularExpression, pageContent);
                if (Result)
                    break;

		}
			return Result;
		}

        /// <summary>
        /// if SearchForText length i > 0
        /// search it inside the file content
        /// </summary>
        /// <param name="FileName"></param>
        /// <param name="SearchForText"></param>
        /// <param name="CaseSensitive"></param>
        /// <param name="UseRegularExpression"></param>
        /// <param name="UseIFilter"></param>
        /// <returns></returns>
        public static bool FileContainsText(String FileName, String SearchForText, bool CaseSensitive, bool UseRegularExpression, bool UseIFilter)
		{
			bool Result = (SearchForText.Length == 0);

			if (!Result)
			{

                // try to use IFilter if you have checked
                // UseIFilter checkbox
                if (Parser.IsParseable(FileName) && UseIFilter)
                {
                    string content = Parser.Parse(FileName);
                    // if content length > 0
                    // means that IFilter works and returns the file content
                    // otherwise IFilter hadn't read the file content
                    // i.e. IFilter seems no to be able to extract text contained in dll or exe file
                    if (content.Length > 0)
                    {
                        Result = containsPattern(SearchForText, CaseSensitive, UseRegularExpression, content);
                        return Result;
                    }
                }
                // scan files to get plaintext
                // with my routines
                if (FileName.ToLower().EndsWith(".pdf"))
				{
					// search text in a pdf file
					Result = SearchInPdf(FileName, SearchForText, CaseSensitive, UseRegularExpression);
				}
				else
				{
					bool Error;
					String TextContent = GetFileContent(FileName, out Error);

					if (!Error)
					{
                        Result = containsPattern(SearchForText, CaseSensitive, UseRegularExpression, TextContent);
                    }
				}
			}

			return Result;
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SearchForText"></param>
        /// <param name="CaseSensitive"></param>
        /// <param name="UseRegularExpression"></param>
        /// <param name="TextContent"></param>
        private static bool containsPattern(String SearchForText, bool CaseSensitive, bool UseRegularExpression, String TextContent)
        {
            bool Result = false;

            if (UseRegularExpression)
            {
                RegexOptions options = RegexOptions.Multiline | RegexOptions.CultureInvariant;
                if (!CaseSensitive)
                    options = options | RegexOptions.IgnoreCase;
                Result = System.Text.RegularExpressions.Regex.IsMatch(TextContent, SearchForText, options);
            }
            else
            {
                if (!CaseSensitive)
                {
                    TextContent = TextContent.ToUpper();
                    SearchForText = SearchForText.ToUpper();
                }

                Result = (TextContent.IndexOf(SearchForText) != -1);
            }
            return Result;
        }

	}
}
