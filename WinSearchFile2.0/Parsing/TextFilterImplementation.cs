// this code was developed by vbAccelerator.com
// http://www.vbaccelerator.com/home/Resources/Babbage/NET_IFilter/IFilter_zip_IFilter_TextFilterImplementation_cs.asp
//
using System;
using System.Collections;
using Microsoft.Win32;

namespace WinSearchFile
{

	public class TextFilterImplementations : ReadOnlyCollectionBase
	{
	
		public TextFilterImplementation this[int index]
		{
			get
			{
				return (TextFilterImplementation)this.InnerList[index];
			}
		}

		private TextFilterImplementation addItem(
			string clsid,
			string dllDescription,
			string dllFileName
			)
		{
			TextFilterImplementation impl = new TextFilterImplementation(clsid, dllFileName, dllDescription);
			this.InnerList.Add(impl);
			return impl;
		}

		private void processHandler(
			RegistryKey softwareClasses, 
			string key,
			string handlerGuid
			)
		{
			if (!handlerGuid.Equals("na"))
			{
				// Determine the IFilter persistent handler GUID
				string subKey = "CLSID\\" + handlerGuid + "\\PersistentAddinsRegistered\\{89BCB740-6119-101A-BCB7-00DD010655AF}";
				RegistryKey guidKey = softwareClasses.OpenSubKey(subKey);
				if (guidKey != null)
				{
					string clsid = guidKey.GetValue("", "na").ToString();
					if (!clsid.Equals("na"))
					{
						TextFilterImplementation tf = null;						
						for (int i = 0; i < this.InnerList.Count; i++)
						{
							if ( ((TextFilterImplementation)this.InnerList[i]).CLSID.Equals(clsid))
							{
								tf = (TextFilterImplementation)this.InnerList[i];
							}
						}
						if (tf == null)
						{
							subKey = "CLSID\\" + clsid;
							RegistryKey impl = softwareClasses.OpenSubKey(subKey);
							if (impl != null)
							{
								string dllDescription = impl.GetValue("", "(No Name)").ToString();
								subKey += "\\InProcServer32";
								RegistryKey implDll = softwareClasses.OpenSubKey(subKey);
								if (implDll != null)
								{
									string dllFileName = implDll.GetValue("").ToString();
									tf = addItem(clsid, dllDescription, dllFileName);
								}
							}
						}
						if (tf != null)
						{
							tf.addClass(key);
						}
					}
				}
			}
		}

		private void init()
		{
			this.InnerList.Clear();

			RegistryKey localMachine = Registry.LocalMachine;
			RegistryKey softwareClasses = localMachine.OpenSubKey("SOFTWARE\\Classes");
			foreach (string key in softwareClasses.GetSubKeyNames())
			{
				// check if we have a persistent handler:
				string subKey = key + "\\PersistentHandler";
				RegistryKey classKey = softwareClasses.OpenSubKey(subKey);
				if (classKey != null)
				{
					processHandler(softwareClasses, key, classKey.GetValue("", "na").ToString());
				}
				else
				{
					// do we have a classkey?
					subKey = key + "\\CLSID";
					RegistryKey clsidKey = softwareClasses.OpenSubKey(subKey);
					if (clsidKey != null)
					{
						// check if we have a persistent handler:
						string clsId = clsidKey.GetValue("","na").ToString();
						if (!clsId.Equals("na"))
						{
							subKey = "CLSID\\" + clsId + "\\PersistentHandler";
							classKey = softwareClasses.OpenSubKey(subKey);
							if (classKey != null)
							{
								processHandler(softwareClasses, key, classKey.GetValue("","na").ToString());
							}
						}
					}
				}
			}
		}

		public TextFilterImplementations()
		{
			init();
		}
	}

	public class TextFilterImplementation : ReadOnlyCollectionBase
	{
		private string dllName = "";
		private string dllDescription = "";
		private string clsid = "";

		public string this[int index]
		{
			get
			{
				return (string)this.InnerList[index];
			}
		}

		internal void addClass(string className)
		{
			if (!this.InnerList.Contains(className))
			{
				this.InnerList.Add(className);
			}
		}

		public string DllFileName
		{
			get
			{
				return this.dllName;
			}
		}
		public string Description
		{
			get
			{
				return this.dllDescription;
			}
		}
		public string CLSID
		{
			get
			{
				return this.clsid;
			}
		}

		public TextFilterImplementation( 
			string clsid,
			string dllName, 
			string dllDescription
			)
		{
			this.clsid = clsid;
			this.dllName= dllName;
			this.dllDescription = dllDescription;
		}
	}

}
