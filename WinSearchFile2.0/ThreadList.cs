//************************************************************************************
// WinSearchFile application
// Copyright (C) 2005-2006, Massimo Beatini
//
// This software is provided "as-is", without any express or implied warranty. In 
// no event will the authors be held liable for any damages arising from the use 
// of this software.
//
// Permission is granted to anyone to use this software for any purpose, including 
// commercial applications, and to alter it and redistribute it freely, subject to 
// the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim 
//    that you wrote the original software. If you use this software in a product, 
//    an acknowledgment in the product documentation would be appreciated but is 
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be 
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
//************************************************************************************

using System;
using System.Collections;
using System.Threading;

namespace WinSearchFile
{
	/// <summary>
	/// Thread states
	/// </summary>
	public enum SearchThreadState {ready, running, cancelled};

	/// <summary>
	/// 
	/// </summary>
	public class SearchThread
	{
		public string name;
		public Thread thrd;
		public SearchThreadState state;
		public string searchdir;
	}


	/// <summary>
	/// Summary description for ThreadList.
	/// </summary>
	public class ThreadList
	{
		private ArrayList thrdList;

		
		public ThreadList()
		{
			thrdList = new ArrayList();
		}

		public int AddItem(SearchThread thrd ) 
		{
			return thrdList.Add(thrd);
		}

		public SearchThread Item(int index)
		{
			return (SearchThread) thrdList[index];
		}

		public SearchThread Item(string name)
		{
			SearchThread st = null;
			SearchThread t;

			for (int i = 0; i <thrdList.Count ;i++)
			{
				t =(SearchThread)thrdList[i];
				if (t.name == name)
				{
					st = t;
					break;
				}			
			}

			return st;
		}

		public Boolean RemoveItem(int index)
		{
			Thread thrd;

			Boolean bRemoved = false;
			try
			{
				thrd = ((SearchThread)thrdList[index]).thrd;
				// if thread is still alive
				// (it should be already stopped)
				// force an Abort.
				try
				{
					if (thrd.IsAlive)
					{
						thrd.Abort();
					}
				}
				catch
				{
				}
				thrdList.RemoveAt(index);
				bRemoved = true;
			}
			catch
			{
			}
			return bRemoved;
		}

		public int ItemCount()
		{
			return thrdList.Count;
		}

		public int[] ItemState()
		{
			int[] result  = {0,0,0};

			for (int i = 0; i <thrdList.Count ;i++)
			{
				result[(int)(((SearchThread)thrdList[i]).state)]++;
			}
			return result;
		}

	}
}
