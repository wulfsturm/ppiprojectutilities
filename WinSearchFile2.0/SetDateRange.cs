//************************************************************************************
// WinSearchFile application
// Copyright (C) 2005-2006, Massimo Beatini
//
// This software is provided "as-is", without any express or implied warranty. In 
// no event will the authors be held liable for any damages arising from the use 
// of this software.
//
// Permission is granted to anyone to use this software for any purpose, including 
// commercial applications, and to alter it and redistribute it freely, subject to 
// the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim 
//    that you wrote the original software. If you use this software in a product, 
//    an acknowledgment in the product documentation would be appreciated but is 
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be 
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
//************************************************************************************
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WinSearchFile
{
    public partial class SetDateRange : Form
    {
        DateTime _start = new DateTime();
        DateTime _end = new DateTime();

        /// <summary>
        /// 
        /// </summary>
        public SetDateRange()
        {
            InitializeComponent();
            _start = monthCalendar1.SelectionStart;
            _end = monthCalendar2.SelectionStart;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void monthCalendar1_DateSelected(object sender, DateRangeEventArgs e)
        {
            _start = monthCalendar1.SelectionStart;
            textBox1.Text = monthCalendar1.SelectionStart.ToLongDateString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void monthCalendar2_DateSelected(object sender, DateRangeEventArgs e)
        {
            _end = monthCalendar2.SelectionStart;
            textBox2.Text = monthCalendar2.SelectionStart.ToLongDateString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        public DialogResult OpenForm(ref DateTime start, ref DateTime end)
        {
            //start = null;
            //end = null;
            textBox1.Text = start.ToLongDateString();
            textBox2.Text = end.ToLongDateString();
            monthCalendar1.SetDate(start);
            monthCalendar2.SetDate(end);
            monthCalendar1.Focus();

            if (ShowDialog() == DialogResult.OK)
            {
                start = _start;
                end = _end;
            }
            return this.DialogResult;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (_end.CompareTo(_start) < 0)
            {
                MessageBox.Show("End Date cannot be previous of Start Date", "Warning", MessageBoxButtons.OK);
            }
            else
            {
                DialogResult = DialogResult.OK;
                Close();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}