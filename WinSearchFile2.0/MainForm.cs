//************************************************************************************
// WinSearchFile application
// Copyright (C) 2005-2006, Massimo Beatini
//
// This software is provided "as-is", without any express or implied warranty. In 
// no event will the authors be held liable for any damages arising from the use 
// of this software.
//
// Permission is granted to anyone to use this software for any purpose, including 
// commercial applications, and to alter it and redistribute it freely, subject to 
// the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim 
//    that you wrote the original software. If you use this software in a product, 
//    an acknowledgment in the product documentation would be appreciated but is 
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be 
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
//************************************************************************************

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Threading;
using System.Text.RegularExpressions;
using PU;

namespace WinSearchFile
{
    public enum FileTimeType {CreationTime, LastAccessTime, LastWriteTime};

	/// <summary>
	/// Summary description for MainForm.
	/// </summary>
	public class MainForm : System.Windows.Forms.Form
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtSearchPattern;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox txtSearchText;
		private System.Windows.Forms.Label label4;
		private System.ComponentModel.IContainer components;
		private System.Windows.Forms.StatusBar statusBar;
		private System.Windows.Forms.CheckedListBox disksListBox;
		private System.Windows.Forms.Button btnSearch;
		private System.Windows.Forms.Button btnStop;
		private System.Windows.Forms.ListBox listFileFounded;
		private System.Windows.Forms.Label ResultLabel;
		private System.Windows.Forms.CheckBox checkSortedList;
		private System.Windows.Forms.Label lblThreadActivity;


		/// <summary>
		/// private variables
		/// </summary>
		private String SearchForText = "";
		private ArrayList Dirs = new ArrayList();
		private bool InProgress = false;
		private String SearchPattern = "";
		private bool CaseSensitive = false;
        private bool UseRegularExpression = false;
        private bool UseIFilter = false;

        private System.Windows.Forms.ColumnHeader filepath;
		private System.Windows.Forms.CheckBox checkCaseSensitive;
		private string ContainingFolder = "";
		private System.Windows.Forms.ToolTip toolTip1;
        private Button btnSaveRes;
        private SaveFileDialog saveFileDialog1;
        private NumericUpDown nudMaxVisitDepth;
        private Label label5;
        private ThreadList thrdList = new ThreadList();
        private Panel panel2;
        private CheckBox chkDateTime;
        private RadioButton radioButton1;
        private RadioButton radioButton3;
        private RadioButton radioButton2;
        private Button btnGetDateRange;
        private TextBox txtEndDate;
        private TextBox txtStartDate;
        private Label label7;
        private Label label6;

        private bool filterByDate = false;
        private FileTimeType fileTimeType;
        private DateTime startDateRange;
        private DateTime endDateRange;

        private int maxVisitDepth = 0;
        private CheckBox chkRegExpression;
        private Button btnBrowseFolder;
        private ListBox dirListBox;
        private FolderBrowserDialog folderBrowserDialog1;
        private ContextMenuStrip contextMenuStrip1;
        private ToolStripMenuItem removeItemToolStripMenuItem;
        private Label label8;
        private Button button1;
        private CheckBox chkIFilter;
        private Label lblElapsedTime;

        private string lastSearchParams = "";

        private static DateTime elapsedTime = new DateTime();


		public MainForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkIFilter = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.btnBrowseFolder = new System.Windows.Forms.Button();
            this.chkRegExpression = new System.Windows.Forms.CheckBox();
            this.chkDateTime = new System.Windows.Forms.CheckBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnGetDateRange = new System.Windows.Forms.Button();
            this.txtEndDate = new System.Windows.Forms.TextBox();
            this.txtStartDate = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.nudMaxVisitDepth = new System.Windows.Forms.NumericUpDown();
            this.checkCaseSensitive = new System.Windows.Forms.CheckBox();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.disksListBox = new System.Windows.Forms.CheckedListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSearchText = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSearchPattern = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dirListBox = new System.Windows.Forms.ListBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.removeItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusBar = new System.Windows.Forms.StatusBar();
            this.listFileFounded = new System.Windows.Forms.ListBox();
            this.ResultLabel = new System.Windows.Forms.Label();
            this.filepath = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.checkSortedList = new System.Windows.Forms.CheckBox();
            this.lblThreadActivity = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnSaveRes = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.lblElapsedTime = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxVisitDepth)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.chkIFilter);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.btnBrowseFolder);
            this.panel1.Controls.Add(this.chkRegExpression);
            this.panel1.Controls.Add(this.chkDateTime);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.nudMaxVisitDepth);
            this.panel1.Controls.Add(this.checkCaseSensitive);
            this.panel1.Controls.Add(this.btnStop);
            this.panel1.Controls.Add(this.btnSearch);
            this.panel1.Controls.Add(this.disksListBox);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtSearchText);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtSearchPattern);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.dirListBox);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(298, 589);
            this.panel1.TabIndex = 0;
            // 
            // chkIFilter
            // 
            this.chkIFilter.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkIFilter.Location = new System.Drawing.Point(6, 623);
            this.chkIFilter.Name = "chkIFilter";
            this.chkIFilter.Size = new System.Drawing.Size(280, 20);
            this.chkIFilter.TabIndex = 14;
            this.chkIFilter.Text = "Use installed IFilter";
            this.chkIFilter.CheckedChanged += new System.EventHandler(this.chkIFilter_CheckedChanged);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.Control;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Image = global::WinSearchFile.Properties.Resources.Help;
            this.button1.Location = new System.Drawing.Point(234, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(44, 46);
            this.button1.TabIndex = 14;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(7, 483);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(156, 21);
            this.label8.TabIndex = 13;
            this.label8.Text = "or select directories";
            // 
            // btnBrowseFolder
            // 
            this.btnBrowseFolder.BackColor = System.Drawing.SystemColors.Control;
            this.btnBrowseFolder.Location = new System.Drawing.Point(180, 481);
            this.btnBrowseFolder.Name = "btnBrowseFolder";
            this.btnBrowseFolder.Size = new System.Drawing.Size(88, 23);
            this.btnBrowseFolder.TabIndex = 12;
            this.btnBrowseFolder.Text = "Browse";
            this.btnBrowseFolder.UseVisualStyleBackColor = false;
            this.btnBrowseFolder.Click += new System.EventHandler(this.btnBrowseFolder_Click);
            // 
            // chkRegExpression
            // 
            this.chkRegExpression.AutoSize = true;
            this.chkRegExpression.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRegExpression.Location = new System.Drawing.Point(143, 144);
            this.chkRegExpression.Name = "chkRegExpression";
            this.chkRegExpression.Size = new System.Drawing.Size(145, 21);
            this.chkRegExpression.TabIndex = 3;
            this.chkRegExpression.Text = "Reg. Expression";
            this.chkRegExpression.CheckedChanged += new System.EventHandler(this.chkRegExpression_CheckedChanged);
            // 
            // chkDateTime
            // 
            this.chkDateTime.AutoSize = true;
            this.chkDateTime.Location = new System.Drawing.Point(6, 201);
            this.chkDateTime.Name = "chkDateTime";
            this.chkDateTime.Size = new System.Drawing.Size(193, 21);
            this.chkDateTime.TabIndex = 5;
            this.chkDateTime.Text = "Date/Time search params";
            this.chkDateTime.UseVisualStyleBackColor = true;
            this.chkDateTime.CheckedChanged += new System.EventHandler(this.chkDateTime_CheckedChanged);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.btnGetDateRange);
            this.panel2.Controls.Add(this.txtEndDate);
            this.panel2.Controls.Add(this.txtStartDate);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.radioButton3);
            this.panel2.Controls.Add(this.radioButton2);
            this.panel2.Controls.Add(this.radioButton1);
            this.panel2.Enabled = false;
            this.panel2.Location = new System.Drawing.Point(6, 225);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(272, 128);
            this.panel2.TabIndex = 8;
            // 
            // btnGetDateRange
            // 
            this.btnGetDateRange.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnGetDateRange.Location = new System.Drawing.Point(227, 69);
            this.btnGetDateRange.Name = "btnGetDateRange";
            this.btnGetDateRange.Size = new System.Drawing.Size(32, 23);
            this.btnGetDateRange.TabIndex = 7;
            this.btnGetDateRange.Text = "...";
            this.btnGetDateRange.UseVisualStyleBackColor = false;
            this.btnGetDateRange.Click += new System.EventHandler(this.btnGetDateRange_Click);
            // 
            // txtEndDate
            // 
            this.txtEndDate.Location = new System.Drawing.Point(46, 92);
            this.txtEndDate.Name = "txtEndDate";
            this.txtEndDate.ReadOnly = true;
            this.txtEndDate.Size = new System.Drawing.Size(178, 22);
            this.txtEndDate.TabIndex = 10;
            // 
            // txtStartDate
            // 
            this.txtStartDate.Location = new System.Drawing.Point(46, 68);
            this.txtStartDate.Name = "txtStartDate";
            this.txtStartDate.ReadOnly = true;
            this.txtStartDate.Size = new System.Drawing.Size(178, 22);
            this.txtStartDate.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 97);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 17);
            this.label7.TabIndex = 4;
            this.label7.Text = "End:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 17);
            this.label6.TabIndex = 3;
            this.label6.Text = "Start:";
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(10, 48);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(119, 21);
            this.radioButton3.TabIndex = 8;
            this.radioButton3.Text = "Last write time";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(10, 28);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(134, 21);
            this.radioButton2.TabIndex = 7;
            this.radioButton2.Text = "Last access time";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(10, 7);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(112, 21);
            this.radioButton1.TabIndex = 6;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Creation time";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 172);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(188, 21);
            this.label5.TabIndex = 7;
            this.label5.Text = "Max directory visit depth:";
            // 
            // nudMaxVisitDepth
            // 
            this.nudMaxVisitDepth.Location = new System.Drawing.Point(202, 170);
            this.nudMaxVisitDepth.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nudMaxVisitDepth.Name = "nudMaxVisitDepth";
            this.nudMaxVisitDepth.Size = new System.Drawing.Size(54, 22);
            this.nudMaxVisitDepth.TabIndex = 4;
            this.nudMaxVisitDepth.ValueChanged += new System.EventHandler(this.nudMaxVisitDepth_ValueChanged);
            // 
            // checkCaseSensitive
            // 
            this.checkCaseSensitive.AutoSize = true;
            this.checkCaseSensitive.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkCaseSensitive.Location = new System.Drawing.Point(6, 144);
            this.checkCaseSensitive.Name = "checkCaseSensitive";
            this.checkCaseSensitive.Size = new System.Drawing.Size(130, 21);
            this.checkCaseSensitive.TabIndex = 2;
            this.checkCaseSensitive.Text = "Case sensitive";
            this.checkCaseSensitive.CheckedChanged += new System.EventHandler(this.checkCaseSensitive_CheckedChanged);
            // 
            // btnStop
            // 
            this.btnStop.BackColor = System.Drawing.SystemColors.Control;
            this.btnStop.Enabled = false;
            this.btnStop.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnStop.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStop.Location = new System.Drawing.Point(169, 648);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(106, 27);
            this.btnStop.TabIndex = 5;
            this.btnStop.Text = "Sto&p Search";
            this.btnStop.UseVisualStyleBackColor = false;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.SystemColors.Control;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnSearch.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Location = new System.Drawing.Point(16, 648);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(105, 27);
            this.btnSearch.TabIndex = 15;
            this.btnSearch.Text = "Sea&rch";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // disksListBox
            // 
            this.disksListBox.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.disksListBox.Location = new System.Drawing.Point(6, 380);
            this.disksListBox.Name = "disksListBox";
            this.disksListBox.Size = new System.Drawing.Size(278, 80);
            this.disksListBox.TabIndex = 12;
            this.disksListBox.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.disksListBox_ItemCheck);
            this.disksListBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.disksListBox_KeyDown);
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 361);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(278, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Look in:";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(278, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Word/phrase in the file or Reg. Expr.:";
            // 
            // txtSearchText
            // 
            this.txtSearchText.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearchText.Location = new System.Drawing.Point(6, 117);
            this.txtSearchText.Name = "txtSearchText";
            this.txtSearchText.Size = new System.Drawing.Size(278, 24);
            this.txtSearchText.TabIndex = 1;
            this.txtSearchText.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(278, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "All or part of the file name:";
            // 
            // txtSearchPattern
            // 
            this.txtSearchPattern.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearchPattern.Location = new System.Drawing.Point(6, 65);
            this.txtSearchPattern.Name = "txtSearchPattern";
            this.txtSearchPattern.Size = new System.Drawing.Size(278, 24);
            this.txtSearchPattern.TabIndex = 0;
            this.txtSearchPattern.Text = "*.*";
            this.txtSearchPattern.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(223, 37);
            this.label1.TabIndex = 0;
            this.label1.Text = "Search by any or all of the criteria below";
            // 
            // dirListBox
            // 
            this.dirListBox.FormattingEnabled = true;
            this.dirListBox.HorizontalScrollbar = true;
            this.dirListBox.ItemHeight = 16;
            this.dirListBox.Location = new System.Drawing.Point(7, 509);
            this.dirListBox.Name = "dirListBox";
            this.dirListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.dirListBox.Size = new System.Drawing.Size(279, 84);
            this.dirListBox.Sorted = true;
            this.dirListBox.TabIndex = 13;
            this.dirListBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dirListBox_MouseDown);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.removeItemToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(232, 28);
            // 
            // removeItemToolStripMenuItem
            // 
            this.removeItemToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.removeItemToolStripMenuItem.Name = "removeItemToolStripMenuItem";
            this.removeItemToolStripMenuItem.Size = new System.Drawing.Size(231, 24);
            this.removeItemToolStripMenuItem.Text = "Remove Selected  Item";
            this.removeItemToolStripMenuItem.Click += new System.EventHandler(this.removeItemToolStripMenuItem_Click);
            // 
            // statusBar
            // 
            this.statusBar.Location = new System.Drawing.Point(0, 595);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(763, 28);
            this.statusBar.TabIndex = 1;
            // 
            // listFileFounded
            // 
            this.listFileFounded.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listFileFounded.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listFileFounded.HorizontalScrollbar = true;
            this.listFileFounded.ItemHeight = 17;
            this.listFileFounded.Location = new System.Drawing.Point(307, 30);
            this.listFileFounded.Name = "listFileFounded";
            this.listFileFounded.Size = new System.Drawing.Size(456, 497);
            this.listFileFounded.TabIndex = 7;
            this.listFileFounded.SelectedIndexChanged += new System.EventHandler(this.listFileFounded_SelectedIndexChanged);
            this.listFileFounded.DoubleClick += new System.EventHandler(this.listFileFounded_DoubleClick);
            // 
            // ResultLabel
            // 
            this.ResultLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ResultLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ResultLabel.Location = new System.Drawing.Point(312, 7);
            this.ResultLabel.Name = "ResultLabel";
            this.ResultLabel.Size = new System.Drawing.Size(59, 18);
            this.ResultLabel.TabIndex = 3;
            // 
            // filepath
            // 
            this.filepath.Text = "File";
            this.filepath.Width = 760;
            // 
            // checkSortedList
            // 
            this.checkSortedList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkSortedList.AutoSize = true;
            this.checkSortedList.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkSortedList.Location = new System.Drawing.Point(629, 7);
            this.checkSortedList.Name = "checkSortedList";
            this.checkSortedList.Size = new System.Drawing.Size(129, 21);
            this.checkSortedList.TabIndex = 6;
            this.checkSortedList.Text = "sorted results";
            this.checkSortedList.CheckedChanged += new System.EventHandler(this.checkSortedList_CheckedChanged);
            // 
            // lblThreadActivity
            // 
            this.lblThreadActivity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblThreadActivity.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThreadActivity.Location = new System.Drawing.Point(307, 565);
            this.lblThreadActivity.Name = "lblThreadActivity";
            this.lblThreadActivity.Size = new System.Drawing.Size(338, 18);
            this.lblThreadActivity.TabIndex = 6;
            // 
            // btnSaveRes
            // 
            this.btnSaveRes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveRes.BackColor = System.Drawing.SystemColors.Control;
            this.btnSaveRes.Enabled = false;
            this.btnSaveRes.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnSaveRes.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveRes.Location = new System.Drawing.Point(652, 565);
            this.btnSaveRes.Name = "btnSaveRes";
            this.btnSaveRes.Size = new System.Drawing.Size(106, 27);
            this.btnSaveRes.TabIndex = 8;
            this.btnSaveRes.Text = "Save &Results";
            this.btnSaveRes.UseVisualStyleBackColor = false;
            this.btnSaveRes.Click += new System.EventHandler(this.btnSaveRes_Click);
            // 
            // lblElapsedTime
            // 
            this.lblElapsedTime.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblElapsedTime.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblElapsedTime.Location = new System.Drawing.Point(560, 7);
            this.lblElapsedTime.Name = "lblElapsedTime";
            this.lblElapsedTime.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblElapsedTime.Size = new System.Drawing.Size(60, 18);
            this.lblElapsedTime.TabIndex = 9;
            this.lblElapsedTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(6, 15);
            this.ClientSize = new System.Drawing.Size(763, 623);
            this.Controls.Add(this.lblElapsedTime);
            this.Controls.Add(this.btnSaveRes);
            this.Controls.Add(this.lblThreadActivity);
            this.Controls.Add(this.checkSortedList);
            this.Controls.Add(this.ResultLabel);
            this.Controls.Add(this.listFileFounded);
            this.Controls.Add(this.statusBar);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "WinSearchFile 2.0";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxVisitDepth)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
           // If this program is already running, set focus
            // to that instance and quit.
            if (ProcessUtils.ThisProcessIsAlreadyRunning())
            {
                // "WinSearchFile 2.0" is the caption (Text property) of the main form.
                ProcessUtils.SetFocusToPreviousInstance("WinSearchFile 2.0");
            }
            else
            {
                Application.EnableVisualStyles();
                MainForm mainf = new MainForm();
                Application.Idle += new System.EventHandler(mainf.OnIdle);

                Application.Run(mainf);
            }
		}


		/// <summary>
		/// Check if there are threads running
		/// </summary>
		private bool isInProgress()
		{
			bool result = false;
			SearchThread st;

			for (int i= 0; i < thrdList.ItemCount(); i++)
			{
				st = thrdList.Item(i);
				if (st.state == SearchThreadState.running)
				{
					result = true;
					break;
				}
			}
			return result;
		}

		/// <summary>
		/// Update form's UI when program is idle. 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public void OnIdle(object sender, EventArgs e)
		{
			InProgress = isInProgress();

			if (InProgress)
			{
				// some thread are searching...
				UpdateStatusBar("Searching...");
                
                // update the search elapsed time
                long delta = DateTime.Now.Ticks - elapsedTime.Ticks;
                DateTime dt = new DateTime(delta);
                lblElapsedTime.Text = dt.ToLongTimeString();
			}
			else
			{
				// check if all threads are in the 
				// cancelled state
				bool allInTheSameState = true;
				for (int i= 0; i < thrdList.ItemCount(); i++)
				{
					if ( ((SearchThread)thrdList.Item(i)).state != SearchThreadState.cancelled)
					{
						allInTheSameState = false;
						break;
					}
				}
				if ((allInTheSameState) && (thrdList.ItemCount()>0))
				{
					// search cancelled
					UpdateStatusBar("Search cancelled");
				}
				else
				{
					// check if all threads are in the 
					// ready state
					allInTheSameState = true;
					for (int i= 0; i < thrdList.ItemCount(); i++)
					{
						if ( ((SearchThread)thrdList.Item(i)).state != SearchThreadState.ready)
						{
							allInTheSameState = false;
							break;
						}
					}
//                    if (allInTheSameState)
                    if ((allInTheSameState) && (thrdList.ItemCount() > 0))
                    {
						// search complete
						UpdateStatusBar("Search completed");
					}
				}

			}

			// enable/disable controls
			btnSearch.Enabled = !InProgress;
			btnStop.Enabled = InProgress;
			disksListBox.Enabled = !InProgress;
			txtSearchPattern.Enabled = !InProgress;
			txtSearchText.Enabled = !InProgress;

            // enable/disble Save results button
            btnSaveRes.Enabled = (!InProgress) && (listFileFounded.Items.Count > 0);

			// update the results label
			String Text = "Res&ults:  (" + listFileFounded.Items.Count.ToString() + ")";

			if (String.Compare(ResultLabel.Text, Text) != 0)
			{
				ResultLabel.Text = Text;
			}

			// update the thread activity label
			int[] itemstate = thrdList.ItemState();
			Text = "Searching threads: ready (" + itemstate[0].ToString() + ") - running (" + itemstate[1].ToString() + ") - cancelled (" + itemstate[2].ToString() + ")"; 

			if (String.Compare(lblThreadActivity.Text, Text) != 0)
			{
				lblThreadActivity.Text = Text;
			}
		}


		/// <summary>
		/// Delegate for AddListBoxItem
		/// </summary>
		public delegate void AddListBoxItemDelegate(String Text);

		/// <summary>
		/// Add a new item to the file founded list
		/// </summary>
		/// <param name="Text"></param>
		public void AddListBoxItem(String Text)
		{
			// I use Monitor to synchronize access 
			// to the file founded list
			Monitor.Enter(listFileFounded);

			listFileFounded.Items.Add(Text);

			Monitor.Exit(listFileFounded);
		}


		/// <summary>
		/// Add items for all floppy, 
		/// CD and hard drives.
		/// </summary>
		private void LoadDisksComboBox()
		{
			disksListBox.Items.Clear();
		
			// Try to use DiskCollection
			// retrieving drive information
			DiskCollection diskColl = new DiskCollection();
			if ( diskColl.Load() )
			{
				foreach(DiskCollection.LogicalDriveInfo diskinfo in diskColl)
				{
					disksListBox.Items.Add(diskinfo.Name.ToString() + ": "+ diskinfo.Description );

				}
			}
			else
			{
				// otherwise build a drive list checking 
				// if a root directory exists
				for (char Ch = 'A'; Ch <= 'Z'; Ch++)
				{
					string Dir = Ch + @":\";
	
					if (Directory.Exists(Dir))
					{
						disksListBox.Items.Add( Ch+ @":" );
					}
				}
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void MainForm_Load(object sender, System.EventArgs e)
		{
			Cursor.Current = Cursors.WaitCursor;

			LoadDisksComboBox();

            // set date range
            startDateRange = new DateTime();
            endDateRange = new DateTime();
            startDateRange = endDateRange = DateTime.Now;
            txtStartDate.Text = startDateRange.ToLongDateString();
            txtEndDate.Text = endDateRange.ToLongDateString(); 

			MinimumSize = Size;

			// Set up the ToolTip text for the Button and Checkbox.
			toolTip1.SetToolTip(this.listFileFounded, "Double click to open the corresponding folder");
			toolTip1.SetToolTip(this.disksListBox, "Press F3 to refresh the list");
            toolTip1.SetToolTip(this.nudMaxVisitDepth, "0 value means no depth limit");
            toolTip1.SetToolTip(this.btnGetDateRange, "Click to set date range");
            toolTip1.SetToolTip(this.txtSearchText, "Write here a word/phrase to search in or a regular expression");
            toolTip1.SetToolTip(this.dirListBox, "Use right mouse button to delete selected items");
            toolTip1.SetToolTip(this.chkIFilter, "View help to see installed IFilter on your PC");

            Cursor.Current = Cursors.Arrow;
		
		}


		public void UpdateStatusBar(String Text)
		{
			statusBar.Text = Text;
            //ElapsedTime.Text = ;
		}



		/// <summary>
		/// Delegate for UpdateThreadStatus function
		/// </summary>
		public delegate void UpdateThreadStatusDelegate(String thrdName, SearchThreadState sts);

		/// <summary>
		/// Store the new state of a thread
		/// </summary>
		/// <param name="thrdName"></param>
		/// <param name="sts"></param>
		public void UpdateThreadStatus(String thrdName, SearchThreadState sts)
		{
			SearchThread st = thrdList.Item(thrdName);
			st.state = sts;
		}


		/// <summary>
		/// Start search
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnSearch_Click(object sender, System.EventArgs e)
		{
            lastSearchParams = "";

			// empty thread list
			for (int i = thrdList.ItemCount()-1; i>=0; i--)
			{
				thrdList.RemoveItem(i);
			}

			// clear the file founded list
			listFileFounded.Items.Clear();
			ContainingFolder = "";
	
			// get the search pattern
			// or use a default
			SearchPattern = txtSearchPattern.Text.Trim();
			if (SearchPattern.Length == 0)
			{
				SearchPattern = "*.*";
			}

            lastSearchParams += "Search Pattern: " + SearchPattern + Environment.NewLine;

			// get the text to search for
			SearchForText = txtSearchText.Text.Trim();

            lastSearchParams += "Search For Text: " + SearchForText.ToString() + Environment.NewLine;
            lastSearchParams += "Case sensitive: " + CaseSensitive.ToString() + Environment.NewLine;
            lastSearchParams += "Regular Expression: " + UseRegularExpression.ToString() + Environment.NewLine;
            lastSearchParams += "Use IFilter: " + UseIFilter.ToString() + Environment.NewLine;

            lastSearchParams += "Max directory visit depth: " + nudMaxVisitDepth.Value.ToString() + Environment.NewLine;

            lastSearchParams += "Filter by Date/Time: " + filterByDate.ToString() + Environment.NewLine;
            if (filterByDate)
            {
                lastSearchParams += "Filter by: " + fileTimeType.ToString() + Environment.NewLine;
                lastSearchParams += "Start: " + txtStartDate.Text + " - End:" + txtEndDate.Text + Environment.NewLine;
            }
            lastSearchParams += "Search in:" + Environment.NewLine;
			// clear the Dirs arraylist
			Dirs.Clear();

            // Look in disks only if
            // the dirListBox is empty
            if (isDirListBoxEmpty())
            {
                // check if each selected drive exists
                foreach (int Index in disksListBox.CheckedIndices)
                {
                    // chek if drive is ready
                    String Dir = disksListBox.Items[Index].ToString().Substring(0, 2);
                    Dir += @"\";
                    if (CheckExists(Dir))
                    {
                        Dirs.Add(Dir);
                        lastSearchParams += Dir + Environment.NewLine;
                    }
                }
            }
            else
            {
                for (int Index = 0; Index < dirListBox.Items.Count; Index++)
                {
                    String Dir = dirListBox.Items[Index].ToString();
                    Dirs.Add(Dir);
                    lastSearchParams += Dir + Environment.NewLine;
                }
            }

            if (Dirs.Count > 0)
            {
                elapsedTime = DateTime.Now;
            }
			// I use 1 thread for each dir to scan
			foreach (String Dir in Dirs)
			{
				Thread oT;
				string thrdName = "Thread" + ((int)(thrdList.ItemCount()+1)).ToString();
				FileSearch fs = new FileSearch(Dir, SearchPattern, SearchForText, CaseSensitive, UseRegularExpression, UseIFilter, maxVisitDepth, filterByDate, fileTimeType, startDateRange, endDateRange, this, thrdName);
				oT = new Thread(new ThreadStart(fs.SearchDir));

				oT.Name = thrdName;

				SearchThread st = new SearchThread();
				st.searchdir = Dir;
				st.name = oT.Name;
				st.thrd = oT;
				st.state = SearchThreadState.ready;
				thrdList.AddItem(st);
				oT.Start();
			}
		}

		#region listFileFounded events

		/// <summary>
		/// get the containg folder of the selected file
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void listFileFounded_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// I use Monitor to synchronize access 
			// to the result list
			Monitor.Enter(listFileFounded);

			string val;
			try
			{
				val = listFileFounded.SelectedItem.ToString();
				int index = val.LastIndexOf("\\") +1;
				ContainingFolder =  val.Substring(0, index);
			}
			catch
			{
				ContainingFolder = "";
			}

			Monitor.Exit(listFileFounded);

		}

		/// <summary>
		/// open the containing folder and select the file
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void listFileFounded_DoubleClick(object sender, System.EventArgs e)
		{
			try
			{
				//System.Diagnostics.Process.Start(ContainingFolder);
                System.Diagnostics.Process.Start("explorer.exe", @"/e /select, " + listFileFounded.SelectedItem.ToString());
			}
			catch(Exception)
			{
			}
		}


		#endregion

		/// <summary>
		/// Stop searching
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnStop_Click(object sender, System.EventArgs e)
		{
			// some threads are running
			if (InProgress)
			{
				// Abort each searching thread in running status
				// and change its status to cancelled
				for (int i= 0; i < thrdList.ItemCount(); i++)
				{
					if (((SearchThread)thrdList.Item(i)).state == SearchThreadState.running)
					{
						((SearchThread)thrdList.Item(i)).state = SearchThreadState.cancelled;
						Thread tt;
						try
						{
							tt = ((SearchThread)thrdList.Item(i)).thrd;
							tt.Abort();
						}
						catch
						{
						}
					}
				}

			}
		}

		/// <summary>
		/// check if directory "dir" exists
		/// </summary>
		/// <param name="dir"></param>
		/// <returns></returns>
		private bool CheckExists(string dir)
		{
			bool result = false;

			if (Directory.Exists(dir))
			{
				result = true;
			}
			return result;
		}

		/// <summary>
		/// select which drive/disk to scan
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void disksListBox_ItemCheck(object sender, System.Windows.Forms.ItemCheckEventArgs e)
		{
			string val = disksListBox.SelectedItem.ToString();

			if ( e.CurrentValue == CheckState.Unchecked )
			{
				// if item checked 
				// check if drive is ready
				if (! CheckExists(val.Substring(0,2)+@"\") )
				{
					MessageBox.Show(val + " not ready!", "Warning");
				}
			}
		}

		/// <summary>
		/// Keypress event handler for the 2
		/// input box
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void txt_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if (e.KeyChar == 13)
				btnSearch_Click(btnSearch, new System.EventArgs());
		}


		/// <summary>
		/// check if searching for "SearchForText" value must considering case sensitive 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void checkCaseSensitive_CheckedChanged(object sender, System.EventArgs e)
		{
			CaseSensitive = checkCaseSensitive.Checked;
		}

		/// <summary>
		/// if checked th file founded list has the
		/// Sorted property equal to true 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void checkSortedList_CheckedChanged(object sender, System.EventArgs e)
		{
			listFileFounded.Sorted = checkSortedList.Checked;
		}

		/// <summary>
		/// refresh the drive list
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void disksListBox_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyValue == (char)Shortcut.F3)
					LoadDisksComboBox();
		
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSaveRes_Click(object sender, EventArgs e)
        {
            Stream myStream;
            string resultFileName = "";

            saveFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.RestoreDirectory = true; 
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                resultFileName = saveFileDialog1.FileName.ToString();
                // saveResults
                if ((myStream = saveFileDialog1.OpenFile()) != null)
                {
                    try
                    {
                        StreamWriter wText = new StreamWriter(myStream);
                        wText.AutoFlush = true;

                        string aLine = null;
                        StringReader strReader = new StringReader(lastSearchParams);
                        while (true)
                        {
                            aLine = strReader.ReadLine();
                            if (aLine == null)
                                break;
                            else
                                wText.WriteLine(aLine);
                        }
                        strReader.Close();
                        strReader = null;

                        wText.WriteLine("=======================");
                        for (int i = 0; i < listFileFounded.Items.Count; i++)
                            wText.WriteLine(listFileFounded.Items[i].ToString());

                        myStream.Close();

                        MessageBox.Show("Results successfully saved!"); 
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Warning");
                    }
                }

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void nudMaxVisitDepth_ValueChanged(object sender, EventArgs e)
        {
            maxVisitDepth = (int)nudMaxVisitDepth.Value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkDateTime_CheckedChanged(object sender, EventArgs e)
        {
            filterByDate = panel2.Enabled = !panel2.Enabled;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGetDateRange_Click(object sender, EventArgs e)
        {
            // start
            SetDateRange frm = new SetDateRange();
            if (frm.OpenForm(ref startDateRange, ref endDateRange) == DialogResult.OK)
            {
                txtStartDate.Text = startDateRange.ToLongDateString();
                txtEndDate.Text = endDateRange.ToLongDateString();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            fileTimeType = FileTimeType.CreationTime;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            fileTimeType = FileTimeType.LastAccessTime;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            fileTimeType = FileTimeType.LastWriteTime;
        }

        private void chkRegExpression_CheckedChanged(object sender, EventArgs e)
        {
            UseRegularExpression = chkRegExpression.Checked;            
        }

        private void btnBrowseFolder_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                dirListBox.Items.Add(folderBrowserDialog1.SelectedPath.ToString());
            }
        }

        private void removeItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // remove selected item
            for (int i = dirListBox.SelectedItems.Count - 1; i >= 0; i-- )
            {
                dirListBox.Items.Remove(dirListBox.SelectedItems[i]);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool isDirListBoxEmpty()
        {
            return (dirListBox.Items.Count == 0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dirListBox_MouseDown(object sender, MouseEventArgs e)
        {
            if ((e.Button == MouseButtons.Right) && (!isDirListBoxEmpty()))
            {
                contextMenuStrip1.Show(dirListBox, e.X, e.Y);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AboutBox1 about = new AboutBox1();
            about.ShowDialog();
        }

        private void chkIFilter_CheckedChanged(object sender, EventArgs e)
        {
            UseIFilter = chkIFilter.Checked;
        }


    }
}
