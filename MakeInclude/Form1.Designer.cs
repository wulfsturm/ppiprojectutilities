﻿namespace MakeInclude
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BrowseSourceCodeButton = new System.Windows.Forms.Button();
            this.SourceCodeTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.BrowseProjectDirectoryButton = new System.Windows.Forms.Button();
            this.ProjectDirectoryTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.MakeIncludeButton = new System.Windows.Forms.Button();
            this.BrowseOutputDirectoryButton = new System.Windows.Forms.Button();
            this.OutputDirectoryTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.IncludeTextBox = new System.Windows.Forms.TextBox();
            this.CopyTextButton = new System.Windows.Forms.Button();
            this.WriteFileButton = new System.Windows.Forms.Button();
            this.ExitButton = new System.Windows.Forms.Button();
            this.CopyFileNameButton = new System.Windows.Forms.Button();
            this.AddToVCxItemsCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // BrowseSourceCodeButton
            // 
            this.BrowseSourceCodeButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BrowseSourceCodeButton.BackgroundImage = global::MakeInclude.Properties.Resources.browse;
            this.BrowseSourceCodeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BrowseSourceCodeButton.FlatAppearance.BorderSize = 0;
            this.BrowseSourceCodeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BrowseSourceCodeButton.Location = new System.Drawing.Point(859, 122);
            this.BrowseSourceCodeButton.Name = "BrowseSourceCodeButton";
            this.BrowseSourceCodeButton.Size = new System.Drawing.Size(47, 42);
            this.BrowseSourceCodeButton.TabIndex = 26;
            this.BrowseSourceCodeButton.Tag = "Input";
            this.BrowseSourceCodeButton.UseVisualStyleBackColor = true;
            this.BrowseSourceCodeButton.Click += new System.EventHandler(this.BrowseFileButton_Click);
            // 
            // SourceCodeTextBox
            // 
            this.SourceCodeTextBox.AllowDrop = true;
            this.SourceCodeTextBox.Location = new System.Drawing.Point(158, 130);
            this.SourceCodeTextBox.Name = "SourceCodeTextBox";
            this.SourceCodeTextBox.Size = new System.Drawing.Size(691, 26);
            this.SourceCodeTextBox.TabIndex = 25;
            this.SourceCodeTextBox.TextChanged += new System.EventHandler(this.SourceCodeTextBox_TextChanged);
            this.SourceCodeTextBox.DragDrop += new System.Windows.Forms.DragEventHandler(this.SourceCodeTextBox_DragDrop);
            this.SourceCodeTextBox.DragEnter += new System.Windows.Forms.DragEventHandler(this.SourceCodeTextBox_DragEnter);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(45, 133);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 20);
            this.label3.TabIndex = 24;
            this.label3.Text = "Source Code";
            // 
            // BrowseProjectDirectoryButton
            // 
            this.BrowseProjectDirectoryButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BrowseProjectDirectoryButton.BackgroundImage = global::MakeInclude.Properties.Resources.browse;
            this.BrowseProjectDirectoryButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BrowseProjectDirectoryButton.FlatAppearance.BorderSize = 0;
            this.BrowseProjectDirectoryButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BrowseProjectDirectoryButton.Location = new System.Drawing.Point(859, 4);
            this.BrowseProjectDirectoryButton.Name = "BrowseProjectDirectoryButton";
            this.BrowseProjectDirectoryButton.Size = new System.Drawing.Size(47, 42);
            this.BrowseProjectDirectoryButton.TabIndex = 23;
            this.BrowseProjectDirectoryButton.Tag = "ProjectDirectory";
            this.BrowseProjectDirectoryButton.UseVisualStyleBackColor = true;
            this.BrowseProjectDirectoryButton.Click += new System.EventHandler(this.BrowseFolderButton_Click);
            // 
            // ProjectDirectoryTextBox
            // 
            this.ProjectDirectoryTextBox.Location = new System.Drawing.Point(158, 12);
            this.ProjectDirectoryTextBox.Name = "ProjectDirectoryTextBox";
            this.ProjectDirectoryTextBox.Size = new System.Drawing.Size(691, 26);
            this.ProjectDirectoryTextBox.TabIndex = 22;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 20);
            this.label1.TabIndex = 21;
            this.label1.Text = "Project Directory";
            // 
            // MakeIncludeButton
            // 
            this.MakeIncludeButton.Location = new System.Drawing.Point(158, 176);
            this.MakeIncludeButton.Name = "MakeIncludeButton";
            this.MakeIncludeButton.Size = new System.Drawing.Size(124, 40);
            this.MakeIncludeButton.TabIndex = 27;
            this.MakeIncludeButton.Text = "Make Include";
            this.MakeIncludeButton.UseVisualStyleBackColor = true;
            this.MakeIncludeButton.Click += new System.EventHandler(this.MakeIncludeButton_Click);
            // 
            // BrowseOutputDirectoryButton
            // 
            this.BrowseOutputDirectoryButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BrowseOutputDirectoryButton.BackgroundImage = global::MakeInclude.Properties.Resources.browse;
            this.BrowseOutputDirectoryButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BrowseOutputDirectoryButton.FlatAppearance.BorderSize = 0;
            this.BrowseOutputDirectoryButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BrowseOutputDirectoryButton.Location = new System.Drawing.Point(859, 56);
            this.BrowseOutputDirectoryButton.Name = "BrowseOutputDirectoryButton";
            this.BrowseOutputDirectoryButton.Size = new System.Drawing.Size(47, 42);
            this.BrowseOutputDirectoryButton.TabIndex = 30;
            this.BrowseOutputDirectoryButton.Tag = "OutputDirectory";
            this.BrowseOutputDirectoryButton.UseVisualStyleBackColor = true;
            this.BrowseOutputDirectoryButton.Click += new System.EventHandler(this.BrowseFolderButton_Click);
            // 
            // OutputDirectoryTextBox
            // 
            this.OutputDirectoryTextBox.Location = new System.Drawing.Point(158, 64);
            this.OutputDirectoryTextBox.Name = "OutputDirectoryTextBox";
            this.OutputDirectoryTextBox.Size = new System.Drawing.Size(691, 26);
            this.OutputDirectoryTextBox.TabIndex = 29;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 67);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(133, 20);
            this.label2.TabIndex = 28;
            this.label2.Text = "Output Directory";
            // 
            // IncludeTextBox
            // 
            this.IncludeTextBox.Location = new System.Drawing.Point(158, 234);
            this.IncludeTextBox.Multiline = true;
            this.IncludeTextBox.Name = "IncludeTextBox";
            this.IncludeTextBox.ReadOnly = true;
            this.IncludeTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.IncludeTextBox.Size = new System.Drawing.Size(691, 329);
            this.IncludeTextBox.TabIndex = 31;
            // 
            // CopyTextButton
            // 
            this.CopyTextButton.Location = new System.Drawing.Point(33, 340);
            this.CopyTextButton.Name = "CopyTextButton";
            this.CopyTextButton.Size = new System.Drawing.Size(96, 76);
            this.CopyTextButton.TabIndex = 32;
            this.CopyTextButton.Text = "Copy Text to Clipboard";
            this.CopyTextButton.UseVisualStyleBackColor = true;
            this.CopyTextButton.Click += new System.EventHandler(this.CopyTextButton_Click);
            this.CopyTextButton.MouseLeave += new System.EventHandler(this.CopyTextButton_MouseLeave);
            // 
            // WriteFileButton
            // 
            this.WriteFileButton.Location = new System.Drawing.Point(327, 176);
            this.WriteFileButton.Name = "WriteFileButton";
            this.WriteFileButton.Size = new System.Drawing.Size(124, 40);
            this.WriteFileButton.TabIndex = 33;
            this.WriteFileButton.Text = "Write File";
            this.WriteFileButton.UseVisualStyleBackColor = true;
            this.WriteFileButton.Click += new System.EventHandler(this.WriteFileButton_Click);
            // 
            // ExitButton
            // 
            this.ExitButton.Location = new System.Drawing.Point(526, 176);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(124, 40);
            this.ExitButton.TabIndex = 34;
            this.ExitButton.Text = "Exit";
            this.ExitButton.UseVisualStyleBackColor = true;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click_1);
            // 
            // CopyFileNameButton
            // 
            this.CopyFileNameButton.Location = new System.Drawing.Point(33, 234);
            this.CopyFileNameButton.Name = "CopyFileNameButton";
            this.CopyFileNameButton.Size = new System.Drawing.Size(96, 76);
            this.CopyFileNameButton.TabIndex = 35;
            this.CopyFileNameButton.Text = "Copy File  Name to Clipboard";
            this.CopyFileNameButton.UseVisualStyleBackColor = true;
            this.CopyFileNameButton.Click += new System.EventHandler(this.CopyFileNameButton_Click);
            this.CopyFileNameButton.MouseLeave += new System.EventHandler(this.CopyFileNameButton_MouseLeave);
            // 
            // AddToVCxItemsCheckBox
            // 
            this.AddToVCxItemsCheckBox.AutoSize = true;
            this.AddToVCxItemsCheckBox.Checked = true;
            this.AddToVCxItemsCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.AddToVCxItemsCheckBox.Location = new System.Drawing.Point(169, 96);
            this.AddToVCxItemsCheckBox.Name = "AddToVCxItemsCheckBox";
            this.AddToVCxItemsCheckBox.Size = new System.Drawing.Size(290, 24);
            this.AddToVCxItemsCheckBox.TabIndex = 36;
            this.AddToVCxItemsCheckBox.Text = "Add to .vcItems and .vcxitems.filter";
            this.AddToVCxItemsCheckBox.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 591);
            this.Controls.Add(this.AddToVCxItemsCheckBox);
            this.Controls.Add(this.CopyFileNameButton);
            this.Controls.Add(this.ExitButton);
            this.Controls.Add(this.WriteFileButton);
            this.Controls.Add(this.CopyTextButton);
            this.Controls.Add(this.IncludeTextBox);
            this.Controls.Add(this.BrowseOutputDirectoryButton);
            this.Controls.Add(this.OutputDirectoryTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.MakeIncludeButton);
            this.Controls.Add(this.BrowseSourceCodeButton);
            this.Controls.Add(this.SourceCodeTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.BrowseProjectDirectoryButton);
            this.Controls.Add(this.ProjectDirectoryTextBox);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Make Include File";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BrowseSourceCodeButton;
        private System.Windows.Forms.TextBox SourceCodeTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button BrowseProjectDirectoryButton;
        private System.Windows.Forms.TextBox ProjectDirectoryTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button MakeIncludeButton;
        private System.Windows.Forms.Button BrowseOutputDirectoryButton;
        private System.Windows.Forms.TextBox OutputDirectoryTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox IncludeTextBox;
        private System.Windows.Forms.Button CopyTextButton;
        private System.Windows.Forms.Button WriteFileButton;
        private System.Windows.Forms.Button ExitButton;
        private System.Windows.Forms.Button CopyFileNameButton;
        private System.Windows.Forms.CheckBox AddToVCxItemsCheckBox;
    }
}

