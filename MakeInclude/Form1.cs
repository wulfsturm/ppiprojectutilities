﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MakeInclude
{
    public partial class Form1 : Form
    {
        private Color bColor;
        public Form1()
        {
            this.Icon = Properties.Resources.files1;
            InitializeComponent();
            ProjectDirectoryTextBox.Text = Properties.Settings.Default.ProjectDirectory;
            OutputDirectoryTextBox.Text = Properties.Settings.Default.OuputDirectory;
            bColor = CopyFileNameButton.BackColor;
            CopyTextButton.Enabled = false;
            CopyFileNameButton.Enabled = false;
        }
        string GetExceptionMessage(string message, System.Exception exception)
        {
            string text = string.Format("{0}\n{1}", message, exception.Message);
            System.Exception inner = exception.InnerException;
            while (inner != null)
            {
                message = string.Format("{0}\n{1}", message, inner.Message);
                inner = inner.InnerException;
            }
            return text;
        }
        List<string> ReadFile(string fileName)
        {
            List<string> contents = new List<string>();

            try
            {
                StreamReader streamReader = new StreamReader(fileName);
                while (!streamReader.EndOfStream)
                {
                    string line = streamReader.ReadLine();
                    contents.Add(line);
                }
                streamReader.Close();
            }
            catch (System.Exception ex)
            {
                string message = GetExceptionMessage(string.Format("Error opening or reading \"{0}\"", fileName), ex);
                MessageBox.Show(message);
            }
            return contents;
        }
        private string WriteFile(List<string> contents, string fileName)
        {
            string retval = string.Empty;
            try
            {
                StreamWriter streamWriter = new StreamWriter(fileName, false);
                foreach (string line in contents)
                    streamWriter.WriteLine(line);
                streamWriter.Close();
            }
            catch (System.Exception ex)
            {
                string message = GetExceptionMessage(string.Format("Error opening or writing to {0}.", fileName), ex);
                MessageBox.Show(message);
                retval = string.Format("Failed to write {0}", fileName);
            }
            return retval;
        }
        private List<string> WriteVCXItems(string directory, string sourceCode, string fileName)
        {
            List<string> retval = new List<string>();
            DirectoryInfo di = new DirectoryInfo(directory);
            FileInfo fiVCX = new FileInfo(Path.Combine(directory, string.Format("{0}.vcxitems", di.Name)));
            FileInfo fiFil = new FileInfo(Path.Combine(directory, string.Format("{0}.vcxitems.filters", di.Name)));
            if (fiVCX.Exists)
            {
                List<string> fileContents = ReadFile(fiVCX.FullName);
                if (fileContents.Count == 0)
                {
                    retval.Add(string.Format("No contents in {0}", fiVCX.FullName));
                }
                else
                {
                    if (AddToVCXItems(fileContents, fileName))
                    {
                        WriteFile(fileContents, fiVCX.FullName);
                        retval.Add(string.Format("{0} added to {1}", fileName, fiVCX.Name));
                    }
                    else
                    {
                        retval.Add(string.Format("{0} NOT added to {1}", fileName, fiVCX.Name));
                    }

                    if (fiFil.Exists)
                    {
                        fileContents = ReadFile(fiFil.FullName);
                        if (fileContents.Count == 0)
                        {
                            retval.Add(string.Format("No contents in {0}", fiFil.FullName));
                        }
                        else
                        {
                            string filterName = sourceCode.Substring(0, sourceCode.Length - fileName.Length - 1);
                            fileContents = ReadFile(fiFil.FullName);
                            if (AddToVCXItemsFilters(fileContents, fileName, filterName))
                            {
                                WriteFile(fileContents, fiFil.FullName);
                                retval.Add(string.Format("{0} added to {1} with filter group {2}", fileName, fiFil.Name, filterName));
                            }
                            else
                            {
                                retval.Add(string.Format("{0} NOT added to {1} with filter group {2}", fileName, fiFil.Name, filterName));
                            }
                        }
                    }
                }
            }
            return retval;
        }
        private bool AddToVCXItems(List<string> fileContents, string fileName)
        {
            bool retVal = false;
            string lineToAdd = string.Format("    <ClInclude Include=\"$(MSBuildThisFileDirectory){0}\" />", fileName);
            if (!fileContents.Contains(lineToAdd))
            {
                int index = 0;
                for (; index < fileContents.Count; index++)
                {
                    if (fileContents[index].StartsWith("    <ClInclude Include=\"$(MSBuildThisFileDirectory)"))
                        break;
                }
                if (index <= fileContents.Count)
                {
                    while ((index < fileContents.Count) && (string.Compare(fileContents[index], "  </ItemGroup>") != 0) &&
                        (string.Compare(fileContents[index], lineToAdd) < 0)) index++;
                    if (index < fileContents.Count)
                    {
                        fileContents.Insert(index, lineToAdd);
                        retVal = true;
                    }
                }
            }
            return retVal;
        }
        private bool AddToVCXItemsFilters(List<string> fileContents, string fileName, string filterName)
        {
            bool retVal = false;
            string lineToAdd = string.Format("    <ClInclude Include=\"$(MSBuildThisFileDirectory){0}\">", fileName);
            if (!fileContents.Contains(lineToAdd))
            {
                int index = 0;
                for (; index < fileContents.Count; index++)
                {
                    if (fileContents[index].StartsWith("    <ClInclude Include=\"$(MSBuildThisFileDirectory)"))
                        break;
                }
                if (index <= fileContents.Count)
                {
                    while ((index < fileContents.Count) && (string.Compare(fileContents[index], "  </ItemGroup>") != 0) &&
                        ((fileContents[index].Contains("<Filter>")) || (string.Compare(fileContents[index], lineToAdd, false) < 0))) index++;
                    if (index < fileContents.Count)
                    {
                        fileContents.Insert(index, lineToAdd);
                        fileContents.Insert(index + 1, string.Format("      <Filter>{0}</Filter>", filterName));
                        fileContents.Insert(index + 2, "    </ClInclude>");
                        retVal = true;
                    }
                }
            }
            return retVal;
        }
        public List<string> AddIncludesIfNecessary(List<string> fileContents)
        {
            List<string> references = new List<string>();
            foreach (string line in fileContents)
            {
                MatchCollection matches = Regex.Matches(line, "[ (,][A-Za-z_0-9]*_[DST]O[ ]*\\*");
                if (matches.Count > 0)
                {
                    foreach (Match m in matches)
                    {
                        string s = string.Format("#include \"{0}.h\"", m.Value.Replace(",", "").Replace("(", "").Trim().Replace("*", "").Trim().Replace(" ", "").Trim());                     
                        if (!references.Contains(s)) references.Add(s);
                    }
                }
            }
            return references;
        }
        public int FindLastInclude(List<string> fileContents, int start)
        {
            int lastInclude = start;
            for (int i = start; i < fileContents.Count(); i++)
                if (fileContents[i].StartsWith("#include ")) lastInclude = i;
            return lastInclude;
        }
        private void AddLinesToTextBox(List<string> lines, TextBox textBox)
        {
            string[] activityText = textBox.Lines;
            List<string> activityList = new List<string>(activityText);
            activityList.AddRange(lines);
            textBox.Lines = activityList.ToArray<string>();
        }
        private void AddLineToTextBox(string line, TextBox textBox)
        {
            string[] activityText = textBox.Lines;
            List<string> activityList = new List<string>(activityText);
            activityList.Add(line);
            textBox.Lines = activityList.ToArray<string>();
            textBox.SelectionStart = textBox.TextLength;
            textBox.ScrollToCaret();
        }
        private void BrowseFolderButton_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.ShowNewFolderButton = false;
            //			fd.RootFolder = Environment.SpecialFolder.MyComputer;
            if ((string)button.Tag == "ProjectDirectory")
            {
                fd.Description = "Specify the path where the Visual Studio project is";
                fd.SelectedPath = ProjectDirectoryTextBox.Text;
            }
            else if ((string)button.Tag == "OutputDirectory")
            {
                fd.Description = "Specify the path where the incldue file is to be written";
                fd.SelectedPath = OutputDirectoryTextBox.Text;
            }
            DialogResult dialogResult = fd.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                if ((string)button.Tag == "ProjectDirectory")
                    ProjectDirectoryTextBox.Text = fd.SelectedPath;
                else if ((string)button.Tag == "OutputDirectory")
                    OutputDirectoryTextBox.Text = fd.SelectedPath;
            }
            fd.Dispose();
        }
        private void BrowseFileButton_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.InitialDirectory = ProjectDirectoryTextBox.Text;
            ofd.Filter = ".c files (*.c)|*.c|All files (*.*)|*.*";
            ofd.FilterIndex = 1;
            if (!String.IsNullOrEmpty(SourceCodeTextBox.Text))
            {
                string sourceFileName = SourceCodeTextBox.Text;
                while (sourceFileName.StartsWith("\\")) sourceFileName = sourceFileName.Substring(1);
                string[] parts = sourceFileName.Split(new char[] { '\\' });
                for (int i = 0; i < (parts.Count() - 1); i++)
                {
                    ofd.InitialDirectory = Path.Combine(ofd.InitialDirectory, parts[i]);
                }
            }
            ofd.DefaultExt = ".c";
            ofd.CheckFileExists = true;
            ofd.CheckPathExists = true;
            ofd.Multiselect = false;
            ofd.ReadOnlyChecked = true;
            if (!string.IsNullOrEmpty(SourceCodeTextBox.Text))
                ofd.FileName = (new FileInfo(SourceCodeTextBox.Text)).Name;
            ofd.Title = "Choose Source File to Analyze...";
            DialogResult dialogResult = ofd.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                string sourceFileName = ofd.FileName;
                if (sourceFileName.ToUpper().StartsWith(ProjectDirectoryTextBox.Text.ToUpper()))
                {
                    sourceFileName = sourceFileName.Substring(ProjectDirectoryTextBox.Text.Length);
                    while (sourceFileName.StartsWith("\\")) sourceFileName = sourceFileName.Substring(1);
                    SourceCodeTextBox.Text = sourceFileName;
                    MakeIncludeButton.Enabled = true;
                    IncludeTextBox.Clear();
                    CopyTextButton.Enabled = false;
                    CopyFileNameButton.Enabled = false;
                }
                else
                {
                    MessageBox.Show("The source file has to be in the directory path specified by Project Directory.");
                    BrowseFileButton_Click((object)null, (EventArgs)null);
                }
            }
            ofd.Dispose();
        }
        private void SourceCodeTextBox_DragDrop(object sender, DragEventArgs e)
        {
            int iNum = 0;
            string sFileName = "";

            foreach (string fileName in (string[])e.Data.GetData(DataFormats.FileDrop))
            {
                if (iNum == 0) sFileName = fileName;
                iNum++;
            }
            if (iNum == 1)
            {
                FileInfo fi = new FileInfo(sFileName);
                if (fi.DirectoryName.StartsWith(ProjectDirectoryTextBox.Text, StringComparison.CurrentCultureIgnoreCase))
                {
                    this.SourceCodeTextBox.Text = fi.FullName.Substring(ProjectDirectoryTextBox.Text.Length);
                    if (this.SourceCodeTextBox.Text.StartsWith("\\")) this.SourceCodeTextBox.Text = this.SourceCodeTextBox.Text.Substring(1);
                    MakeIncludeButton.Enabled = true;
                }
                else
                {
                    MessageBox.Show("You can only add files from the project specified in the Project Directory field.");
                    MakeIncludeButton.Enabled = false;
                }
            }
            else
            {
                MessageBox.Show("You can only drag one file into the Source Code field.");
                MakeIncludeButton.Enabled = false;
            }
        }
        private void SourceCodeTextBox_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop, false))
            {
                e.Effect = DragDropEffects.All;
            }
        }
        private void SourceCodeTextBox_TextChanged(object sender, EventArgs e)
        {
            TextBox textBox = (TextBox)sender;
            if (!String.IsNullOrEmpty(textBox.Text))
            {
                string sourceFileName = Path.Combine(ProjectDirectoryTextBox.Text, textBox.Text);
                if ((File.Exists(sourceFileName)) && (Directory.Exists(OutputDirectoryTextBox.Text)))
                {
                    MakeIncludeButton.Enabled = true;
                }
                else
                    MakeIncludeButton.Enabled = false;
            }
            else
                MakeIncludeButton.Enabled = false;
            IncludeTextBox.Clear();
            CopyTextButton.Enabled = false;
            CopyFileNameButton.Enabled = false;
        }
        FunctionsClass GetFunctions(List<string> contents)
        {
            FunctionsClass functions = new FunctionsClass();
            int depth = 0;
            List<string> lastLines = new List<string>();
            int index = FindLastInclude(contents, 0) + 1;
            // First pass
            if (index < contents.Count())
            {
                int commentLevel = 0;
                for (int i = index; i < contents.Count(); i++)
                {
                    while (contents[i].Contains(" ;")) contents[i] = contents[i].Replace(" ;", ";");
                    string line = contents[i].Trim();
                    RemoveComments removeComments = new RemoveComments(line, commentLevel);
                    line = removeComments.Line;
                    commentLevel = removeComments.CommentLevel;
                    //                    if ((String.IsNullOrWhiteSpace(line.Trim())) || (commentLevel > 0))
                    if (String.IsNullOrWhiteSpace(line.Trim()))
                    {

                    }
                    else
                    {
                        if (line.Replace("\t", "").Trim().Contains("{"))
                        {
                            if (depth == 0)
                            {
                                if (lastLines.Count() > 0)
                                {
                                    functions.Add(lastLines);
                                    //for (int j = 0; j < lastLines.Count() - 1; j++)
                                    //    functions.Add(string.Format("{0}", lastLines[j].Trim().Replace("( ", "(").Replace(" )", ")")));
                                    //functions.Add(string.Format("{0};", lastLines[lastLines.Count() - 1].Trim().Replace("( ", "(").Replace(" )", ")")));
                                }
                            }
                            lastLines.Clear();
                            depth++;
                            if (line.Contains("}"))
                                depth--;
                        }
                        else if (line.Contains("}"))
                        {
                            depth--;
                            if (line.Contains("{"))
                                depth++;
                        }
                        else if (depth == 0)
                        {
                            if ((!line.StartsWith("#")) && (!line.EndsWith(";")))
                                lastLines.Add(line);
                            else if (line.EndsWith(";"))
                                lastLines.Clear();
                        }
                    }
                }
            }
            return functions;
        }
        private void ExitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void MakeIncludeButton_Click(object sender, EventArgs e)
        {
            IncludeTextBox.Clear();
            FileInfo fileInfo = new FileInfo(SourceCodeTextBox.Text);
            string sourceFileName = Path.Combine(ProjectDirectoryTextBox.Text, SourceCodeTextBox.Text);
            string includeFileName = Path.Combine(OutputDirectoryTextBox.Text,
                string.Format("{0}.h", fileInfo.Name.Substring(0, fileInfo.Name.Length - fileInfo.Extension.Length)));
            List<string> contents = ReadFile(sourceFileName);
            FunctionsClass functions = GetFunctions(contents);
            functions.Sort();
            List<string> prefix = AddIncludesIfNecessary(functions.GetFunctionList());
            prefix.Insert(0, "#pragma once");
            prefix.Add("");
            AddLinesToTextBox(prefix, IncludeTextBox);
            AddLinesToTextBox(functions.GetFunctionList(), IncludeTextBox);
            CopyTextButton.Enabled = true;
            CopyFileNameButton.Enabled = true;
        }

        private void CopyTextButton_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(IncludeTextBox.Text))
                Clipboard.SetText(IncludeTextBox.Text);
            CopyTextButton.BackColor = Color.DarkBlue;
        }
        private void CopyFileNameButton_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(SourceCodeTextBox.Text))
            {
                FileInfo fi = new FileInfo(SourceCodeTextBox.Text);
                string fName = string.Format("{0}.h", fi.Name.Substring(0, fi.Name.Length - fi.Extension.Length));
                Clipboard.SetText(fName);
            }
            CopyFileNameButton.BackColor = Color.DarkBlue;
        }
        private void ExitButton_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void CopyTextButton_MouseLeave(object sender, EventArgs e)
        {
            CopyTextButton.BackColor = bColor;
        }

        private void CopyFileNameButton_MouseLeave(object sender, EventArgs e)
        {
            CopyFileNameButton.BackColor = bColor;
        }

        private void WriteFileButton_Click(object sender, EventArgs e)
        {
            FileInfo fi = new FileInfo(SourceCodeTextBox.Text);
            string fName = Path.Combine(OutputDirectoryTextBox.Text, string.Format("{0}.h", fi.Name.Substring(0, fi.Name.Length - fi.Extension.Length)));
            WriteFile(new List<string>(IncludeTextBox.Lines), fName);
            if (AddToVCxItemsCheckBox.Checked)
            {
                AddLineToTextBox("\n\n\n", IncludeTextBox);
                List<string> results = WriteVCXItems(OutputDirectoryTextBox.Text, SourceCodeTextBox.Text, string.Format("{0}.h", fi.Name.Substring(0, fi.Name.Length - fi.Extension.Length)));
                AddLinesToTextBox(results, IncludeTextBox);
            }
        }
    }
}
