﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeInclude
{
    public class FunctionsClass : IDisposable
    {
        public List<FunctionClass> Functions { get; private set; }
        public List<int> SortOrder { get; private set; }
        private bool _disposed;
        public FunctionsClass ()
        {
            SortOrder = new List<int>(); ;
            Functions = new List<FunctionClass>();
            _disposed = false;
        }
        private int GetSortIndex(int index)
        {
            int retval = -1;
            if ((index >= 0) && (index < Functions.Count))
            {
                for (int i = 0; i < SortOrder.Count; i++)
                {
                    if (SortOrder[i] == index)
                    {
                        retval = i;
                        break;
                    }
                }
            }
            return retval;
        }
        public void Add(List<string> linesIn)
        {
            SortOrder.Add(Functions.Count);
            Functions.Add(new FunctionClass(linesIn));
        }
        public void Sort()
        {
            bool keepGoing = true;
            while (keepGoing)
            {
                keepGoing = false;
                for (int i = 0; i < Functions.Count - 1; i++)
                {
                    int i1 = GetSortIndex(i);
                    int i2 = GetSortIndex(i + 1);
                    if (String.Compare(Functions[i1].Lines[0].Replace(" ", ""), Functions[i2].Lines[0].Replace(" ", ""), true) > 0)
                    {
                        SortOrder[i1] = i + 1;
                        SortOrder[i2] = i;
                        keepGoing = true;
                    }
                }
            }
        }
        public List<string> GetFunctionList()
        {
            List<string> functions = new List<string>();
            for (int i = 0; i < Functions.Count; i++)
            {
                int i1 = GetSortIndex(i);
                string index = "";
                foreach (string s in Functions[i1].Lines)
                {
                    functions.Add(string.Format("{0}{1}", index, s));
                    index = "    ";
                }
                functions[functions.Count - 1] = String.Format("{0};", functions[functions.Count - 1]);
            }
            return functions;
        }
        public void Dispose()
        {
            if (!_disposed)
            {
                foreach (FunctionClass f in Functions)
                    f.Dispose();
                Functions = null;
                SortOrder = null;
                _disposed = true;
            }
        }
        [System.Runtime.InteropServices.DllImport("Kernel32")]
        private extern static Boolean CloseHandle(IntPtr handle);
        ~FunctionsClass()
        {
            if (!this._disposed) Dispose();
        }
    }
    public class FunctionClass : IDisposable
    {
        public List<string> Lines;
        private bool _disposed;
        public FunctionClass(List<string> linesIn)
        {
            Lines = new List<string>();
            Lines = linesIn.Select(x => x.Replace("( ", "(")).ToList();
            Lines = Lines.Select(x => x.Replace(" )", ")")).ToList();
//            Lines.AddRange(linesIn);
            _disposed = false;
        }
        public void Dispose()
        {
            if (!_disposed)
            {
                Lines = null;
                _disposed = true;
            }
        }
        [System.Runtime.InteropServices.DllImport("Kernel32")]
        private extern static Boolean CloseHandle(IntPtr handle);
        ~FunctionClass()
        {
            if (!this._disposed) Dispose();
        }
    }
}