﻿namespace FindUndefinedExternals
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.GoButton = new System.Windows.Forms.Button();
            this.ClearButton = new System.Windows.Forms.Button();
            this.ExitButton = new System.Windows.Forms.Button();
            this.BrowseIncludeFilesDirectoryButton = new System.Windows.Forms.Button();
            this.IncludeFilesDirectoryTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.BrowseProjectDirectoryButton = new System.Windows.Forms.Button();
            this.ProjectDirectoryTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SearchResultsLabel = new System.Windows.Forms.Label();
            this.InputTextBox = new System.Windows.Forms.RichTextBox();
            this.InputPasteButton = new System.Windows.Forms.Button();
            this.OutputCopyButton = new System.Windows.Forms.Button();
            this.OutputTextBox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 102);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(169, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Output From Compile";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 284);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(152, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Include Statements";
            // 
            // GoButton
            // 
            this.GoButton.Location = new System.Drawing.Point(301, 477);
            this.GoButton.Name = "GoButton";
            this.GoButton.Size = new System.Drawing.Size(75, 40);
            this.GoButton.TabIndex = 4;
            this.GoButton.Text = "Find";
            this.GoButton.UseVisualStyleBackColor = true;
            this.GoButton.Click += new System.EventHandler(this.GoButton_Click);
            // 
            // ClearButton
            // 
            this.ClearButton.Location = new System.Drawing.Point(473, 477);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(75, 40);
            this.ClearButton.TabIndex = 5;
            this.ClearButton.Text = "Clear";
            this.ClearButton.UseVisualStyleBackColor = true;
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // ExitButton
            // 
            this.ExitButton.Location = new System.Drawing.Point(645, 477);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(75, 40);
            this.ExitButton.TabIndex = 6;
            this.ExitButton.Text = "Exit";
            this.ExitButton.UseVisualStyleBackColor = true;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // BrowseIncludeFilesDirectoryButton
            // 
            this.BrowseIncludeFilesDirectoryButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BrowseIncludeFilesDirectoryButton.BackgroundImage = global::FindUndefinedExternals.Properties.Resources.browse;
            this.BrowseIncludeFilesDirectoryButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BrowseIncludeFilesDirectoryButton.FlatAppearance.BorderSize = 0;
            this.BrowseIncludeFilesDirectoryButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BrowseIncludeFilesDirectoryButton.Location = new System.Drawing.Point(888, 50);
            this.BrowseIncludeFilesDirectoryButton.Name = "BrowseIncludeFilesDirectoryButton";
            this.BrowseIncludeFilesDirectoryButton.Size = new System.Drawing.Size(47, 42);
            this.BrowseIncludeFilesDirectoryButton.TabIndex = 14;
            this.BrowseIncludeFilesDirectoryButton.Tag = "IncludeFilesDirectory";
            this.BrowseIncludeFilesDirectoryButton.UseVisualStyleBackColor = true;
            this.BrowseIncludeFilesDirectoryButton.Click += new System.EventHandler(this.BrowseFolderButton_Click);
            // 
            // IncludeFilesDirectoryTextBox
            // 
            this.IncludeFilesDirectoryTextBox.Location = new System.Drawing.Point(187, 58);
            this.IncludeFilesDirectoryTextBox.Name = "IncludeFilesDirectoryTextBox";
            this.IncludeFilesDirectoryTextBox.Size = new System.Drawing.Size(691, 26);
            this.IncludeFilesDirectoryTextBox.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 61);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(177, 20);
            this.label3.TabIndex = 12;
            this.label3.Text = "Include Files Directory";
            // 
            // BrowseProjectDirectoryButton
            // 
            this.BrowseProjectDirectoryButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BrowseProjectDirectoryButton.BackgroundImage = global::FindUndefinedExternals.Properties.Resources.browse;
            this.BrowseProjectDirectoryButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BrowseProjectDirectoryButton.FlatAppearance.BorderSize = 0;
            this.BrowseProjectDirectoryButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BrowseProjectDirectoryButton.Location = new System.Drawing.Point(888, 9);
            this.BrowseProjectDirectoryButton.Name = "BrowseProjectDirectoryButton";
            this.BrowseProjectDirectoryButton.Size = new System.Drawing.Size(47, 42);
            this.BrowseProjectDirectoryButton.TabIndex = 17;
            this.BrowseProjectDirectoryButton.Tag = "ProjectDirectory";
            this.BrowseProjectDirectoryButton.UseVisualStyleBackColor = true;
            this.BrowseProjectDirectoryButton.Click += new System.EventHandler(this.BrowseFolderButton_Click);
            // 
            // ProjectDirectoryTextBox
            // 
            this.ProjectDirectoryTextBox.Location = new System.Drawing.Point(187, 17);
            this.ProjectDirectoryTextBox.Name = "ProjectDirectoryTextBox";
            this.ProjectDirectoryTextBox.Size = new System.Drawing.Size(691, 26);
            this.ProjectDirectoryTextBox.TabIndex = 16;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(44, 20);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(136, 20);
            this.label4.TabIndex = 15;
            this.label4.Text = "Project Directory";
            // 
            // SearchResultsLabel
            // 
            this.SearchResultsLabel.AutoSize = true;
            this.SearchResultsLabel.Location = new System.Drawing.Point(183, 533);
            this.SearchResultsLabel.Name = "SearchResultsLabel";
            this.SearchResultsLabel.Size = new System.Drawing.Size(0, 20);
            this.SearchResultsLabel.TabIndex = 18;
            // 
            // InputTextBox
            // 
            this.InputTextBox.Location = new System.Drawing.Point(187, 102);
            this.InputTextBox.Name = "InputTextBox";
            this.InputTextBox.Size = new System.Drawing.Size(801, 162);
            this.InputTextBox.TabIndex = 19;
            this.InputTextBox.Text = "";
            this.InputTextBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.InputTextBox_MouseDown);
            // 
            // InputPasteButton
            // 
            this.InputPasteButton.Location = new System.Drawing.Point(48, 153);
            this.InputPasteButton.Name = "InputPasteButton";
            this.InputPasteButton.Size = new System.Drawing.Size(112, 58);
            this.InputPasteButton.TabIndex = 20;
            this.InputPasteButton.Text = "Paste From Clipboard";
            this.InputPasteButton.UseVisualStyleBackColor = true;
            this.InputPasteButton.Click += new System.EventHandler(this.InputPasteButton_Click);
            // 
            // OutputCopyButton
            // 
            this.OutputCopyButton.Location = new System.Drawing.Point(48, 340);
            this.OutputCopyButton.Name = "OutputCopyButton";
            this.OutputCopyButton.Size = new System.Drawing.Size(112, 58);
            this.OutputCopyButton.TabIndex = 21;
            this.OutputCopyButton.Text = "Copy To Clipboard";
            this.OutputCopyButton.UseVisualStyleBackColor = true;
            this.OutputCopyButton.Click += new System.EventHandler(this.OutputCopyButton_Click);
            this.OutputCopyButton.MouseLeave += new System.EventHandler(this.OutputCopyButton_MouseLeave);
            // 
            // OutputTextBox
            // 
            this.OutputTextBox.Location = new System.Drawing.Point(187, 281);
            this.OutputTextBox.Name = "OutputTextBox";
            this.OutputTextBox.ReadOnly = true;
            this.OutputTextBox.Size = new System.Drawing.Size(801, 162);
            this.OutputTextBox.TabIndex = 22;
            this.OutputTextBox.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 562);
            this.Controls.Add(this.OutputTextBox);
            this.Controls.Add(this.OutputCopyButton);
            this.Controls.Add(this.InputPasteButton);
            this.Controls.Add(this.InputTextBox);
            this.Controls.Add(this.SearchResultsLabel);
            this.Controls.Add(this.BrowseProjectDirectoryButton);
            this.Controls.Add(this.ProjectDirectoryTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.BrowseIncludeFilesDirectoryButton);
            this.Controls.Add(this.IncludeFilesDirectoryTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ExitButton);
            this.Controls.Add(this.ClearButton);
            this.Controls.Add(this.GoButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Find Undefined Externals";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button GoButton;
        private System.Windows.Forms.Button ClearButton;
        private System.Windows.Forms.Button ExitButton;
        private System.Windows.Forms.Button BrowseIncludeFilesDirectoryButton;
        private System.Windows.Forms.TextBox IncludeFilesDirectoryTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button BrowseProjectDirectoryButton;
        private System.Windows.Forms.TextBox ProjectDirectoryTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label SearchResultsLabel;
        private System.Windows.Forms.RichTextBox InputTextBox;
        private System.Windows.Forms.Button InputPasteButton;
        private System.Windows.Forms.Button OutputCopyButton;
        private System.Windows.Forms.RichTextBox OutputTextBox;
    }
}

