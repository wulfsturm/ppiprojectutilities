﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FindUndefinedExternals
{
    public partial class Form1 : Form
    {
        private const char SPLIT = '\'';
        private Color COLOROKAY = Color.LightGreen;
        private Color COLORERROR = Color.OrangeRed;
        private Color COLORWARN = Color.Yellow;
        private Color COLORUSED = Color.DeepSkyBlue;
        private Color COLORNORM;

        private int totalFilesSearched;
        public Form1()
        {
            this.Icon = Properties.Resources.search;
            InitializeComponent();
            IncludeFilesDirectoryTextBox.Text = Properties.Settings.Default.InitialIncludeDirectory;
            ProjectDirectoryTextBox.Text = Properties.Settings.Default.InitialProjectDirectory;
            COLORNORM = InputPasteButton.BackColor;
        }
        private List<string> GetNamesFromInput(string[] lines)
        {
            List<string> externals = new List<string>();
            foreach (string line in lines)
            {
                if (!line.Contains("Done building"))
                {
                    string external = line;
                    if (line.Contains("warning C4013"))
                    {
                        string[] parts = line.Split(new char[] { SPLIT });
                        if (parts.Length == 3)
                        {
                            external = parts[1];
                        }
                    }
                    else if (line.Contains("): warning C"))
                        external = string.Empty;
                    if (external != string.Empty) externals.Add(external);
                }
            }
            return externals;
        }
        private int AddLineToOutput(string line)
        {
            List<string> lines = new List<string>(OutputTextBox.Lines);
            if (!lines.Contains(line))
            {
                lines.Add(line);
                OutputTextBox.Lines = lines.ToArray();
            }
            return lines.Count();
        }
        string GetExceptionMessage(string message, System.Exception exception)
        {
            string text = string.Format("{0}\n{1}", message, exception.Message);
            System.Exception inner = exception.InnerException;
            while (inner != null)
            {
                message = string.Format("{0}\n{1}", message, inner.Message);
                inner = inner.InnerException;
            }
            return text;
        }
        List<string> ReadFile(string fileName)
        {
            List<string> contents = new List<string>();

            try
            {
                StreamReader streamReader = new StreamReader(fileName);
                while (!streamReader.EndOfStream)
                {
                    string line = streamReader.ReadLine();
                    contents.Add(line);
                }
                streamReader.Close();
            }
            catch (System.Exception ex)
            {
                string message = GetExceptionMessage(string.Format("Error opening or reading \"{0}\"", fileName), ex);
                MessageBox.Show(message);
            }
            return contents;
        }
        private bool CheckIfIncludeFileInProject(string fileName)
        {
            String cFile = Path.Combine(ProjectDirectoryTextBox.Text, Path.Combine("i3include", (new FileInfo(fileName)).Name));
            return File.Exists(cFile);
        }
        private void BrowseFolderButton_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.ShowNewFolderButton = false;
            //			fd.RootFolder = Environment.SpecialFolder.MyComputer;
            if ((string)button.Tag == "ProjectDirectory")
            {
                fd.Description = "Specify the path where the Visual Studio project is";
                fd.SelectedPath = ProjectDirectoryTextBox.Text;
            }
            else if ((string)button.Tag == "IncludeFilesDirectory")
            {
                fd.Description = "Specify the path where the Generate Code directory structure is";
                fd.SelectedPath = IncludeFilesDirectoryTextBox.Text;
            }
            DialogResult dialogResult = fd.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                if ((string)button.Tag == "ProjectDirectory")
                    ProjectDirectoryTextBox.Text = fd.SelectedPath;
                else if ((string)button.Tag == "IncludeFilesDirectory")
                    IncludeFilesDirectoryTextBox.Text = fd.SelectedPath;
            }
            fd.Dispose();
        }
        SearchStatus SearchDirectories(DirectoryInfo directory, string external, int count)
        {
            string fileName = string.Empty;
            foreach (FileInfo fileInfo in directory.EnumerateFiles())
            {
                if (fileInfo.Extension.ToLower() == ".h")
                {
                    List<string> contents = ReadFile(fileInfo.FullName);
                    foreach (string line in contents)
                    {
                        if (line.Contains(external))
                        {
                            string[] parts1 = line.Trim().Split(new char[] { ' ', '(' });
                            if (parts1.Length > 2)
                            {
                                if (parts1[1] == external)
                                {
                                    fileName = fileInfo.FullName;
                                    break;
                                }
                            }
                        }
                    }
                    count++;
                    SearchResultsLabel.Text = string.Format("Searched {0} file{1}", count, count == 1 ? "" : "s");
                    this.Update();
                }
            }
            if (string.IsNullOrEmpty(fileName))
            {
                foreach (DirectoryInfo directoryInfo in directory.EnumerateDirectories())
                {
                    SearchStatus retval = SearchDirectories(directoryInfo, external, count);
                    count = retval.Count;
                    if (!string.IsNullOrEmpty(retval.FileName))
                    {
                        fileName = retval.FileName;
                        break;
                    }
                }
            }
            return new SearchStatus(fileName, count);
        }
        public void MarkSingleLine(RichTextBox rtb, int index, Color color)
        {
            int a = rtb.GetFirstCharIndexFromLine(index);
            int b = rtb.Lines[index].Length;
            rtb.Select(a, b);
            rtb.SelectionBackColor = color;
            rtb.Select(0, 0);
        }
        private void ExitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ClearButton_Click(object sender, EventArgs e)
        {
            InputTextBox.Clear();
            OutputTextBox.Clear();
            SearchResultsLabel.Text = string.Empty;
            InputPasteButton.BackColor = COLORNORM;
        }

        private void GoButton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ProjectDirectoryTextBox.Text))
            {
                if (Directory.Exists(ProjectDirectoryTextBox.Text))
                {
                    if (!string.IsNullOrEmpty(IncludeFilesDirectoryTextBox.Text))
                    {
                        if (Directory.Exists(IncludeFilesDirectoryTextBox.Text))
                        {
                            Stopwatch stopWatch = new Stopwatch();
                            totalFilesSearched = 0;
                            OutputTextBox.Clear();
                            stopWatch.Start();
                            DirectoryInfo projectDirectory = new DirectoryInfo(ProjectDirectoryTextBox.Text);
                            DirectoryInfo includeDirectory = new DirectoryInfo(IncludeFilesDirectoryTextBox.Text);
                            if (!string.IsNullOrEmpty(InputTextBox.Text))
                            {
                                List<string> lines = GetNamesFromInput(InputTextBox.Lines);
                                InputTextBox.Lines = lines.ToArray();
                            }
                            for (int i = 0; i < InputTextBox.Lines.Length; i++)
                            {
                                SearchStatus searchStatus = SearchDirectories(includeDirectory, InputTextBox.Lines[i], 0);
                                Application.DoEvents();
                                string fileName = searchStatus.FileName;
                                totalFilesSearched += searchStatus.Count;
                                if (!String.IsNullOrEmpty(fileName))
                                {
                                    MarkSingleLine(InputTextBox, i, COLOROKAY);
                                    if (CheckIfIncludeFileInProject(fileName))
                                    {
                                        AddLineToOutput(string.Format("#include \"{0}\"", (new FileInfo(fileName).Name)));
                                    }
                                    else
                                    {
                                        int thisLine = AddLineToOutput(fileName) - 1;
                                        MarkSingleLine(OutputTextBox, thisLine, COLORERROR);
                                    }
                                }
                                else
                                {
                                    SearchStatus pSearchStatus = SearchDirectories(projectDirectory, InputTextBox.Lines[i], searchStatus.Count);
                                    Application.DoEvents();
                                    fileName = pSearchStatus.FileName;
                                    totalFilesSearched += searchStatus.Count;
                                    if (!String.IsNullOrEmpty(fileName))
                                    {
                                        MarkSingleLine(InputTextBox, i, COLOROKAY);
                                        if (CheckIfIncludeFileInProject(fileName))
                                        {
                                            AddLineToOutput(string.Format("#include \"{0}\"", (new FileInfo(fileName).Name)));
                                        }
                                        else
                                        {
                                            int thisLine = AddLineToOutput(fileName) - 1;
                                            MarkSingleLine(OutputTextBox, thisLine, COLORERROR);
                                        }
                                    }
                                    else
                                        MarkSingleLine(InputTextBox, i, COLORERROR);
                                }
                            }
                            stopWatch.Stop();
                            TimeSpan ts = stopWatch.Elapsed;
                            string elapsedTime = String.Format("{0:N00}.{1:00}",
                                ts.Hours*3600 + ts.Minutes*60 + ts.Seconds,
                                ts.Milliseconds / 10);
                            SearchResultsLabel.Text = string.Format("Finished! A total of {0} file{1} searched, elapsed time {2} seconds.",
                                totalFilesSearched.ToString("N0"), totalFilesSearched == 1 ? "" : "s", elapsedTime);
                        }
                    }
                }
            }
        }

        private void InputPasteButton_Click(object sender, EventArgs e)
        {
            InputPasteButton.BackColor = COLORNORM;
            InputTextBox.Text = Clipboard.GetText();
        }

        private void OutputCopyButton_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(OutputTextBox.Text))
                Clipboard.SetText(OutputTextBox.Text);
            ((Button)sender).BackColor = COLORUSED;
        }

        private void InputTextBox_MouseDown(object sender, MouseEventArgs e)
        {
            MouseButtons mouse = e.Button; 
            if (mouse == MouseButtons.Right)
            {
                InputPasteButton.BackColor = COLORWARN;
            }
        }
        private void OutputCopyButton_MouseLeave(object sender, EventArgs e)
        {
            ((Button)sender).BackColor = COLORNORM;
        }
    }

    public class SearchStatus
    {
        public string FileName { get; set; }
        public int Count { get; set; }
        public SearchStatus()
        {
            FileName = string.Empty;
            Count = 0;
        }
        public SearchStatus(string fileNameIn, int countIn)
        {
            FileName = fileNameIn;
            Count = countIn;
        }
    }
}
