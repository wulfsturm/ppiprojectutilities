﻿namespace GetIncludeFilesForSource
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BrowseProjectDirectoryButton = new System.Windows.Forms.Button();
            this.ProjectDirectoryTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.BrowseIncludeFilesDirectoryButton = new System.Windows.Forms.Button();
            this.IncludeFilesDirectoryTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.BrowseSourceCodeButton = new System.Windows.Forms.Button();
            this.SourceCodeTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.AnalyzeButton = new System.Windows.Forms.Button();
            this.ClearAllButton = new System.Windows.Forms.Button();
            this.ExitButton = new System.Windows.Forms.Button();
            this.RestoreDefaultsButton = new System.Windows.Forms.Button();
            this.ResultsTextBox = new System.Windows.Forms.TextBox();
            this.FileListButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BrowseProjectDirectoryButton
            // 
            this.BrowseProjectDirectoryButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BrowseProjectDirectoryButton.BackgroundImage = global::GetIncludeFilesForSource.Properties.Resources.browse;
            this.BrowseProjectDirectoryButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BrowseProjectDirectoryButton.FlatAppearance.BorderSize = 0;
            this.BrowseProjectDirectoryButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BrowseProjectDirectoryButton.Location = new System.Drawing.Point(892, 4);
            this.BrowseProjectDirectoryButton.Name = "BrowseProjectDirectoryButton";
            this.BrowseProjectDirectoryButton.Size = new System.Drawing.Size(47, 42);
            this.BrowseProjectDirectoryButton.TabIndex = 8;
            this.BrowseProjectDirectoryButton.Tag = "ProjectDirectory";
            this.BrowseProjectDirectoryButton.UseVisualStyleBackColor = true;
            this.BrowseProjectDirectoryButton.Click += new System.EventHandler(this.BrowseFolderButton_Click);
            // 
            // ProjectDirectoryTextBox
            // 
            this.ProjectDirectoryTextBox.Location = new System.Drawing.Point(191, 12);
            this.ProjectDirectoryTextBox.Name = "ProjectDirectoryTextBox";
            this.ProjectDirectoryTextBox.Size = new System.Drawing.Size(691, 26);
            this.ProjectDirectoryTextBox.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 20);
            this.label1.TabIndex = 6;
            this.label1.Text = "Project Directory";
            // 
            // BrowseIncludeFilesDirectoryButton
            // 
            this.BrowseIncludeFilesDirectoryButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BrowseIncludeFilesDirectoryButton.BackgroundImage = global::GetIncludeFilesForSource.Properties.Resources.browse;
            this.BrowseIncludeFilesDirectoryButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BrowseIncludeFilesDirectoryButton.FlatAppearance.BorderSize = 0;
            this.BrowseIncludeFilesDirectoryButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BrowseIncludeFilesDirectoryButton.Location = new System.Drawing.Point(892, 54);
            this.BrowseIncludeFilesDirectoryButton.Name = "BrowseIncludeFilesDirectoryButton";
            this.BrowseIncludeFilesDirectoryButton.Size = new System.Drawing.Size(47, 42);
            this.BrowseIncludeFilesDirectoryButton.TabIndex = 11;
            this.BrowseIncludeFilesDirectoryButton.Tag = "IncludeFilesDirectory";
            this.BrowseIncludeFilesDirectoryButton.UseVisualStyleBackColor = true;
            this.BrowseIncludeFilesDirectoryButton.Click += new System.EventHandler(this.BrowseFolderButton_Click);
            // 
            // IncludeFilesDirectoryTextBox
            // 
            this.IncludeFilesDirectoryTextBox.Location = new System.Drawing.Point(191, 62);
            this.IncludeFilesDirectoryTextBox.Name = "IncludeFilesDirectoryTextBox";
            this.IncludeFilesDirectoryTextBox.Size = new System.Drawing.Size(691, 26);
            this.IncludeFilesDirectoryTextBox.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 65);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(177, 20);
            this.label2.TabIndex = 9;
            this.label2.Text = "Include Files Directory";
            // 
            // BrowseSourceCodeButton
            // 
            this.BrowseSourceCodeButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BrowseSourceCodeButton.BackgroundImage = global::GetIncludeFilesForSource.Properties.Resources.browse;
            this.BrowseSourceCodeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BrowseSourceCodeButton.FlatAppearance.BorderSize = 0;
            this.BrowseSourceCodeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BrowseSourceCodeButton.Location = new System.Drawing.Point(892, 113);
            this.BrowseSourceCodeButton.Name = "BrowseSourceCodeButton";
            this.BrowseSourceCodeButton.Size = new System.Drawing.Size(47, 42);
            this.BrowseSourceCodeButton.TabIndex = 14;
            this.BrowseSourceCodeButton.Tag = "Input";
            this.BrowseSourceCodeButton.UseVisualStyleBackColor = true;
            this.BrowseSourceCodeButton.Click += new System.EventHandler(this.BrowseFileButton_Click);
            // 
            // SourceCodeTextBox
            // 
            this.SourceCodeTextBox.AllowDrop = true;
            this.SourceCodeTextBox.Location = new System.Drawing.Point(191, 121);
            this.SourceCodeTextBox.Name = "SourceCodeTextBox";
            this.SourceCodeTextBox.Size = new System.Drawing.Size(691, 26);
            this.SourceCodeTextBox.TabIndex = 13;
            this.SourceCodeTextBox.TextChanged += new System.EventHandler(this.SourceCodeTextBox_TextChanged);
            this.SourceCodeTextBox.DragDrop += new System.Windows.Forms.DragEventHandler(this.SourceCodeTextBox_DragDrop);
            this.SourceCodeTextBox.DragEnter += new System.Windows.Forms.DragEventHandler(this.SourceCodeTextBox_DragEnter);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(78, 124);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 20);
            this.label3.TabIndex = 12;
            this.label3.Text = "Source Code";
            // 
            // AnalyzeButton
            // 
            this.AnalyzeButton.Location = new System.Drawing.Point(191, 173);
            this.AnalyzeButton.Name = "AnalyzeButton";
            this.AnalyzeButton.Size = new System.Drawing.Size(84, 31);
            this.AnalyzeButton.TabIndex = 15;
            this.AnalyzeButton.Text = "Analyze";
            this.AnalyzeButton.UseVisualStyleBackColor = true;
            this.AnalyzeButton.Click += new System.EventHandler(this.AnalyzeButton_Click);
            // 
            // ClearAllButton
            // 
            this.ClearAllButton.Location = new System.Drawing.Point(570, 173);
            this.ClearAllButton.Name = "ClearAllButton";
            this.ClearAllButton.Size = new System.Drawing.Size(84, 31);
            this.ClearAllButton.TabIndex = 16;
            this.ClearAllButton.Text = "Clear All";
            this.ClearAllButton.UseVisualStyleBackColor = true;
            this.ClearAllButton.Click += new System.EventHandler(this.ClearAllButton_Click);
            // 
            // ExitButton
            // 
            this.ExitButton.Location = new System.Drawing.Point(725, 173);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(84, 31);
            this.ExitButton.TabIndex = 17;
            this.ExitButton.Text = "Exit";
            this.ExitButton.UseVisualStyleBackColor = true;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // RestoreDefaultsButton
            // 
            this.RestoreDefaultsButton.Location = new System.Drawing.Point(346, 173);
            this.RestoreDefaultsButton.Name = "RestoreDefaultsButton";
            this.RestoreDefaultsButton.Size = new System.Drawing.Size(153, 31);
            this.RestoreDefaultsButton.TabIndex = 18;
            this.RestoreDefaultsButton.Text = "Restore Defaults";
            this.RestoreDefaultsButton.UseVisualStyleBackColor = true;
            this.RestoreDefaultsButton.Click += new System.EventHandler(this.RestoreDefaultsButton_Click);
            // 
            // ResultsTextBox
            // 
            this.ResultsTextBox.Location = new System.Drawing.Point(38, 225);
            this.ResultsTextBox.Multiline = true;
            this.ResultsTextBox.Name = "ResultsTextBox";
            this.ResultsTextBox.ReadOnly = true;
            this.ResultsTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.ResultsTextBox.Size = new System.Drawing.Size(930, 313);
            this.ResultsTextBox.TabIndex = 19;
            // 
            // FileListButton
            // 
            this.FileListButton.Location = new System.Drawing.Point(38, 553);
            this.FileListButton.Name = "FileListButton";
            this.FileListButton.Size = new System.Drawing.Size(196, 31);
            this.FileListButton.TabIndex = 20;
            this.FileListButton.Text = "File List To Clipboard";
            this.FileListButton.UseVisualStyleBackColor = true;
            this.FileListButton.Click += new System.EventHandler(this.FileListButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 596);
            this.Controls.Add(this.FileListButton);
            this.Controls.Add(this.ResultsTextBox);
            this.Controls.Add(this.RestoreDefaultsButton);
            this.Controls.Add(this.ExitButton);
            this.Controls.Add(this.ClearAllButton);
            this.Controls.Add(this.AnalyzeButton);
            this.Controls.Add(this.BrowseSourceCodeButton);
            this.Controls.Add(this.SourceCodeTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.BrowseIncludeFilesDirectoryButton);
            this.Controls.Add(this.IncludeFilesDirectoryTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.BrowseProjectDirectoryButton);
            this.Controls.Add(this.ProjectDirectoryTextBox);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Analyze Source Code and Find Include Files";
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BrowseProjectDirectoryButton;
        private System.Windows.Forms.TextBox ProjectDirectoryTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BrowseIncludeFilesDirectoryButton;
        private System.Windows.Forms.TextBox IncludeFilesDirectoryTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button BrowseSourceCodeButton;
        private System.Windows.Forms.TextBox SourceCodeTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button AnalyzeButton;
        private System.Windows.Forms.Button ClearAllButton;
        private System.Windows.Forms.Button ExitButton;
        private System.Windows.Forms.Button RestoreDefaultsButton;
        private System.Windows.Forms.TextBox ResultsTextBox;
        private System.Windows.Forms.Button FileListButton;
    }
}

