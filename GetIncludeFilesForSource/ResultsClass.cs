﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetIncludeFilesForSource
{
    public class ResultsClass
    {
        public List<string> StringList { get; set; }
        public String StringResult { get; set; }
        public Boolean Status { get; set; }
        public string Message { get; set; }
        public ResultsClass()
        {
            StringList = new List<string>();
            StringResult = string.Empty;
            Status = true;
            Message = string.Empty;
        }
    }
}
