﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GetIncludeFilesForSource
{
    public partial class Form1 : Form
    {
        private enum FilterType
        {
            DO,
            DO_Custom,
            DW,
            TO,
            TO_Custom,
            SO,
            Unknown
        }
        private List<string> doFilesAddedList;
        private List<string> soFilesAddedList;
        private List<string> toFilesAddedList;
        private bool performSourceTextChanged;
        public Form1()
        {
            this.Icon = Properties.Resources.download_into_laptop1;
            doFilesAddedList = new List<string>();
            soFilesAddedList = new List<string>();
            toFilesAddedList = new List<string>();
            InitializeComponent();
            performSourceTextChanged = true;
            RestoreDefaultsButton_Click((object)null, (EventArgs)null);
            AnalyzeButton.Enabled = false;
            FileListButton.Enabled = false;
        }
        string GetExceptionMessage(string message, System.Exception exception)
        {
            string text = string.Format("{0}\n{1}", message, exception.Message);
            System.Exception inner = exception.InnerException;
            while (inner != null)
            {
                message = string.Format("{0}\n{1}", message, inner.Message);
                inner = inner.InnerException;
            }
            return text;
        }
        List<string> ReadFile(string fileName)
        {
            List<string> contents = new List<string>();

            try
            {
                StreamReader streamReader = new StreamReader(fileName);
                while (!streamReader.EndOfStream)
                {
                    string line = streamReader.ReadLine();
                    contents.Add(line);
                }
                streamReader.Close();
            }
            catch (System.Exception ex)
            {
                string message = GetExceptionMessage(string.Format("Error opening or reading \"{0}\"", fileName), ex);
                MessageBox.Show(message);
            }
            return contents;
        }
        private string WriteFile(List<string> contents, string fileName)
        {
            string retval = string.Empty;
            try
            {
                StreamWriter streamWriter = new StreamWriter(fileName, false);
                foreach (string line in contents)
                    streamWriter.WriteLine(line);
                streamWriter.Close();
            }
            catch (System.Exception ex)
            {
                string message = GetExceptionMessage(string.Format("Error opening or writing to {0}.", fileName), ex);
                MessageBox.Show(message);
                retval = string.Format("Failed to write {0}", fileName);
            }
            return retval;
        }
        private FilterType GetFilterType(string fileName)
        {
            FilterType retval = FilterType.Unknown;
            if (fileName.Trim().EndsWith("DO.h"))
                retval = FilterType.DO;
            else if (fileName.Trim().EndsWith("DO.c"))
                retval = FilterType.DO;
            else if (fileName.Trim().EndsWith("DO_Custom.h"))
                retval = FilterType.DO_Custom;
            else if (fileName.Trim().EndsWith("DO_Custom.c"))
                retval = FilterType.DO_Custom;
            else if (fileName.Trim().EndsWith("enum.h"))
                retval = FilterType.DW;
            else if (fileName.Trim().EndsWith("SO.h"))
                retval = FilterType.SO;
            else if (fileName.Trim().EndsWith("SO.c"))
                retval = FilterType.SO;
            else if (fileName.Trim().EndsWith("TO.h"))
                retval = FilterType.TO;
            else if (fileName.Trim().EndsWith("TO.c"))
                retval = FilterType.TO;
            else if (fileName.Trim().EndsWith("TO_Custom.h"))
                retval = FilterType.TO_Custom;
            else if (fileName.Trim().EndsWith("TO_Custom.c"))
                retval = FilterType.TO_Custom;
            return retval;
        }
        string GetFilterName(string fileName)
        {
            string retval = string.Empty;
            switch (GetFilterType(fileName))
            {
                case FilterType.DO:
                    retval = "DataObjects";
                    break;
                case FilterType.DO_Custom:
                    retval = "DataObjectsCustom";
                    break;
                case FilterType.DW:
                    retval = "Enums";
                    break;
                case FilterType.SO:
                    retval = "ScreenObjects";
                    break;
                case FilterType.TO:
                    retval = "TableObjects";
                    break;
                case FilterType.TO_Custom:
                    retval = "TableObjectsCustom";
                    break;
                default:
                    retval = string.Empty;
                    break;
            }
            return retval;
        }
        List<string> FindAllIncludeStatements(List<string> fileContents)
        {
            return FindAllIncludeStatements(fileContents, null);
        }
        List<string> FindAllIncludeStatements(List<string>fileContents, List<string> includes)
        {
            if (includes == null) includes = new List<string>();
            foreach (string line in fileContents)
            {
                if (line.StartsWith("#include "))
                {
                    string[] parts = line.Split(new char[] { '"' });
                    if (parts.Length == 3)
                    {
                        if (!IncludeFound(parts[1]))
                        {
                            ResultsClass resultsClass = FindIncludeFile(parts[1]);
                            if (resultsClass.Status)
                            {
                                if (!includes.Contains(parts[1]))
                                {
                                    includes.Add(parts[1]);
                                    foreach (string subFile in resultsClass.StringList)
                                    {
                                        List<string> subFileContents = ReadFile(subFile);
                                        includes = FindAllIncludeStatements(subFileContents, includes);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return includes;
        }
        private bool IncludeFound(string includeFile)
        {
            string includeFileName = Path.Combine(Path.Combine(ProjectDirectoryTextBox.Text, "i3include"), includeFile);
            if (File.Exists(includeFileName)) return true;
            includeFileName = Path.Combine(Path.Combine(ProjectDirectoryTextBox.Text, "i3language"), includeFile);
            return File.Exists(includeFileName);
        }
        private ResultsClass FindIncludeFile(string includeName)
        {
            ResultsClass results = new ResultsClass();
            if (includeName.EndsWith("_Custom.h"))
            {
                string includeFileName = Path.Combine(Path.Combine(IncludeFilesDirectoryTextBox.Text, "custom"), includeName);
                string cSourceFileName = includeFileName.Replace(".h", ".c");
                if (!File.Exists(includeFileName))
                {
                    results.Message = string.Format("{0} not found!", includeFileName);
                    results.Status = false;
                }
                else if (!File.Exists(cSourceFileName))
                {
                    results.Message = string.Format("{0} not found!", cSourceFileName);
                    results.Status = false;
                }
                else
                {
                    results.Status = true;
                    results.StringList.Add(includeFileName);
                    results.StringList.Add(cSourceFileName);
                }
            }
            else if (includeName.EndsWith("_DO.h"))
            {
                string includeFileName = Path.Combine(Path.Combine(IncludeFilesDirectoryTextBox.Text, "infra\\do"), includeName);
                string cSourceFileName = includeFileName.Replace(".h", ".c");
                if (!File.Exists(includeFileName))
                {
                    results.Message = string.Format("{0} not found!", includeFileName);
                    results.Status = false;
                    results.StringResult = string.Empty;
                }
                else if (!File.Exists(cSourceFileName))
                {
                    results.Message = string.Format("{0} not found!", cSourceFileName);
                    results.Status = false;
                }
                else
                {
                    results.Status = true;
                    results.StringList.Add(includeFileName);
                    results.StringList.Add(cSourceFileName);
                }
            }
            else if (includeName.EndsWith("_TO.h"))
            {
                string includeFileName = Path.Combine(Path.Combine(IncludeFilesDirectoryTextBox.Text, "infra\\to"), includeName);
                string cSourceFileName = includeFileName.Replace(".h", ".c");
                if (!File.Exists(includeFileName))
                {
                    results.Message = string.Format("{0} not found!", includeFileName);
                    results.Status = false;
                    results.StringResult = string.Empty;
                }
                else if (!File.Exists(cSourceFileName))
                {
                    results.Message = string.Format("{0} not found!", cSourceFileName);
                    results.Status = false;
                }
                else
                {
                    results.Status = true;
                    results.StringList.Add(includeFileName);
                    results.StringList.Add(cSourceFileName);
                }
            }
            else if (includeName.EndsWith("_SO.h"))
            {
                string includeFileName = Path.Combine(Path.Combine(IncludeFilesDirectoryTextBox.Text, "infra\\so"), includeName);
                string cSourceFileName = includeFileName.Replace(".h", ".c");
                if (!File.Exists(includeFileName))
                {
                    results.Message = string.Format("{0} not found!", includeFileName);
                    results.Status = false;
                    results.StringResult = string.Empty;
                }
                else if (!File.Exists(cSourceFileName))
                {
                    results.Message = string.Format("{0} not found!", cSourceFileName);
                    results.Status = false;
                }
                else
                {
                    results.Status = true;
                    results.StringList.Add(includeFileName);
                    results.StringList.Add(cSourceFileName);
                }
            }
            else if (includeName.EndsWith("_enum.h"))
            {
                string includeFileName = Path.Combine(Path.Combine(IncludeFilesDirectoryTextBox.Text, "infra\\dw"), includeName);
                if (!File.Exists(includeFileName))
                {
                    results.Message = string.Format("{0} not found!", includeFileName);
                    results.Status = false;
                    results.StringResult = string.Empty;
                }
                else
                {
                    results.Status = true;
                    results.StringList.Add(includeFileName);
                }
            }
            else
            {
                results.Status = false;
                results.Message = string.Format("{0} not a recognized generated file name!", includeName);
            }
            return results;
        }
        private void AddLinesToTextBox(List<string> lines, TextBox textBox)
        {
            string[] activityText = textBox.Lines;
            List<string> activityList = new List<string>(activityText);
            activityList.AddRange(lines);
            textBox.Lines = activityList.ToArray<string>();
        }
        private void AddLineToTextBox(string line, TextBox textBox)
        {
            string[] activityText = textBox.Lines;
            List<string> activityList = new List<string> (activityText);
            activityList.Add(line);
            textBox.Lines = activityList.ToArray<string>();
        }
        private List<string> AddFilesToProject(ResultsClass results)
        {
            List<string> sigh = new List<string>();
            foreach (string file in results.StringList)
            {
                if (file.ToLower().EndsWith(".h"))
                    sigh.Add(AddFileToProject(file, "i3include"));
                else if (file.EndsWith(".c"))
                    sigh.AddRange(AddSourceFileToProject(file));
                else
                    sigh.Add(string.Format("Unrecognized file type: {0}", file));
            }
            return sigh;
        }
        List<string> AddSourceFileToProject(string fileName)
        {
            List<string> retval = new List<string>();
            FilterType filterType = GetFilterType(fileName);
            if (fileName.EndsWith("_DO.c"))
            {
                doFilesAddedList.Add(new FileInfo(fileName).Name);
                retval.Add(AddFileToProject(fileName, "DataObjects"));
                FileInfo fi = new FileInfo(fileName);
                ResultsClass resultsClass = FindIncludeFile(fi.Name.Replace(".c", "_Custom.h"));
                if (resultsClass.Status)
                {
                    foreach (string customFile in resultsClass.StringList)
                    {
                        if (customFile.ToLower().EndsWith(".h"))
                            retval.Add(AddFileToProject(customFile, "i3include"));
                        else if (customFile.ToLower().EndsWith("c"))
                        {
                            doFilesAddedList.Add(new FileInfo(customFile).Name);
                            retval.Add(AddFileToProject(customFile, "DataObjects"));
                        }
                    }
                }
            }
            else if (fileName.EndsWith("_TO.c"))
            {
                toFilesAddedList.Add(new FileInfo(fileName).Name);
                retval.Add(AddFileToProject(fileName, "TableObjects"));
                FileInfo fi = new FileInfo(fileName);
                ResultsClass resultsClass = FindIncludeFile(fi.Name.Replace(".c", "_Custom.h"));
                if (resultsClass.Status)
                {
                    foreach (string customFile in resultsClass.StringList)
                    {
                        if (customFile.ToLower().EndsWith(".h"))
                            retval.Add(AddFileToProject(customFile, "i3include"));
                        else if (customFile.ToLower().EndsWith("c"))
                        {
                            toFilesAddedList.Add(new FileInfo(customFile).Name);
                            retval.Add(AddFileToProject(customFile, "TableObjects"));
                        }
                    }
                }
            }
            else if (fileName.EndsWith("_SO.c"))
            {
                soFilesAddedList.Add(new FileInfo(fileName).Name);
                retval.Add(AddFileToProject(fileName, "ScreenObjects"));
                FileInfo fi = new FileInfo(fileName);
                ResultsClass resultsClass = FindIncludeFile(fi.Name.Replace(".c", "_Custom.h"));
                if (resultsClass.Status)
                {
                    foreach (string customFile in resultsClass.StringList)
                    {
                        if (customFile.ToLower().EndsWith(".h"))
                            retval.Add(AddFileToProject(customFile, "i3include"));
                        else if (customFile.ToLower().EndsWith("c"))
                        {
                            soFilesAddedList.Add(new FileInfo(customFile).Name);
                            retval.Add(AddFileToProject(customFile, "ScreenObjects"));
                        }
                    }
                }
            }
            return retval;
        }
        string AddFileToProject(string fileName, string branch)
        {
            string retval = string.Empty;
            try
            {
                FileInfo fi = new FileInfo(fileName);
                File.Copy(fileName, Path.Combine(Path.Combine(ProjectDirectoryTextBox.Text, branch), fi.Name));
            }
            catch (System.Exception ex)
            {
                retval = GetExceptionMessage(string.Format("Error adding file {0} to {1}", fileName, Path.Combine(ProjectDirectoryTextBox.Text, branch)), ex);
            }
            retval = AddLineToVcxItems(Path.Combine(Path.Combine(ProjectDirectoryTextBox.Text, branch), string.Format("{0}.vcxitems", branch)), fileName);
            retval = string.Format("{0}\n{1}", retval, AddLineToVcxItemsFilters(Path.Combine(Path.Combine(ProjectDirectoryTextBox.Text, branch), string.Format("{0}.vcxitems.filters", branch)), fileName));
            return retval;
        }
        string AddLineToVcxItems(string vcxfile, string fileName)
        {
            string retval = string.Empty;
            FileInfo fi = new FileInfo(fileName);
            if (!File.Exists(string.Format("{0}.backup", vcxfile)))
            try
            {
                File.Copy(vcxfile, string.Format("{0}.backup", vcxfile));
            }
            catch(System.Exception ex)
            {
                    retval = GetExceptionMessage(string.Format("Error backing up {0}", vcxfile), ex);
                    return retval;
            }
            List<string> vcxItemsContents = ReadFile(vcxfile);
            string entryType = fileName.ToLower().EndsWith(".h") ? "ClInclude" : "ClCompile";
            string searchString = string.Format("<{0} Include=", entryType);
            int lineNumber = 0;
            bool inSection = false;
            foreach (string line in vcxItemsContents)
            {
                if (line.TrimStart().StartsWith(searchString))
                {
                    inSection = true;
                    int start = line.LastIndexOfAny(new char[] { ')' });
                    if (start < 0)
                    {
                        retval = string.Format("Can't parse {0}. No ')' found.", vcxfile);
                        return retval;
                    }
                    start++;
                    int end = line.LastIndexOfAny(new char[] { '"' });
                    if (end < 0)
                    {
                        retval = string.Format("Can't parse {0}. No '\"' found.", vcxfile);
                        return retval;
                    }
                    string lineFileName = line.Substring(start, end - start);
                    if (string.Compare(lineFileName, fi.Name) > 0)
                        break;
                }
                else if (inSection)
                    break;
                lineNumber++;
            }
            if (lineNumber >= vcxItemsContents.Count())
            {
                retval = string.Format("Error parsing {0}. Can't find the right itemgroup.", vcxfile);
                return retval;
            }
            vcxItemsContents.Insert(lineNumber, string.Format("    <{0} Include=\"$(MSBuildThisFileDirectory){1}\" />",
                entryType, fi.Name));
            retval = WriteFile(vcxItemsContents, vcxfile);
            if (retval == string.Empty)
            {
                FileInfo fvcx = new FileInfo(vcxfile);
                retval = string.Format("{0} copied to {1} and added into {2}", fi.Name, fvcx.DirectoryName, fvcx.Name);
            }
            return retval;
        }
        string AddLineToVcxItemsFilters(string vcxfiltersfile, string fileName)
        {
            string retval = string.Empty;
            // There may not be a filters file for this project
            if (File.Exists(vcxfiltersfile))
            {
                FileInfo fi = new FileInfo(fileName);
                if (!File.Exists(string.Format("{0}.backup", vcxfiltersfile)))
                {
                    try
                    {
                        File.Copy(vcxfiltersfile, string.Format("{0}.backup", vcxfiltersfile));
                    }
                    catch (System.Exception ex)
                    {
                        retval = GetExceptionMessage(string.Format("Error backing up {0}", vcxfiltersfile), ex);
                        return retval;
                    }
                }
                List<string> vcxItemsContents = ReadFile(vcxfiltersfile);
                // Check to see if a line with this name already exists
                string searcha = string.Format("    <ClInclude Include=\"$(MSBuildThisFileDirectory){0}\">", fi.Name);
                string searchb = string.Format("    <ClInclude Include=\"$(MSBuildThisFileDirectory){0}\" />", fi.Name);
                if (vcxItemsContents.Contains(searcha))
                {
                    int index = vcxItemsContents.IndexOf(searcha);
                    string filter = GetFilterName(fi.Name);
                    vcxItemsContents[index + 1] = string.Format("      <Filter>{0}</Filter>", filter);
                }
                else if (vcxItemsContents.Contains(searchb))
                {
                    int index = vcxItemsContents.IndexOf(searchb);
                    string filter = GetFilterName(fi.Name);
                    if (!String.IsNullOrEmpty(filter))
                    {
                        vcxItemsContents[index] = string.Format("    <ClInclude Include=\"$(MSBuildThisFileDirectory){0}\">", fi.Name);
                        vcxItemsContents.Insert(index + 1, string.Format("      <Filter>{0}</Filter>", filter));
                        vcxItemsContents.Insert(index + 2, "    </ClInclude>");
                    }
                }
                else
                {
                    string entryType = fileName.ToLower().EndsWith(".h") ? "ClInclude" : "ClCompile";
                    string searchString = string.Format("<{0} Include=", entryType);
                    int lineNumber = 0;
                    foreach (string line in vcxItemsContents)
                    {
                        if (line.TrimStart().StartsWith(searchString))
                        {
                            string[] parts = line.Split(new char[] { '"', ')' });
                            if (parts.Length != 4)
                            {
                                retval = string.Format("Can't parse {0}. Split {{'\"', ')'}} failed.", vcxfiltersfile);
                                return retval;
                            }
                            string lineFileName = parts[2];
                            if (string.Compare(lineFileName, fi.Name) > 0)
                                break;
                            else
                                lineNumber++;
                        }
                        else
                            lineNumber++;
                    }
                    if (lineNumber >= vcxItemsContents.Count())
                    {
                        retval = string.Format("Error parsing {0}. Can't find the right itemgroup.", vcxfiltersfile);
                        return retval;
                    }
                    string filter = GetFilterName(fi.Name);
                    if (!String.IsNullOrEmpty(filter))
                    {
                        vcxItemsContents.Insert(lineNumber, string.Format("    <{0} Include=\"$(MSBuildThisFileDirectory){1}\">",
                            entryType, fi.Name));
                        vcxItemsContents.Insert(lineNumber + 1, string.Format("      <Filter>{0}</Filter>", GetFilterName(fi.Name)));
                        vcxItemsContents.Insert(lineNumber + 2, "    </ClInclude>");
                    }
                    else
                    {
                        vcxItemsContents.Insert(lineNumber, string.Format("    <{0} Include=\"$(MSBuildThisFileDirectory){1}\" />",
                            entryType, fi.Name));
                    }
                }

                retval = WriteFile(vcxItemsContents, vcxfiltersfile);
                if (retval == string.Empty)
                {
                    FileInfo fvcx = new FileInfo(vcxfiltersfile);
                    retval = string.Format("{0} copied to {1} and added into {2}", fi.Name, fvcx.DirectoryName, fvcx.Name);
                }
            }
            return retval;
        }
        private void ExitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void ClearAllButton_Click(object sender, EventArgs e)
        {
            ProjectDirectoryTextBox.Clear();
            IncludeFilesDirectoryTextBox.Clear();
            doFilesAddedList.Clear();
            soFilesAddedList.Clear();
            toFilesAddedList.Clear();
            SourceCodeTextBox.Clear();
            AnalyzeButton.Enabled = false;
            FileListButton.Enabled = false;
            ResultsTextBox.Text = string.Empty;
        }
        private void RestoreDefaultsButton_Click(object sender, EventArgs e)
        {
            ClearAllButton_Click((object)null, (EventArgs)null);
            ProjectDirectoryTextBox.Text = Properties.Settings.Default.ProjectDirectory;
            IncludeFilesDirectoryTextBox.Text = Properties.Settings.Default.IncludeFilesSourceDirectory;
        }
        private void BrowseFolderButton_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.ShowNewFolderButton = false;
            //			fd.RootFolder = Environment.SpecialFolder.MyComputer;
            if ((string)button.Tag == "ProjectDirectory")
            {
                fd.Description = "Specify the path where the Visual Studio project is";
                fd.SelectedPath = ProjectDirectoryTextBox.Text;
            }
            else if ((string)button.Tag == "IncludeFilesDirectory")
            {
                fd.Description = "Specify the path where the Generate Code directory structure is";
                fd.SelectedPath = IncludeFilesDirectoryTextBox.Text;
            }
            DialogResult dialogResult = fd.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                if ((string)button.Tag == "ProjectDirectory")
                    ProjectDirectoryTextBox.Text = fd.SelectedPath;
                else if ((string)button.Tag == "IncludeFilesDirectory")
                    IncludeFilesDirectoryTextBox.Text = fd.SelectedPath;
            }
            fd.Dispose();
        }
        private void BrowseFileButton_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.InitialDirectory = ProjectDirectoryTextBox.Text;
            ofd.Filter = ".c files (*.c)|*.c|.h files (*.h)|*.h|All files (*.*)|*.*";
            ofd.FilterIndex = 1;
            if (!String.IsNullOrEmpty(SourceCodeTextBox.Text))
            {
                string sourceFileName = SourceCodeTextBox.Text;
                while (sourceFileName.StartsWith("\\")) sourceFileName = sourceFileName.Substring(1);
                string[] parts = sourceFileName.Split(new char[] { '\\' });
                for (int i = 0; i < (parts.Count() - 1); i++)
                {
                    ofd.InitialDirectory = Path.Combine(ofd.InitialDirectory, parts[i]);
                }
            }
            ofd.DefaultExt = ".c";
            ofd.CheckFileExists = true;
            ofd.CheckPathExists = true;
            ofd.Multiselect = false;
            ofd.ReadOnlyChecked = true;
            if (!string.IsNullOrEmpty(SourceCodeTextBox.Text))
                ofd.FileName = (new FileInfo(SourceCodeTextBox.Text)).Name;
            ofd.Title = "Choose Source File to Analyze...";
            DialogResult dialogResult = ofd.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                string sourceFileName = ofd.FileName;
                if (sourceFileName.ToUpper().StartsWith(ProjectDirectoryTextBox.Text.ToUpper()))
                {
                    sourceFileName = sourceFileName.Substring(ProjectDirectoryTextBox.Text.Length);
                    while (sourceFileName.StartsWith("\\")) sourceFileName = sourceFileName.Substring(1);
                    SourceCodeTextBox.Text = sourceFileName;
                    AnalyzeButton.Enabled = true;
                    ResultsTextBox.Text = string.Empty;
                }
                else
                {
                    MessageBox.Show("The source file has to be in the directory path specified by Project Directory.");
                    BrowseFileButton_Click((object)null, (EventArgs)null);
                }
            }
            ofd.Dispose();
        }
        private void SourceCodeTextBox_DragDrop(object sender, DragEventArgs e)
        {
            int iNum = 0;
            string sFileName = "";

            foreach (string fileName in (string[])e.Data.GetData(DataFormats.FileDrop))
            {
                if (iNum == 0) sFileName = fileName;
                iNum++;
            }
            if (iNum == 1)
            {
                FileInfo fi = new FileInfo(sFileName);
                if (fi.DirectoryName.StartsWith(ProjectDirectoryTextBox.Text, StringComparison.CurrentCultureIgnoreCase))
                {
                    this.SourceCodeTextBox.Text = fi.FullName.Substring(ProjectDirectoryTextBox.Text.Length);
                    if (this.SourceCodeTextBox.Text.StartsWith("\\")) this.SourceCodeTextBox.Text = this.SourceCodeTextBox.Text.Substring(1);
                    AnalyzeButton.Enabled = true;
                    ResultsTextBox.Text = string.Empty;
                }
                else
                {
                    MessageBox.Show("You can only add files from the project specified in the Project Directory field.");
                    AnalyzeButton.Enabled = false;
                }
            }
            else
            {
                performSourceTextChanged = false;
                bool finalCheck = true;
                foreach (string fileName in (string[])e.Data.GetData(DataFormats.FileDrop))
                {
                    FileInfo fi = new FileInfo(fileName);
                    if (fi.DirectoryName.StartsWith(ProjectDirectoryTextBox.Text, StringComparison.CurrentCultureIgnoreCase))
                    {
                        string subString = fi.FullName.Substring(ProjectDirectoryTextBox.Text.Length);
                        if (subString.StartsWith("\\")) subString = subString.Substring(1);
                        this.SourceCodeTextBox.Text = String.Format("{0} \"{1}\"", this.SourceCodeTextBox.Text, subString);
                        if (finalCheck) AnalyzeButton.Enabled = true;
                    }
                    else
                    {
                        MessageBox.Show("You can only add files from the project specified in the Project Directory field.");
                        AnalyzeButton.Enabled = false;
                        finalCheck = false;
                    }
                }
                performSourceTextChanged = true;
                if (finalCheck) SourceCodeTextBox_TextChanged(SourceCodeTextBox, (EventArgs)null);
            }
        }
        private void SourceCodeTextBox_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop, false))
            {
                e.Effect = DragDropEffects.All;
            }
        }
        private void SourceCodeTextBox_TextChanged(object sender, EventArgs e)
        {
            if (performSourceTextChanged)
            {
                TextBox textBox = (TextBox)sender;
                if (!String.IsNullOrEmpty(textBox.Text))
                {
                    string[] files = textBox.Text.Split(new char[] { '\"' });
                    foreach (string SourceFileName in files)
                    {
                        if (!String.IsNullOrEmpty(SourceFileName.Trim()))
                        {
                            string sourceFileName = Path.Combine(ProjectDirectoryTextBox.Text, SourceFileName);
                            if (File.Exists(sourceFileName))
                            {
                                AnalyzeButton.Enabled = true;
                                ResultsTextBox.Text = string.Empty;
                            }
                            else
                                AnalyzeButton.Enabled = false;
                        }
                    }
                }
            }
        }
        private void AnalyzeButton_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(SourceCodeTextBox.Text))
            {
                string[] files = SourceCodeTextBox.Text.Split(new char[] { '\"' });
                foreach (string SourceFileName in files)
                {
                    if (!String.IsNullOrEmpty(SourceFileName.Trim()))
                    {
                        string sourceFileName = Path.Combine(ProjectDirectoryTextBox.Text, SourceFileName);
                        if (File.Exists(sourceFileName))
                        {
                            List<string> fileContents = ReadFile(sourceFileName);
                            if (fileContents.Count() > 0)
                            {
                                List<string> includes = FindAllIncludeStatements(fileContents);
                                foreach (string include in includes)
                                {
                                    if (!IncludeFound(include))
                                    {
                                        ResultsClass results = FindIncludeFile(include);
                                        if (results.Status)
                                        {
                                            AddLinesToTextBox(AddFilesToProject(results), ResultsTextBox);
                                            FileListButton.Enabled = true;
                                        }
                                        else
                                        {
                                            AddLineToTextBox(results.Message, ResultsTextBox);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                AnalyzeButton.Enabled = false;
            }
        }
        private void Form1_Resize(object sender, EventArgs e)
        {
            Size size = this.Size;
            ResultsTextBox.Size = new Size(size.Width - 88, size.Height - 296);
        }

        private void FileListButton_Click(object sender, EventArgs e)
        {
            doFilesAddedList.Sort();
            soFilesAddedList.Sort();
            toFilesAddedList.Sort();
            string filesAdded = string.Empty;
            foreach (string file in doFilesAddedList)
                filesAdded = string.Format("{0}\r\n{1}", filesAdded, file);
            foreach (string file in soFilesAddedList)
                filesAdded = string.Format("{0}\r\n{1}", filesAdded, file);
            foreach (string file in toFilesAddedList)
                filesAdded = string.Format("{0}\r\n{1}", filesAdded, file);
            Clipboard.Clear();
            Clipboard.SetText(filesAdded);
        }
    }
}
